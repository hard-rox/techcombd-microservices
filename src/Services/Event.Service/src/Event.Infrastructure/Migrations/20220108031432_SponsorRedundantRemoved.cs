﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Event.Infrastructure.Migrations
{
    public partial class SponsorRedundantRemoved : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SponsorDpUrl",
                table: "Sponsors");

            migrationBuilder.DropColumn(
                name: "SponsorName",
                table: "Sponsors");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "SponsorDpUrl",
                table: "Sponsors",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SponsorName",
                table: "Sponsors",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }
    }
}
