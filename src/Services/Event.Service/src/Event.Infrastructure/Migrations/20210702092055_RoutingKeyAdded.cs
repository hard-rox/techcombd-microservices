﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Event.Infrastructure.Migrations
{
    public partial class RoutingKeyAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "SearchKey",
                table: "Events",
                newName: "RouteKey");

            migrationBuilder.RenameIndex(
                name: "IX_Events_SearchKey",
                table: "Events",
                newName: "IX_Events_RouteKey");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "RouteKey",
                table: "Events",
                newName: "SearchKey");

            migrationBuilder.RenameIndex(
                name: "IX_Events_RouteKey",
                table: "Events",
                newName: "IX_Events_SearchKey");
        }
    }
}
