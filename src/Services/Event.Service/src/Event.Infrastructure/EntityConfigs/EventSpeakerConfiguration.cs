﻿using Event.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Event.Infrastructure.EntityConfigs
{
    internal class EventSpeakerConfiguration : IEntityTypeConfiguration<EventSpeaker>
    {
        public void Configure(EntityTypeBuilder<EventSpeaker> builder)
        {
            builder.HasKey(x => new { x.EventId, x.SpeakerProfileGuid });
            builder.Property(x => x.EventId).IsRequired();
            builder.Property(x => x.SpeakerProfileGuid).IsRequired();
            builder.HasOne(x => x.Event).WithMany(x => x.Speakers).HasForeignKey(x => x.EventId);
        }
    }
}
