﻿using Event.Domain.Enums;
using Microsoft.EntityFrameworkCore;

namespace Event.Infrastructure.EntityConfigs
{
    internal class EventConfiguration : IEntityTypeConfiguration<Domain.Entities.Event>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<Domain.Entities.Event> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Title).IsRequired();
            builder.Property(x => x.About).IsRequired();
            builder.Property(x => x.Status).HasDefaultValue(EventStatus.Draft);
        }
    }
}
