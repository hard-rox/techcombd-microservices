﻿using Event.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Event.Infrastructure.EntityConfigs
{
    internal class EventSponsorConfiguration : IEntityTypeConfiguration<EventSponsor>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<EventSponsor> builder)
        {
            builder.HasKey(x => new { x.EventId, x.SponsorProfileGuid });
            builder.Property(x => x.EventId).IsRequired();
            builder.Property(x => x.SponsorProfileGuid).IsRequired();
            builder.HasOne(x => x.Event).WithMany(x => x.Sponsors).HasForeignKey(x => x.EventId);
        }
    }
}
