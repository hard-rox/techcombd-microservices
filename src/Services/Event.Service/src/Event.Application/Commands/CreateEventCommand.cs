﻿using Event.Domain.Enums;
using MediatR;
using System;

namespace Event.Application.Commands
{
    public class CreateEventCommand : IRequest<Domain.Entities.Event>
    {
        public string Title { get; set; }
        public string About { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public string MapEmbedUrl { get; set; }
        public int? SeatCapacity { get; set; }
        public string FacebookEventUrl { get; set; }
        public string BannerUrl { get; set; }
        public DateTime? EventStartDateTime { get; set; }
        public DateTime? EventEndDateTime { get; set; }
        public DateTime? RegistrationStartDateTime { get; set; }
        public DateTime? RegistrationEndDateTime { get; set; }
        public EventStatus Status { get; set; }
        public EventType? Type { get; set; }
        public bool? IsRegistrationRequired { get; set; }
        public Guid[] SpeakerProfileIds { get; set; }
        public Guid[] SponsorProfileIds { get; set; }
    }
}
