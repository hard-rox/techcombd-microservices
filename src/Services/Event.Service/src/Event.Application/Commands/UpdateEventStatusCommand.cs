﻿using Event.Domain.Enums;
using MediatR;
using System;
using System.Linq;

namespace Event.Application.Commands
{
    public class UpdateEventStatusCommand : IRequest<IQueryable<Event.Domain.Entities.Event>>
    {
        public Guid Id { get; set; }
        public EventStatus Status { get; set; }
    }
}
