﻿using System;
using System.Linq;
using MediatR;

namespace Event.Application.Queries
{
    public class GetEventsFromIdQuery : IRequest<IQueryable<Domain.Entities.Event>>
    {
        public GetEventsFromIdQuery(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; set; }
    }
}
