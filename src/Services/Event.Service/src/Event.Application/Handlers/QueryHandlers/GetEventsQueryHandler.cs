﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Event.Application.Interfaces;
using Event.Application.Queries;
using Event.Domain.Enums;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Event.Application.Handlers.QueryHandlers
{
    internal class GetEventsQueryHandler : IRequestHandler<GetEventsQuery, IQueryable<Domain.Entities.Event>>
    {
        private readonly IEventDataContext _context;
        private readonly ILogger<GetEventsQueryHandler> logger;
        private readonly ICurrentUserService currentUserService;

        public GetEventsQueryHandler(IEventDataContext context, ILogger<GetEventsQueryHandler> logger, ICurrentUserService currentUserService)
        {
            _context = context;
            this.logger = logger;
            this.currentUserService = currentUserService;
        }

        public async Task<IQueryable<Domain.Entities.Event>> Handle(GetEventsQuery request, CancellationToken cancellationToken)
        {
            logger.LogInformation("Handling GetEventsQuery...");
            var query = _context.Events.AsQueryable();
            if (!currentUserService.IsAuthenticated) query = query.Where(x => x.Status == EventStatus.Active);
            var result = await Task.Run(() => query, cancellationToken);
            logger.LogInformation("Handled GetEventsQuery...");
            return result;
        }
    }
}
