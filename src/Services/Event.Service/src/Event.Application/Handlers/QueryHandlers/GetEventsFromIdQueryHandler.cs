﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Event.Application.Interfaces;
using Event.Application.Queries;
using MediatR;

namespace Event.Application.Handlers.QueryHandlers
{
    internal class GetEventsFromIdQueryHandler : IRequestHandler<GetEventsFromIdQuery, IQueryable<Domain.Entities.Event>>
    {
        private readonly IEventDataContext _context;

        public GetEventsFromIdQueryHandler(IEventDataContext context)
        {
            _context = context;
        }

        public async Task<IQueryable<Domain.Entities.Event>> Handle(GetEventsFromIdQuery request, CancellationToken cancellationToken)
        {
            return await Task.Run(() => _context.Events.Where(e => e.EntityGuid == request.Id), cancellationToken);
        }
    }
}
