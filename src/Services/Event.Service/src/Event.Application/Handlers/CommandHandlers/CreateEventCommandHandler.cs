﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Common.Events;
using Common.Exceptions;
using Event.Application.Commands;
using Event.Application.Interfaces;
using Event.Domain.Entities;
using MassTransit;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Event.Application.Handlers.CommandHandlers
{
    internal class CreateEventCommandHandler : IRequestHandler<CreateEventCommand, Domain.Entities.Event>
    {
        private readonly IEventDataContext _context;
        private readonly ICurrentUserService _currentUserService;
        private readonly IBus _messageBus;

        public CreateEventCommandHandler(IEventDataContext context, ICurrentUserService currentUserService, IBus messageBus)
        {
            _context = context;
            _currentUserService = currentUserService;
            _messageBus = messageBus;
        }

        public async Task<Domain.Entities.Event> Handle(CreateEventCommand request, CancellationToken cancellationToken)
        {
            var isDuplicateName = await _context.Events.FirstOrDefaultAsync(x => x.Title == request.Title.Trim().ToLower(), cancellationToken: cancellationToken) != null;
            if (isDuplicateName) throw new ResponseException($"Duplicate Event title {request.Title}");
            var isRequestValid = request.Title != null 
                && request.About != null 
                && request.FacebookEventUrl != null
                && request.BannerUrl != null
                && request.EventStartDateTime != null
                && request.EventEndDateTime != null
                && request.Type != null
                && request.IsRegistrationRequired != null;
            if (request.Status != Domain.Enums.EventStatus.Draft && !isRequestValid)
                throw new ResponseException($"Cannot save event as {request.Status} without all data, Consider saving as draft");

            var newEvent = new Domain.Entities.Event()
            {
                Title = request.Title.Trim(),
                About = request.About.Trim(),
                Description = request.Description?.Trim(),
                Location = request.Location?.Trim(),
                MapEmbedUrl = request.MapEmbedUrl?.Trim(),
                SeatCapacity = request.SeatCapacity,
                FacebookEventUrl = request.FacebookEventUrl?.Trim(),
                BannerUrl = request.BannerUrl?.Trim(),
                EventStartDateTimeUtc = request.EventStartDateTime,
                EventEndDateTimeUtc = request.EventEndDateTime,
                RegistrationStartDateTimeUtc = request.RegistrationStartDateTime,
                RegistrationEndDateTimeUtc = request.RegistrationEndDateTime,
                Status = request.Status,
                Type = request.Type,
                IsRegistrationRequired = request.IsRegistrationRequired,

                Speakers = new List<EventSpeaker>(),
                Sponsors = new List<EventSponsor>(),
            };
            foreach (var speakerProfileId in request.SpeakerProfileIds)
            {
                newEvent.Speakers.Add(new EventSpeaker() { SpeakerProfileGuid = speakerProfileId });
            }
            foreach (var sponsorProfileId in request.SponsorProfileIds)
            {
                newEvent.Sponsors.Add(new EventSponsor() { SponsorProfileGuid = sponsorProfileId });
            }

            await _context.Events.AddAsync(newEvent, cancellationToken);
            var result = await _context.SaveChangesAsync(cancellationToken) > 0;
            if (!result)
                throw new ResponseException("Couldn't saved.'");

            var eventCreated = new EventCreated(
                newEvent.EntityGuid, newEvent.Title,
                newEvent.RegistrationStartDateTimeUtc,
                newEvent.RegistrationEndDateTimeUtc,
                (int)newEvent.Status,
                _currentUserService.UserId,
                newEvent.SeatCapacity);
            await _messageBus.Publish(eventCreated);

            ///TODO: Publish emails to speakers and sponsors...
            return newEvent;
        }
    }
}
