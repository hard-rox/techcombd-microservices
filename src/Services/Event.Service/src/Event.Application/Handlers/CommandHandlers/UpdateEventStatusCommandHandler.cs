﻿using Common.Events;
using Common.Exceptions;
using Event.Application.Commands;
using Event.Application.Interfaces;
using MassTransit;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Event.Application.Handlers.CommandHandlers
{
    internal class UpdateEventStatusCommandHandler : IRequestHandler<UpdateEventStatusCommand, IQueryable<Event.Domain.Entities.Event>>
    {
        private readonly ILogger<UpdateEventCommandHandler> _logger;
        private readonly IEventDataContext eventDataContext;
        private readonly ICurrentUserService _currentUserService;
        private readonly IBus _messageBus;

        public UpdateEventStatusCommandHandler(ILogger<UpdateEventCommandHandler> logger, IEventDataContext eventDataContext, ICurrentUserService currentUserService, IBus messageBus)
        {
            _logger = logger;
            this.eventDataContext = eventDataContext;
            _currentUserService = currentUserService;
            _messageBus = messageBus;
        }

        public async Task<IQueryable<Domain.Entities.Event>> Handle(UpdateEventStatusCommand request, CancellationToken cancellationToken)
        {
            var queryable = eventDataContext.Events.Where(e => e.EntityGuid == request.Id);
            var @event = await queryable.FirstOrDefaultAsync();
            if (@event == null) throw new ResponseException("Event not found");

            @event.Status = request.Status;
            var isSaved = await eventDataContext.SaveChangesAsync() > 0;
            if (!isSaved) throw new ResponseException("Could'nt update");

            var eventUpdated = new EventUpdated(
                @event.EntityGuid,
                @event.Title,
                @event.SeatCapacity,
                @event.RegistrationStartDateTimeUtc,
                @event.RegistrationEndDateTimeUtc,
                (int)@event.Status,
                _currentUserService.UserId);
            await _messageBus.Publish(eventUpdated);

            return queryable;
        }
    }
}
