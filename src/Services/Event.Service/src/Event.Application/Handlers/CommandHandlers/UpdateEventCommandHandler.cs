﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Common.Events;
using Common.Exceptions;
using Event.Application.Commands;
using Event.Application.Interfaces;
using Event.Domain.Entities;
using MassTransit;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Event.Application.Handlers.CommandHandlers
{
    internal class UpdateEventCommandHandler : IRequestHandler<UpdateEventCommand, IQueryable<Domain.Entities.Event>>
    {
        private readonly IEventDataContext _context;
        private readonly ICurrentUserService _currentUserService;
        private readonly IBus _messageBus;

        public UpdateEventCommandHandler(IEventDataContext context, ICurrentUserService currentUserService, IBus messageBus)
        {
            _context = context;
            _currentUserService = currentUserService;
            _messageBus = messageBus;
        }

        public async Task<IQueryable<Domain.Entities.Event>> Handle(UpdateEventCommand request, CancellationToken cancellationToken)
        {
            var queryable = _context.Events.Where(x => x.EntityGuid == request.Id);
            var @event = await queryable
                .Include(x => x.Speakers)
                .Include(x => x.Sponsors)
                .FirstOrDefaultAsync();
            if (@event == null) throw new ResponseException("Event not found");

            @event.Title = request.Title.Trim();
            @event.About = request.About.Trim();
            @event.Description = request.Description?.Trim();
            @event.Location = request.Location?.Trim();
            @event.MapEmbedUrl = request.MapEmbedUrl?.Trim();
            @event.SeatCapacity = request.SeatCapacity;
            @event.FacebookEventUrl = request.FacebookEventUrl?.Trim();
            @event.BannerUrl = request.BannerUrl?.Trim();
            @event.EventStartDateTimeUtc = request.EventStartDateTime;
            @event.EventEndDateTimeUtc = request.EventEndDateTime;
            @event.RegistrationStartDateTimeUtc = request.RegistrationStartDateTime;
            @event.RegistrationEndDateTimeUtc = request.RegistrationEndDateTime;
            @event.Status = request.Status;
            @event.Type = request.Type;
            @event.IsRegistrationRequired = request.IsRegistrationRequired;

            @event.Speakers = new List<EventSpeaker>();
            @event.Sponsors = new List<EventSponsor>();

            foreach (var speakerProfileId in request.SpeakerProfileIds)
            {
                @event.Speakers.Add(new EventSpeaker() { SpeakerProfileGuid = speakerProfileId });
            }
            foreach (var sponsorProfileId in request.SponsorProfileIds)
            {
                @event.Sponsors.Add(new EventSponsor() { SponsorProfileGuid = sponsorProfileId });
            }
            var updated = await _context.SaveChangesAsync() > 0;
            if (!updated) throw new ResponseException("Something error...");

            var eventUpdated = new EventUpdated(
                @event.EntityGuid,
                @event.Title,
                (int)@event.SeatCapacity,
                (DateTime)@event.RegistrationStartDateTimeUtc,
                (DateTime)@event.RegistrationEndDateTimeUtc,
                (int)@event.Status,
                _currentUserService.UserId);
            await _messageBus.Publish(eventUpdated);

            return queryable;
        }
    }
}
