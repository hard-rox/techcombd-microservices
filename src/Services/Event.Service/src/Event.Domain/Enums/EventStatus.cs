﻿namespace Event.Domain.Enums
{
    public enum EventStatus
    {
        Active = 1,
        Inactive = 2,
        Draft = 3,
    }
}