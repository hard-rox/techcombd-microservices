﻿namespace Event.Domain.Enums
{
    public enum EventType
    {
        Online = 1,
        Onsite = 2,
    }
}
