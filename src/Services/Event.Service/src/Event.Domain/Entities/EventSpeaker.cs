﻿using System;
using Event.Domain.Common;

namespace Event.Domain.Entities
{
    public class EventSpeaker : AuditableEntity
    {
        public long EventId { get; set; }
        public Guid SpeakerProfileGuid { get; set; }

        public virtual Event Event { get; set; }
    }
}
