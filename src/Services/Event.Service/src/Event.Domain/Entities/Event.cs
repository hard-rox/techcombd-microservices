﻿using System;
using System.Collections.Generic;
using Event.Domain.Common;
using Event.Domain.Enums;

namespace Event.Domain.Entities
{
    public class Event : AuditableEntity
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string About { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public string MapEmbedUrl { get; set; }
        public int? SeatCapacity { get; set; }
        public int? SeatsOccupied { get; set; }
        public string FacebookEventUrl { get; set; }
        public string BannerUrl { get; set; }
        public DateTime? EventStartDateTimeUtc { get; set; }
        public DateTime? EventEndDateTimeUtc { get; set; }
        public DateTime? RegistrationStartDateTimeUtc { get; set; }
        public DateTime? RegistrationEndDateTimeUtc { get; set; }
        public EventStatus Status { get; set; }
        public EventType? Type { get; set; }
        public bool? IsRegistrationRequired { get; set; }

        public virtual ICollection<EventSpeaker> Speakers { get; set; }
        public virtual ICollection<EventSponsor> Sponsors { get; set; }
    }
}
