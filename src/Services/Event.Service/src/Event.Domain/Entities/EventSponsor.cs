﻿using Event.Domain.Common;
using System;

namespace Event.Domain.Entities
{
    public class EventSponsor : AuditableEntity
    {
        public long EventId { get; set; }
        public Guid SponsorProfileGuid { get; set; }

        public virtual Event Event { get; set; }
    }
}
