﻿using System.Linq;
using HotChocolate.Data;

namespace Event.GraphQL.Payloads
{
    public class UpdateEventPayload
    {
        public UpdateEventPayload(IQueryable<Domain.Entities.Event> @event)
        {
            UpdatedEvent = @event;
        }

        [UseFirstOrDefault]
        [UseProjection]
        public IQueryable<Domain.Entities.Event> UpdatedEvent { get; private set; }
    }
}
