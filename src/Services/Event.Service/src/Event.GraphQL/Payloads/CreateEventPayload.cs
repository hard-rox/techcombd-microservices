﻿namespace Event.GraphQL.Payloads
{
    public class CreateEventPayload
    {
        public CreateEventPayload(Domain.Entities.Event @event)
        {
            Event = @event;
        }

        public Domain.Entities.Event Event { get; }
    }
}
