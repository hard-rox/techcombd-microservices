﻿using HotChocolate.Types;
using System.IO;
using System.Threading.Tasks;

namespace Event.GraphQL.Helpers
{
    public static class Extensions
    {
        public static async Task<(byte[], string)> ToByteArrayAsync(this IFile file)
        {
            if (file == null) return (null, null);

            var stream = file.OpenReadStream();
            using var ms = new MemoryStream();
            await stream.CopyToAsync(ms);
            return (ms.ToArray(), file.Name);
        }
    }
}
