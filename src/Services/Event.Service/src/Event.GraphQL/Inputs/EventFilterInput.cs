﻿using HotChocolate.Data.Filters;

namespace Event.GraphQL.Inputs
{
    public class EventFilterInput : FilterInputType<Domain.Entities.Event>
    {
        protected override void Configure(IFilterInputTypeDescriptor<Domain.Entities.Event> descriptor)
        {
            descriptor.BindFieldsExplicitly();
            descriptor.Field(x => x.Title);
            descriptor.Field(x => x.About);
            descriptor.Field(x => x.Status);
            descriptor.Field(x => x.EventStartDateTimeUtc).Name("eventStartDateTime");
            descriptor.Field(x => x.EventEndDateTimeUtc).Name("eventEndDateTime");
            descriptor.Field(x => x.RegistrationStartDateTimeUtc).Name("registrationStartDateTime");
            descriptor.Field(x => x.RegistrationEndDateTimeUtc).Name("registrationEndDateTime");
            descriptor.Field(x => x.Speakers);
            descriptor.Field(x => x.Sponsors);
        }
    }
}
