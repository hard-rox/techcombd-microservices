﻿using Event.Application.Commands;
using Event.Domain.Enums;
using HotChocolate.Types;

namespace Event.GraphQL.Inputs
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class CreateEventInput : InputObjectType<CreateEventCommand>
    {
        protected override void Configure(IInputObjectTypeDescriptor<CreateEventCommand> descriptor)
        {
            base.Configure(descriptor);

            descriptor.Field(c => c.Title)
                .Type<NonNullType<StringType>>();
            descriptor.Field(c => c.About)
                .Type<NonNullType<StringType>>();
            descriptor.Field(c => c.EventStartDateTime)
                .Type<DateTimeType>();
            descriptor.Field(c => c.EventEndDateTime)
                .Type<DateTimeType>();
            descriptor.Field(c => c.RegistrationStartDateTime)
                .Type<DateTimeType>();
            descriptor.Field(c => c.RegistrationEndDateTime)
                .Type<DateTimeType>();
            descriptor.Field(c => c.SeatCapacity)
                .Type<IntType>();
            descriptor.Field(c => c.Type)
                .Type<EnumType<EventType>>();
            descriptor.Field(c => c.IsRegistrationRequired)
                .Type<BooleanType>();
            descriptor.Field(c => c.SpeakerProfileIds)
                .Type<ListType<NonNullType<UuidType>>>();
            descriptor.Field(c => c.SponsorProfileIds)
                .Type<ListType<NonNullType<UuidType>>>();
        }
    }
}
