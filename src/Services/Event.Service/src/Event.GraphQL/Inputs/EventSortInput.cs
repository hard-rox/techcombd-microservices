﻿using HotChocolate.Data.Sorting;

namespace Event.GraphQL.Inputs
{
    public class EventSortInput : SortInputType<Event.Domain.Entities.Event>
    {
        protected override void Configure(ISortInputTypeDescriptor<Domain.Entities.Event> descriptor)
        {
            base.Configure(descriptor);
            descriptor.BindFieldsExplicitly();

            descriptor.Field(x => x.Title);
            descriptor.Field(x => x.EventStartDateTimeUtc).Name("eventStartDateTime");
            descriptor.Field(x => x.EventEndDateTimeUtc).Name("eventEndDateTime");
            descriptor.Field(x => x.RegistrationStartDateTimeUtc).Name("registrationStartDateTime");
            descriptor.Field(x => x.RegistrationEndDateTimeUtc).Name("registrationEndDateTime");
        }
    }
}
