﻿using Event.Domain.Entities;
using HotChocolate.Data.Filters;

namespace Event.GraphQL.Inputs
{
    public class EventSponsorFilterInput : FilterInputType<EventSponsor>
    {
        protected override void Configure(IFilterInputTypeDescriptor<EventSponsor> descriptor)
        {
            base.Configure(descriptor);
            descriptor.BindFieldsExplicitly();

            descriptor.Field(x => x.SponsorProfileGuid).Name("id");
        }
    }
}
