﻿using Event.Domain.Entities;
using HotChocolate.Data.Filters;

namespace Event.GraphQL.Inputs
{
    public class EventSpeakerFilterInput : FilterInputType<EventSpeaker>
    {
        protected override void Configure(IFilterInputTypeDescriptor<EventSpeaker> descriptor)
        {
            base.Configure(descriptor);
            descriptor.BindFieldsExplicitly();

            descriptor.Field(x => x.SpeakerProfileGuid).Name("id");
        }
    }
}
