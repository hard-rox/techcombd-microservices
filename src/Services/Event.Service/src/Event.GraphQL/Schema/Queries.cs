﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Event.Application.Queries;
using Event.GraphQL.Inputs;
using HotChocolate;
using HotChocolate.Data;
using HotChocolate.Types;
using MediatR;

namespace Event.GraphQL.Schema
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class Queries
    {
        [UseOffsetPaging]
        [UseProjection]
        [UseFiltering(typeof(EventFilterInput))]
        [UseSorting]
        public async Task<IQueryable<Domain.Entities.Event>> GetEvents([Service] IMediator mediator)
        {
            return await mediator.Send(new GetEventsQuery());
        }

        [UseFirstOrDefault]
        [UseProjection]
        public async Task<IQueryable<Domain.Entities.Event>> GetEvent([Service] IMediator mediator, [GraphQLNonNullType]Guid id)
        {
            return await mediator.Send(new GetEventsFromIdQuery(id));
        }
    }
}
