﻿using System.Threading.Tasks;
using Common.Constants;
using Event.Application.Commands;
using Event.GraphQL.Payloads;
using HotChocolate;
using HotChocolate.AspNetCore.Authorization;
using MediatR;

namespace Event.GraphQL.Schema
{
    // ReSharper disable once ClassNeverInstantiated.Global
    [Authorize(Policy = "Admin")]
    public class Mutations
    {
        public async Task<CreateEventPayload> CreateEventAsync([Service] IMediator mediator,
            [GraphQLNonNullType] CreateEventCommand input)
        {
            var @event = await mediator.Send(input);
            return new CreateEventPayload(@event);
        }

        public async Task<UpdateEventPayload> UpdateEventAsync([Service] IMediator mediator,
            [GraphQLNonNullType] UpdateEventCommand input)
        {
            var updatedEvent = await mediator.Send(input);
            return new UpdateEventPayload(updatedEvent);
        }

        public async Task<UpdateEventPayload> UpdateEventStatusAsync([Service] IMediator mediator,
            [GraphQLNonNullType] UpdateEventStatusCommand input)
        {
            var updatedEvent = await mediator.Send(input);
            return new UpdateEventPayload(updatedEvent);
        }
    }
}
