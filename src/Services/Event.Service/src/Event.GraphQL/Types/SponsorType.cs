﻿using Event.Domain.Entities;
using HotChocolate.Types;

namespace Event.GraphQL.Types
{
    public class SponsorType : ObjectType<EventSponsor>
    {
        protected override void Configure(IObjectTypeDescriptor<EventSponsor> descriptor)
        {
            base.Configure(descriptor);
            descriptor.BindFieldsExplicitly();

            descriptor.Field(e => e.SponsorProfileGuid)
                .Name("id")
                .Type<NonNullType<UuidType>>();
        }
    }
}
