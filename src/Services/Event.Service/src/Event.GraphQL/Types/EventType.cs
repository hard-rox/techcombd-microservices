﻿using Common.Constants;
using HotChocolate.Types;

namespace Event.GraphQL.Types
{
    public class EventType : ObjectType<Domain.Entities.Event>
    {
        protected override void Configure(IObjectTypeDescriptor<Domain.Entities.Event> descriptor)
        {
            base.Configure(descriptor);

            descriptor.Field(e => e.EntityGuid)
                .Name("id")
                .Type<NonNullType<UuidType>>();
            descriptor.Field(e => e.EventStartDateTimeUtc)
                .Type<DateTimeType>()
                .Name("eventStartDateTime");
            descriptor.Field(e => e.EventEndDateTimeUtc)
                .Type<DateTimeType>()
                .Name("eventEndDateTime");
            descriptor.Field(e => e.RegistrationStartDateTimeUtc)
                .Type<DateTimeType>()
                .Name("registrationStartDateTime");
            descriptor.Field(e => e.RegistrationEndDateTimeUtc)
                .Type<DateTimeType>()
                .Name("registrationEndDateTime");
            descriptor.Field(e => e.Speakers)
                .Type<ListType<SpeakerType>>();
            descriptor.Field(e => e.Sponsors)
                .Type< ListType<SponsorType>>();
            descriptor.Field(e => e.Status)
                .Authorize("Admin");

            descriptor.Ignore(e => e.Id);
            descriptor.Ignore(e => e.CreatedAtUtc);
            descriptor.Ignore(e => e.CreatedBy);
            descriptor.Ignore(e => e.LastModifiedAtUtc);
            descriptor.Ignore(e => e.LastModifiedBy);
        }
    }
}
