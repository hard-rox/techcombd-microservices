﻿using Event.Domain.Entities;
using HotChocolate.Types;

namespace Event.GraphQL.Types
{
    public class SpeakerType : ObjectType<EventSpeaker>
    {
        protected override void Configure(IObjectTypeDescriptor<EventSpeaker> descriptor)
        {
            base.Configure(descriptor);
            descriptor.BindFieldsExplicitly();

            descriptor.Field(e => e.SpeakerProfileGuid)
                .Name("id")
                .Type<NonNullType<UuidType>>();
        }
    }
}
