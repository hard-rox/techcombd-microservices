﻿using Common.Constants;
using Event.GraphQL.Helpers;
using IdentityModel;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Event.GraphQL.Configs
{
    public static class AuthConfig
    {
        public static void AddIdentityServerAuth(this IServiceCollection services, IConfiguration configuration)
        {
            var settings = configuration
                .GetSection("AppSettings").Get<AppSettings>();
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.Authority = settings.IdentityServerConfig.Issuer;
                    options.Audience = settings.IdentityServerConfig.Audience;
                });
            services.AddAuthorization(options =>
            {
                options.AddPolicy("Admin", policy =>
                {
                    policy.RequireAssertion(context =>
                    {
                        if (context.User.HasClaim(x => x.Type == "client_id" && x.Value == "cron-worker")) return true;
                        else if(context.User.Identity.IsAuthenticated && context.User.IsInRole(UserRoles.Admin)) return true;
                        return false;
                    });
                });
            });
        }
    }

}
