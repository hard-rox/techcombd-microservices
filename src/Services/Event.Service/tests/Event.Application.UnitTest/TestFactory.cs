﻿using Event.Application.Interfaces;
using Event.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Moq;

namespace Event.Application.UnitTest
{
    public static class TestFactory
    {
        public static IEventDataContext GetContext()
        {
            var options = new DbContextOptionsBuilder()
               .UseInMemoryDatabase("TestDb")
               .Options;
            return new TestContext(options);
        }

        public static ILogger<T> GetLogger<T>()
        {
            return new Mock<ILogger<T>>().Object;
        }
    }

    public class TestContext : DbContext, IEventDataContext
    {
        public TestContext(DbContextOptions options) : base(options){}
        public DbSet<Domain.Entities.Event> Events { get; set; }
        public DbSet<EventSpeaker> EventSpeakers { get; set; }
        public DbSet<EventSponsor> EventSponsors { get; set; }
    }
}
