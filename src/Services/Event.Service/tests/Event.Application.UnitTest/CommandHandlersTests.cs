﻿using Event.Application.Interfaces;
using MassTransit;
using Moq;

namespace Event.Application.UnitTest
{
    public class CommandHandlersTests
    {
        private readonly Mock<ICurrentUserService> mockCurrentUserService = new();
        private readonly Mock<IBus> mockBus = new();

        //[Fact]
        //public async Task CreateEventCommand_Normally_CreatesEvent()
        //{
        //    var context = TestFactory.GetContext();
        //    var handler = new CreateEventCommandHandler(context, mockCurrentUserService.Object, mockBus.Object);
        //    var name = DateTime.Now.ToLongDateString();
        //    var command = new CreateEventCommand(name, "Test", "Test", null);

        //    var result = await handler.Handle(command, default);
        //    var collection = context.Events.ToList();

        //    Assert.NotEmpty(collection);
        //    Assert.Equal(name, result.Name);
        //}

        //[Fact]
        //public async Task CreateEventCommand_OnDuplicateName_ThrowsResponseException()
        //{
        //    var context = TestFactory.GetContext();
        //    context.Events.AddRange(new[]
        //    {
        //        new Domain.Entities.Event(){Name = "Test", RouteKey = "test"}
        //    });
        //    await context.SaveChangesAsync();
        //    var command = new CreateEventCommand("Test", "Test", "Test", null);
        //    var handler = new CreateEventCommandHandler(context, mockCurrentUserService.Object, mockBus.Object);

        //    var exception = await Assert.ThrowsAsync<ResponseException>(() => handler.Handle(command, default));
        //}
    }
}
