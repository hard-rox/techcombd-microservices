﻿using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using PushNotification.Application.Interfaces;
using PushNotification.Domain.Entities;
using PushNotification.Infrastructure.SignalRHubs;
using System.Threading.Tasks;

namespace PushNotification.Infrastructure.Services
{
    internal class NotificationService : INotificationService
    {
        private readonly IHubContext<PrivateHub> _privateHubContext;
        private readonly IHubContext<PublicHub> _publicHubContext;

        public NotificationService(IHubContext<PrivateHub> echoHubContext, IHubContext<PublicHub> publicHubContext)
        {
            _privateHubContext = echoHubContext;
            _publicHubContext = publicHubContext;
        }

        public async Task TriggerNotificationAsync(Notification notification)
        {
            var notificationObject = new
            {
                Id = notification.EntityGuid,
                notification.Title,
                notification.Text,
                IssuedAtUtc = notification.DeliveredAtUtc
            };
            var notificationJson = JsonConvert.SerializeObject(notificationObject, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            });
            if (string.IsNullOrEmpty(notification.AudienceUserId))
            {
                await _privateHubContext.Clients.All
                    .SendAsync("Notification", notificationObject);
                await _publicHubContext.Clients.All
                    .SendAsync("Notification", notificationObject);
            }
            else
            {
                await _privateHubContext.Clients.Group(notification.AudienceUserId)
                    .SendAsync("Notification", notificationJson);
            }
        }
    }
}
