﻿using IdentityModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;

namespace PushNotification.Infrastructure.SignalRHubs
{
    [Authorize]
    public class PrivateHub : Hub
    {
        private readonly ILogger<PrivateHub> _logger;

        public PrivateHub(ILogger<PrivateHub> logger)
        {
            _logger = logger;
        }

        public override async Task OnConnectedAsync()
        {
            var userId = Context.User?.Claims?.First(x => x.Type == JwtClaimTypes.Subject).Value;
            _logger.LogInformation($"Authorized user connected with id: {userId}");
            await Groups.AddToGroupAsync(Context.ConnectionId, userId);
            await Clients.Client(Context.ConnectionId)
            .SendAsync($"Connected", $"Authorized user {userId} connected to Echo Hub with Id: {Context.ConnectionId}.");
            await base.OnConnectedAsync();
        }

        public override async Task OnDisconnectedAsync(System.Exception exception)
        {
            var userId = Context.User?.Claims?.First(x => x.Type == JwtClaimTypes.Subject).Value;
            await Clients.Client(Context.ConnectionId)
                .SendAsync($"Disconnected", $"Id: {Context.ConnectionId} disconnected.");
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, userId);
            await base.OnDisconnectedAsync(exception);
        }
    }
}
