﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace PushNotification.Infrastructure.SignalRHubs
{
    public class PublicHub : Hub
    {
        private readonly ILogger<PublicHub> _logger;

        public PublicHub(ILogger<PublicHub> logger)
        {
            _logger = logger;
        }

        public override async Task OnConnectedAsync()
        {
            await Clients.Client(Context.ConnectionId)
                .SendAsync($"Connected", $"Connected to Broadcast Hub with Id: {Context.ConnectionId}.");
            _logger.LogInformation($"Connected to Broadcast Hub with Id: {Context.ConnectionId}.");
            await base.OnConnectedAsync();
        }

        public override async Task OnDisconnectedAsync(System.Exception exception)
        {
            await Clients.Client(Context.ConnectionId)
                .SendAsync($"Disconnected", $"Id: {Context.ConnectionId} disconnected.");
            _logger.LogInformation($"Disconnected", $"Id: {Context.ConnectionId} disconnected.");
            await base.OnDisconnectedAsync(exception);
        }
    }
}
