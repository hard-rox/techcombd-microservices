﻿using MediatR;
using Microsoft.Extensions.Logging;
using PushNotification.Application.Commands;
using PushNotification.Application.Interfaces;
using PushNotification.Domain.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace PushNotification.Application.Handlers.CommandHandlers
{
    internal class TriggerPushNotificationCommandHandler : IRequestHandler<TriggerPushNotificationCommand, bool>
    {
        private readonly ILogger<TriggerPushNotificationCommandHandler> _logger;
        private readonly IPushNotificationDataContext pushNotificationDataContext;
        private readonly INotificationService notificationService;

        public TriggerPushNotificationCommandHandler(IPushNotificationDataContext pushNotificationDataContext, INotificationService notificationService, ILogger<TriggerPushNotificationCommandHandler> logger)
        {
            this.pushNotificationDataContext = pushNotificationDataContext;
            this.notificationService = notificationService;
            _logger = logger;
        }

        public async Task<bool> Handle(TriggerPushNotificationCommand request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("Triggering new notification", request);
            var notification = new Notification()
            {
                AudienceUserId = request.AudienceUserId,
                DeliveredAtUtc = System.DateTime.UtcNow,
                ImageUrl = request.ImageUrl,
                IsSuccessfullyDelivered = true,
                Text = request.Text,
                Title = request.Title,
                Url = request.Url,
            };
            await pushNotificationDataContext.NotificationLog.AddAsync(notification);
            var rowsUpdated = await pushNotificationDataContext.SaveChangesAsync();
            if (rowsUpdated <= 0) _logger.LogError("Notification couldn't persisted.", notification);
            
            await notificationService.TriggerNotificationAsync(notification);
            return true;
        }
    }
}
