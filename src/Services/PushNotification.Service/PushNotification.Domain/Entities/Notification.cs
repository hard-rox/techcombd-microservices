﻿using PushNotification.Domain.Common;
using System;

namespace PushNotification.Domain.Entities
{
    public class Notification : AuditableEntity
    {
        public long Id { get; set; }
        public string AudienceUserId { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public string Url { get; set; }
        public string ImageUrl { get; set; }
        public bool IsSuccessfullyDelivered { get; set; }
        public DateTime DeliveredAtUtc { get; set; }
    }
}
