﻿using MassTransit;
using PushNotification.Web.Consumers;
using PushNotification.Web.Helpers;

namespace PushNotification.Web.Configs
{
    internal static class EventBusConfig
    {
        internal static void ConfigureMassTransitRabbitMQ(this IServiceCollection services, IConfiguration configuration)
        {
            var settings = configuration
                .GetSection("AppSettings").Get<AppSettings>();
            services.AddMassTransit(config =>
            {
                config.AddConsumer<EventCreatedConsumer>();
                config.AddConsumer<RedundantEventCreatedConsumer>();
                config.AddConsumer<NotificationPushedConsumer>();
                config.UsingRabbitMq((ctx, cfg) =>
                {
                    cfg.Host(settings.RabbitMQ.Host, cfg =>
                    {
                        cfg.Username(settings.RabbitMQ.UserName);
                        cfg.Password(settings.RabbitMQ.Password);
                    });

                    cfg.ReceiveEndpoint(Common.BusEndpoints.PushNotificationServiceEndpoint, x =>
                    {
                        x.ConfigureConsumer<EventCreatedConsumer>(ctx);
                        x.ConfigureConsumer<RedundantEventCreatedConsumer>(ctx);
                        x.ConfigureConsumer<NotificationPushedConsumer>(ctx);
                    });
                });
            });
            services.AddMassTransitHostedService();
        }
    }
}
