﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using PushNotification.Web.Helpers;

namespace PushNotification.Web.Configs
{
    public static class AuthConfig
    {
        public static void AddIdentityServerAuth(this IServiceCollection services, IConfiguration configuration)
        {
            var settings = configuration
                .GetSection("AppSettings").Get<AppSettings>();
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.Authority = settings.IdentityServerConfig.Issuer;
                    options.Audience = settings.IdentityServerConfig.Audience;
                });
            services.AddAuthorization();
        }
    }

}
