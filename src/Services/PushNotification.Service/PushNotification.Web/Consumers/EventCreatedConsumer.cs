﻿using Common.Events;
using MassTransit;
using MediatR;
using PushNotification.Application.Commands;

namespace PushNotification.Web.Consumers
{
    public class EventCreatedConsumer : IConsumer<EventCreated>
    {
        private readonly ILogger<EventCreatedConsumer> _logger;
        private readonly IMediator mediator;

        public EventCreatedConsumer(ILogger<EventCreatedConsumer> logger, IMediator mediator)
        {
            _logger = logger;
            this.mediator = mediator;
        }

        public async Task Consume(ConsumeContext<EventCreated> context)
        {
            _logger.LogInformation("Handling Event creating notification....");

            try
            {
                await mediator.Send(new TriggerPushNotificationCommand(
                    "New event creation in progress",
                    "New event creation in progress",
                    audienceUserId: context.Message.CreatedBy));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, ex);
            }
            finally
            {
                _logger.LogInformation("Handled Event creating notification....");
            }
        }
    }
}
