﻿using Common.Events;
using MassTransit;
using MediatR;
using PushNotification.Application.Commands;

namespace PushNotification.Web.Consumers
{
    public class RedundantEventCreatedConsumer : IConsumer<RedundantEventCreated>
    {
        private readonly ILogger<RedundantEventCreatedConsumer> _logger;
        private readonly IMediator mediator;

        public RedundantEventCreatedConsumer(ILogger<RedundantEventCreatedConsumer> logger, IMediator mediator)
        {
            _logger = logger;
            this.mediator = mediator;
        }
        public async Task Consume(ConsumeContext<RedundantEventCreated> context)
        {
            _logger.LogInformation("Handling Push Notification Event....");

            try
            {
                await mediator.Send(new TriggerPushNotificationCommand(
                    "New event creaed",
                    "New event creaed",
                    audienceUserId: context.Message.CreatedBy));
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex.Message, ex);
            }
            finally
            {
                _logger.LogInformation("Handled Push Notification Event....");
            }
        }
    }
}
