﻿using Common.Events;
using MassTransit;
using MediatR;
using PushNotification.Application.Commands;

namespace PushNotification.Web.Consumers
{
    public class NotificationPushedConsumer : IConsumer<NotificationPushed>
    {

        private readonly ILogger<NotificationPushedConsumer> _logger;
        private readonly IMediator mediator;
        public NotificationPushedConsumer(ILogger<NotificationPushedConsumer> logger, IMediator mediator)
        {
            _logger = logger;
            this.mediator = mediator;
        }

        public async Task Consume(ConsumeContext<NotificationPushed> context)
        {
            _logger.LogInformation("Handling Push notification....");

            try
            {
                await mediator.Send(new TriggerPushNotificationCommand(
                    context.Message.Title,
                    context.Message.Text,
                    audienceUserId: context.Message.AudienceUserId));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, ex);
            }
            finally
            {
                _logger.LogInformation("Handled Push notification....");
            }
        }
    }
}
