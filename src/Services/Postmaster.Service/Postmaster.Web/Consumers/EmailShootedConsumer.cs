﻿using Common.Events;
using MailKit.Net.Smtp;
using MassTransit;
using MimeKit;
using Postmaster.Web.Helpers;

namespace Postmaster.Web.Consumers
{
    public class EmailShootedConsumer : IConsumer<EmailShooted>
    {
        private readonly ILogger<EmailShootedConsumer> logger;
        private readonly IConfiguration configuration;

        public EmailShootedConsumer(ILogger<EmailShootedConsumer> logger, IConfiguration configuration)
        {
            this.logger = logger;
            this.configuration = configuration;
        }

        public async Task Consume(ConsumeContext<EmailShooted> context)
        {
            logger.LogInformation("Handling Email shoot....");
            var emailOptions = configuration.GetSection("AppSettings").Get<AppSettings>().EmailOptions;

            try
            {
                MimeMessage email = new()
                {
                    Sender = MailboxAddress.Parse(emailOptions.SenderEmail)
                };

                var receiverEmailList = context.Message.ReceiverEmails.Select(receiver => MailboxAddress.Parse(receiver));
                email.To.AddRange(receiverEmailList);

                //emailMessage?.CCMail?.Split(",")?.ToList()?.ForEach(cc => email.Cc.Add(MailboxAddress.Parse(cc)));
                //emailMessage?.BCCMail?.Split(",")?.ToList()?.ForEach(bcc => email.Bcc.Add(MailboxAddress.Parse(bcc)));

                email.Subject = context.Message.Subject;
                BodyBuilder builder = new()
                {
                    HtmlBody = context.Message.Body
                };

                //if (!string.IsNullOrEmpty(emailMessage.AttachmentPaths))
                //{
                //    var filePaths = emailMessage.AttachmentPaths.Split(",");
                //    foreach (var filePath in filePaths)
                //    {
                //        if (File.Exists(filePath))
                //        {
                //            builder.Attachments.Add(filePath);
                //        }
                //    }
                //}

                email.Body = builder.ToMessageBody();

                using SmtpClient smtp = new();
                await smtp.ConnectAsync(emailOptions.HostServer, emailOptions.SmtpPort);
                if (!string.IsNullOrEmpty(emailOptions.Password))
                    await smtp.AuthenticateAsync(emailOptions.UserName, emailOptions.Password);
                await smtp.SendAsync(email);
                await smtp.DisconnectAsync(true);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message, ex);
            }
            finally
            {
                logger.LogInformation("Handled Email shoot....");
            }
        }
    }
}
