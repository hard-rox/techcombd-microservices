﻿using MassTransit;
using Postmaster.Web.Consumers;
using Postmaster.Web.Helpers;

namespace Postmaster.Web.Configs
{
    internal static class EventBusConfig
    {
        internal static void ConfigureMassTransitRabbitMQ(this IServiceCollection services, IConfiguration configuration)
        {
            var settings = configuration
                .GetSection("AppSettings").Get<AppSettings>();
            services.AddMassTransit(config =>
            {
                config.AddConsumer<EmailShootedConsumer>();
                config.UsingRabbitMq((ctx, cfg) =>
                {
                    cfg.Host(settings.RabbitMQ.Host, cfg =>
                    {
                        cfg.Username(settings.RabbitMQ.UserName);
                        cfg.Password(settings.RabbitMQ.Password);
                    });

                    cfg.ReceiveEndpoint(Common.BusEndpoints.PostmasterServiceEndpoint, x =>
                    {
                        x.ConfigureConsumer<EmailShootedConsumer>(ctx);
                    });
                });
            });
            services.AddMassTransitHostedService();
        }
    }
}
