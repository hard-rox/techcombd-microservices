﻿namespace Postmaster.Web.Helpers
{
    public class AppSettings
    {
        public IdentityServerConfig IdentityServerConfig { get; set; }
        public RabbitMQ RabbitMQ { get; set; }
        public EmailOptions EmailOptions { get; set; }
    }

    public class IdentityServerConfig
    {
        public string Audience { get; set; }
        public string Issuer { get; set; }
    }

    public class RabbitMQ
    {
        public string Host { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }

    public class EmailOptions
    {
        public string SenderEmail { get; set; }
        public string HostServer { get; set; }
        public int SmtpPort { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool IsSSL { get; set; }
        public int TryLimit { get; set; }
    }
}
