﻿using Common.Constants;
using EventRegistration.Application.Commands;
using EventRegistration.GraphQL.Payloads;
using HotChocolate;
using HotChocolate.AspNetCore.Authorization;
using MediatR;
using System.Threading.Tasks;

namespace EventRegistration.GraphQL.Schema
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class Mutations
    {
        public async Task<EventRegistrationPayload> Registration([Service] IMediator mediator,
            EventRegistrationCommand input)
        {
            var result = await mediator.Send(input);
            return result
                ? new EventRegistrationPayload(true, "Successful")
                : new EventRegistrationPayload(false, "Unsuccessful");
        }

        [Authorize(Roles = new string[] { UserRoles.Developer, UserRoles.Admin })]
        public async Task<UpdateEventRegistrationPayload> UpdateRegistrationStatusAsync([Service] IMediator mediator,
            [GraphQLNonNullType] UpdateRegistrationStatusCommand input)
        {
            var updated = await mediator.Send(input);
            return new UpdateEventRegistrationPayload(updated);
        }
    }
}
