﻿using Common.Constants;
using EventRegistration.Application.Queries;
using EventRegistration.Domain.Entities;
using HotChocolate;
using HotChocolate.AspNetCore.Authorization;
using HotChocolate.Data;
using HotChocolate.Types;
using MediatR;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace EventRegistration.GraphQL.Schema
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class Queries
    {
        [Authorize(Roles = new string[] { UserRoles.Developer, UserRoles.Admin })]
        [UseOffsetPaging]
        [UseProjection]
        [UseFiltering]
        [UseSorting]
        public async Task<IQueryable<Registration>> GetEventRegistrationsAsync([Service] IMediator mediator, [GraphQLNonNullType]Guid eventId)
        {
            return await mediator.Send(new GetEventRegistrationsQuery(eventId));
        }
    }
}
