﻿using EventRegistration.Domain.Entities;
using HotChocolate.Types;

namespace EventRegistration.GraphQL.Types
{
    public class RegistrationType : ObjectType<Registration>
    {
        protected override void Configure(IObjectTypeDescriptor<Registration> descriptor)
        {
            base.Configure(descriptor);

            descriptor.Field(e => e.EntityGuid)
                .Name("id")
                .Type<NonNullType<UuidType>>();
            descriptor.Field(e => e.Name).Type<NonNullType<StringType>>();
            descriptor.Field(e => e.Email).Type<NonNullType<StringType>>();
            descriptor.Field(e => e.PhoneNumber).Type<NonNullType<StringType>>();
            descriptor.Field(e => e.CreatedAtUtc)
                .Name(@"registeredAt")
                .Type<NonNullType<DateTimeType>>();

            descriptor.Ignore(e => e.Id);
            descriptor.Ignore(e => e.EventId);
            descriptor.Ignore(e => e.Event);
            descriptor.Ignore(e => e.CreatedBy);
            descriptor.Ignore(e => e.LastModifiedAtUtc);
            descriptor.Ignore(e => e.LastModifiedBy);
        }
    }
}
