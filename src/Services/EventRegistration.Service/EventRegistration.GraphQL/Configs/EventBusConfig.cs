﻿using EventRegistration.GraphQL.Consumers;
using EventRegistration.GraphQL.Helpers;
using MassTransit;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace EventRegistration.GraphQL.Configs
{
    internal static class EventBusConfig
    {
        internal static void ConfigureMassTransitRabbitMQ(this IServiceCollection services, IConfiguration configuration)
        {
            var settings = configuration
                .GetSection("AppSettings").Get<AppSettings>();
            services.AddMassTransit(config =>
            {
                config.AddConsumer<EventCreatedConsumer>();
                config.AddConsumer<EventUpdatedConsumer>();
                config.UsingRabbitMq((ctx, cfg) =>
                {
                    cfg.Host(settings.RabbitMQ.Host, cfg =>
                    {
                        cfg.Username(settings.RabbitMQ.UserName);
                        cfg.Password(settings.RabbitMQ.Password);
                    });

                    cfg.ReceiveEndpoint(Common.BusEndpoints.EventRegistrationServiceEndpoint, x =>
                    {
                        x.ConfigureConsumer<EventCreatedConsumer>(ctx);
                        x.ConfigureConsumer<EventUpdatedConsumer>(ctx);
                    });
                });
            });
            services.AddMassTransitHostedService();
        }
    }
}
