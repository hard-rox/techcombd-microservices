﻿using EventRegistration.Domain.Entities;
using HotChocolate.Data;
using System.Linq;

namespace EventRegistration.GraphQL.Payloads
{
    public class UpdateEventRegistrationPayload
    {
        public UpdateEventRegistrationPayload(IQueryable<Registration> updatedRegistration)
        {
            UpdatedRegistration = updatedRegistration;
        }
        [UseFirstOrDefault]
        [UseProjection]
        public IQueryable<Registration> UpdatedRegistration { get; private set; }
    }
}
