﻿namespace EventRegistration.GraphQL.Helpers
{
    public class AppSettings
    {
        public IdentityServerConfig IdentityServerConfig { get; set; }
        public RabbitMQ RabbitMQ { get; set; }
    }

    public class IdentityServerConfig
    {
        public string Audience { get; set; }
        public string Issuer { get; set; }
    }

    public class RabbitMQ
    {
        public string Host { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
