﻿using Common.Events;
using EventRegistration.Application.Commands;
using MassTransit;
using MediatR;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace EventRegistration.GraphQL.Consumers
{
    public class EventUpdatedConsumer : IConsumer<EventUpdated>
    {
        private readonly ILogger<EventUpdatedConsumer> _logger;
        private readonly IMediator _mediator;
        private readonly IBus bus;

        public EventUpdatedConsumer(ILogger<EventUpdatedConsumer> logger, IMediator mediator, IBus bus)
        {
            _logger = logger;
            _mediator = mediator;
            this.bus = bus;
        }

        public async Task Consume(ConsumeContext<EventUpdated> context)
        {
            _logger.LogInformation("Handling EventUpdate....");

            var succeded = await _mediator.Send(new UpdateRedundantEventCommand(
                context.Message.EventServiceEntityGuid,
                context.Message.EventTitle,
                context.Message.RegistrationStartDateTimeUtc,
                context.Message.RegistrationEndDateTimeUtc,
                context.Message.SeatCapacity,
                context.Message.Status
                ));
            if (succeded)
                await bus.Publish(new RedundantEventCreated(context.Message.EventTitle, context.Message.LastModifiedBy));

            _logger.LogInformation("Handled EventCreated....");
        }
    }
}
