﻿using Common.Events;
using EventRegistration.Application.Commands;
using MassTransit;
using MediatR;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace EventRegistration.GraphQL.Consumers
{
    public class EventCreatedConsumer : IConsumer<EventCreated>
    {
        private readonly ILogger<EventCreatedConsumer> _logger;
        private readonly IMediator _mediator;
        private readonly IBus bus;

        public EventCreatedConsumer(ILogger<EventCreatedConsumer> logger, IMediator mediator, IBus bus)
        {
            _logger = logger;
            _mediator = mediator;
            this.bus = bus;
        }

        public async Task Consume(ConsumeContext<EventCreated> context)
        {
            _logger.LogInformation("Handling EventCreated....");

            var succeded = await _mediator.Send(new CreateRedundantEventCommand(
                context.Message.EventServiceEntityGuid,
                context.Message.EventTitle,
                context.Message.RegistrationStartDateTimeUtc,
                context.Message.RegistrationEndDateTimeUtc,
                context.Message.SeatCapacity,
                context.Message.Status
                ));
            if (succeded)
                await bus.Publish(new RedundantEventCreated(context.Message.EventTitle, context.Message.CreatedBy));

            _logger.LogInformation("Handled EventCreated....");
        }
    }
}
