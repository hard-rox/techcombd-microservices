﻿using EventRegistration.Application.Commands;
using HotChocolate.Types;

namespace EventRegistration.GraphQL.Inputs
{
    public class UpdateEventRegistrationStatusInput : InputObjectType<UpdateRegistrationStatusCommand>
    {

    }
}
