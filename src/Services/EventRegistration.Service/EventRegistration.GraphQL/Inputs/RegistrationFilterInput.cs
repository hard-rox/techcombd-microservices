﻿using EventRegistration.Domain.Entities;
using HotChocolate.Data.Filters;

namespace EventRegistration.GraphQL.Inputs
{
    public class RegistrationFilterInput : FilterInputType<Registration>
    {
        protected override void Configure(IFilterInputTypeDescriptor<Registration> descriptor)
        {
            base.Configure(descriptor);
            descriptor.BindFieldsExplicitly();

            descriptor.Field(x => x.Name);
            descriptor.Field(x => x.Email);
            descriptor.Field(x => x.Organization);
            descriptor.Field(x => x.PhoneNumber);
            descriptor.Field(x => x.Profession);
            descriptor.Field(x => x.Status);
            descriptor.Field(e => e.CreatedAtUtc)
                .Name(@"registeredAt");
        }
    }
}
