﻿using EventRegistration.Application.Commands;
using EventRegistration.Application.Interfaces;
using EventRegistration.Application.Queries;
using EventRegistrations.Workers;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using ReCron;

namespace EventRegistration.Workers.Workers
{
    internal class RedundantEventSyncWorker : CronWorkerService
    {
        private readonly ILogger<RedundantEventSyncWorker> _logger;
        private readonly IGraphQLGatewayClient graphQLClient;
        private readonly IMediator mediator;
        public RedundantEventSyncWorker(IWorkerConfig<RedundantEventSyncWorker> config, ILogger<RedundantEventSyncWorker> logger, IServiceScopeFactory serviceScopeFactory) : base(config.CronExpression, config.TimeZoneInfo, logger)
        {
            _logger = logger;

            var serviceScope = serviceScopeFactory.CreateScope();
            graphQLClient = serviceScope.ServiceProvider.GetRequiredService<IGraphQLGatewayClient>();
            mediator = serviceScope.ServiceProvider.GetRequiredService<IMediator>();
        }

        protected override async Task WorkerProcess(CancellationToken stoppingToken)
        {
            _logger.LogInformation("{WorkerName} is started", nameof(RedundantEventSyncWorker));
            var eventsResult = await graphQLClient.GetEvents.ExecuteAsync(0, 100, stoppingToken);
            if (eventsResult.Errors.Any())
            {
                var error = eventsResult.Errors.FirstOrDefault();
                if(error.Extensions.Any(x => x.Value.ToString().Contains("AUTH_NOT_AUTHENTICATED")))
                {
                    _logger.LogError("{WorkerName} - Unauthorized", error.Message);
                }
                else
                {
                    _logger.LogError("{WorkerName} - Graphql request error: {Error}", error.Message);
                }
            }
            else
            {
                var events = eventsResult?.Data?.Events?.Items?.ToList();
                foreach (var @event in events)
                {
                    DateTime? eventRegStartDateTimeUtc = @event.RegistrationStartDateTime != null ? ((DateTimeOffset)@event.RegistrationStartDateTime).UtcDateTime : null;
                    DateTime? eventRegEndDateTimeUtc = @event.RegistrationEndDateTime != null ? ((DateTimeOffset)@event.RegistrationEndDateTime).UtcDateTime : null;
                    var redundantEvent = await mediator.Send(new GetRedundantEventFromEventServiceIdQuery(@event.Id), stoppingToken);
                    if (redundantEvent == null)
                        await mediator.Send(new CreateRedundantEventCommand(
                            @event.Id,
                            @event.Title,
                            eventRegStartDateTimeUtc,
                            eventRegEndDateTimeUtc,
                            @event.SeatCapacity,
                            (int)@event.Status));
                    else
                    {
                        var hasChange = redundantEvent.Title != @event.Title
                            || redundantEvent.RegistrationStartDateTimeUtc != eventRegStartDateTimeUtc
                            || redundantEvent.RegistrationEndDateTimeUtc != eventRegEndDateTimeUtc
                            || redundantEvent.SeatCapacity != @event.SeatCapacity
                            || redundantEvent.EventServiceStatus != (int)@event.Status;
                        if (hasChange)
                        {
                            await mediator.Send(new UpdateRedundantEventCommand(@event.Id, @event.Title, eventRegStartDateTimeUtc, eventRegEndDateTimeUtc, @event.SeatCapacity, (int)@event.Status));
                        }
                    }
                }
            }
            _logger.LogInformation("{WorkerName} is ended", nameof(RedundantEventSyncWorker));
        }
    }
}
