﻿using EventRegistration.Application;
using EventRegistration.Workers.Workers;
using EventRegistrations.Workers.Helpers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ReCron;

namespace EventRegistration.Workers
{
    public static class DependencyContainer
    {
        public static void AddWorkers(this IServiceCollection services, IConfiguration configuration)
        {
            var gatewayUrl = configuration["AppSettings:gatewayUrl"];
            var authServiceEndpoint = configuration["AppSettings:IdentityServerConfig:Issuer"];

            services.AddHttpClient("AuthClient", client =>
            {
                client.BaseAddress = new Uri(authServiceEndpoint);
            });

            services.AddSingleton<AuthHelper>();

            services.AddGraphQLGatewayClient()
                .ConfigureHttpClient(async (sp, client) =>
                {
                    client.BaseAddress = new Uri(gatewayUrl);
                    var authHelperService = sp.GetRequiredService<AuthHelper>();
                    var tokenType = authHelperService.GetTokenType();
                    var token = await authHelperService.GetTokenAsync();
                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue(tokenType, token);
                });

            services.AddCronWorker<RedundantEventSyncWorker>(config =>
            {
                config.CronExpression = "*/5 * * * *";
                config.TimeZoneInfo = TimeZoneInfo.Local;
            });
        }
    }
}
