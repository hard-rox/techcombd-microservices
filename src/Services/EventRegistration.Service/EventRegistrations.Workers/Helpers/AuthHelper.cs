﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace EventRegistrations.Workers.Helpers
{
    internal class AuthHelper
    {
        private readonly ILogger<AuthHelper> _logger;
        private string token = string.Empty;
        private string tokenType = "Bearer";
        private DateTime expiresIn = DateTime.Now;
        private string scopes = string.Empty;
        private readonly HttpClient httpClient;
        private const string tokenEndpoint = @"connect/token";
        private readonly FormUrlEncodedContent credentials = new(new[]
        {
            new KeyValuePair<string, string>("client_id", "cron-worker"),
            new KeyValuePair<string, string>("client_secret", "df79eea1-30f8-4398-b863-ab18a26c617a"),
            new KeyValuePair<string, string>("grant_type", "client_credentials"),
        });

        public AuthHelper(IHttpClientFactory httpClientFactory, ILogger<AuthHelper> logger)
        {
            httpClient = httpClientFactory.CreateClient("AuthClient");
            _logger = logger;

            SetTokenAsync();
        }

        private async Task SetTokenAsync()
        {
            try
            {
                var response = await httpClient.PostAsync(tokenEndpoint, credentials);
                response.EnsureSuccessStatusCode();
                var responseString = await response.Content.ReadAsStringAsync();
                var tokenDefinition = new { Access_Token = "", Expires_In = 0, Token_Type = "", Scope = "" };
                var tokenResponse = JsonConvert.DeserializeAnonymousType(responseString, tokenDefinition);
                token = tokenResponse.Access_Token;
                tokenType = tokenResponse.Token_Type;
                expiresIn = DateTime.Now.AddSeconds(tokenResponse.Expires_In);
                scopes = tokenResponse.Scope;
            }
            catch (Exception ex)
            {
                _logger.LogError("Get Token error", ex);
            }
        }

        public async Task<string> GetTokenAsync()
        {
            if (string.IsNullOrEmpty(token) || DateTime.Now > expiresIn)
            {
                await SetTokenAsync();
                return token;
            }
            return token;
        }

        public string GetTokenType()
        {
            return tokenType;
        }
    }
}
