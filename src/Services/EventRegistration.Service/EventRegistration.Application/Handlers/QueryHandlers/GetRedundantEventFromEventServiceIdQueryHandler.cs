﻿using EventRegistration.Application.Interfaces;
using EventRegistration.Application.Queries;
using EventRegistration.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace EventRegistration.Application.Handlers.QueryHandlers
{
    internal class GetRedundantEventFromEventServiceIdQueryHandler : IRequestHandler<GetRedundantEventFromEventServiceIdQuery, Event>
    {
        private readonly IEventRegistrationDataContext eventRegistrationDataContext;

        public GetRedundantEventFromEventServiceIdQueryHandler(IEventRegistrationDataContext eventRegistrationDataContext)
        {
            this.eventRegistrationDataContext = eventRegistrationDataContext;
        }

        public async Task<Event> Handle(GetRedundantEventFromEventServiceIdQuery request, CancellationToken cancellationToken)
        {
            return await eventRegistrationDataContext.Events.FirstOrDefaultAsync(x => x.EventServiceEntityGuid == request.EventServiceId, cancellationToken);
        }
    }
}
