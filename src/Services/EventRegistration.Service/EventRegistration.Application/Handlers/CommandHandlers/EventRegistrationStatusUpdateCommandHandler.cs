﻿using Common.Events;
using Common.Exceptions;
using EventRegistration.Application.Commands;
using EventRegistration.Application.Interfaces;
using EventRegistration.Domain.Entities;
using MassTransit;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace EventRegistration.Application.Handlers.CommandHandlers
{
    internal class EventRegistrationStatusUpdateCommandHandler : IRequestHandler<UpdateRegistrationStatusCommand, IQueryable<Registration>>
    {
        private readonly ILogger<EventRegistrationStatusUpdateCommandHandler> logger;
        private readonly IEventRegistrationDataContext dataContext;
        private readonly IBus bus;

        public EventRegistrationStatusUpdateCommandHandler(ILogger<EventRegistrationStatusUpdateCommandHandler> logger, IEventRegistrationDataContext dataContext, IBus bus)
        {
            this.logger = logger;
            this.dataContext = dataContext;
            this.bus = bus;
        }
        public async Task<IQueryable<Registration>> Handle(UpdateRegistrationStatusCommand request, CancellationToken cancellationToken)
        {
            var queryable = dataContext.EventRegistrations
                .Include(x => x.Event)
                .Where(x => x.EntityGuid == request.Id);
            var reg = await queryable.FirstOrDefaultAsync();
            if (reg == null) throw new ResponseException("Registration not found");

            reg.Status = request.Status;
            var updated = await dataContext.SaveChangesAsync(cancellationToken) > 0;
            if (!updated) throw new ResponseException("Couldn't update");
            if(reg.Status == Domain.Enums.RegistrationStatus.Confirmed)
            {
                var email = new EmailShooted("Registration Confirmed", $"Hello {reg.Name}\nYour registration for event {reg.Event.Title} is now confirmed.", reg.Email);
                bus.Publish(email);
            }
            if (reg.Status == Domain.Enums.RegistrationStatus.Declined)
            {
                var email = new EmailShooted("Registration Declined", $"Hello {reg.Name}\nYour registration for event {reg.Event.Title} is declined.", reg.Email);
                bus.Publish(email);
            }
            return queryable;
        }
    }
}
