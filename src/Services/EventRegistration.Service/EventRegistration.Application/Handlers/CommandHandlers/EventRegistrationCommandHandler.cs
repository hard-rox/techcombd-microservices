﻿using Common.Events;
using Common.Exceptions;
using EventRegistration.Application.Commands;
using EventRegistration.Application.Interfaces;
using EventRegistration.Domain.Entities;
using MassTransit;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace EventRegistration.Application.Handlers.CommandHandlers
{
    internal class EventRegistrationCommandHandler : IRequestHandler<EventRegistrationCommand, bool>
    {
        private readonly IEventRegistrationDataContext _context;
        private readonly IBus eventBus;

        public EventRegistrationCommandHandler(IEventRegistrationDataContext context, IBus eventBus)
        {
            _context = context;
            this.eventBus = eventBus;
        }

        public async Task<bool> Handle(EventRegistrationCommand request, CancellationToken cancellationToken)
        {
            var @event = await _context.Events.FirstOrDefaultAsync(e => e.EventServiceEntityGuid == request.EventId,
                cancellationToken: cancellationToken);
            if (@event == null || @event.EventServiceStatus != 1) throw new ResponseException("Event not found.");

            var registration =
                await _context.EventRegistrations.FirstOrDefaultAsync(
                    r => r.Email == request.Email || r.PhoneNumber == request.PhoneNumber, cancellationToken);
            if (registration != null) throw new ResponseException("Already registered with same email or phone.");

            registration = new Registration()
            {
                EventId = @event.Id,
                Name = request.Name,
                PhoneNumber = request.PhoneNumber,
                Email = request.Email,
                Profession = request.Profession,
                Organization = request.Profession,
                Status = Domain.Enums.RegistrationStatus.Pending
            };

            await _context.EventRegistrations.AddAsync(registration, cancellationToken);
            var result = await _context.SaveChangesAsync(cancellationToken);
            if(result <= 0) return false;
            var notificationEmailEvent = new EmailShooted("Registration Successfull",
                $"You have successfully registered for the event {@event.Title}. You will get registration confirmation link after registration date ends.",
                registration.Email);
            eventBus.Publish(notificationEmailEvent);
            return true;
        }
    }
}
