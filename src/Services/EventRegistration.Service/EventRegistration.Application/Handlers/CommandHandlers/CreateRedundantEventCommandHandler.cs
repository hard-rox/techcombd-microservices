﻿using Common.Events;
using EventRegistration.Application.Commands;
using EventRegistration.Application.Interfaces;
using EventRegistration.Domain.Entities;
using MassTransit;
using MediatR;
using Microsoft.Extensions.Logging;
using System.Threading;
using System.Threading.Tasks;

namespace EventRegistration.Application.Handlers.CommandHandlers
{
    internal class CreateRedundantEventCommandHandler : IRequestHandler<CreateRedundantEventCommand, bool>
    {
        private readonly ILogger<CreateRedundantEventCommandHandler> _logger;
        private readonly IEventRegistrationDataContext _context;
        private readonly ICurrentUserService _currentUserService;
        private readonly IBus _eventBus;

        public CreateRedundantEventCommandHandler(IEventRegistrationDataContext eventRegistrationDataContext, ICurrentUserService currentUserService, IBus eventBus, ILogger<CreateRedundantEventCommandHandler> logger)
        {
            _context = eventRegistrationDataContext;
            _currentUserService = currentUserService;
            _eventBus = eventBus;
            _logger = logger;
        }

        public async Task<bool> Handle(CreateRedundantEventCommand request, CancellationToken cancellationToken)
        {
            var newEvent = new Event()
            {
                EventServiceEntityGuid = request.EventServiceEntityGuid,
                Title = request.Title,
                RegistrationStartDateTimeUtc = request.RegistrationStartDateTimeUtc,
                RegistrationEndDateTimeUtc = request.RegistrationEndDateTimeUtc,
                SeatCapacity = request.SeatCapacity
            };

            await _context.Events.AddAsync(newEvent, cancellationToken);
            var result = await _context.SaveChangesAsync(cancellationToken) > 0;
            if (!result)
            {
                _logger.LogError($"Redundant event of Id: {request.EventServiceEntityGuid} couldn't be created.");
            }
            _logger.LogInformation($"Redundant event of Id: {request.EventServiceEntityGuid} created.");
            var redundantEvent = new RedundantEventCreated(newEvent.Title, newEvent.CreatedBy);
            await _eventBus.Publish(redundantEvent);
            return true;
        }
    }
}
