﻿using EventRegistration.Application.Commands;
using EventRegistration.Application.Interfaces;
using MassTransit;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Threading;
using System.Threading.Tasks;

namespace EventRegistration.Application.Handlers.CommandHandlers
{
    internal class UpdateRedundantEventCommandHandler : IRequestHandler<UpdateRedundantEventCommand, bool>
    {
        private readonly ILogger<CreateRedundantEventCommandHandler> _logger;
        private readonly IEventRegistrationDataContext _context;
        private readonly ICurrentUserService _currentUserService;
        private readonly IBus _eventBus;

        public UpdateRedundantEventCommandHandler(IEventRegistrationDataContext eventRegistrationDataContext, ICurrentUserService currentUserService, IBus eventBus, ILogger<CreateRedundantEventCommandHandler> logger)
        {
            _context = eventRegistrationDataContext;
            _currentUserService = currentUserService;
            _eventBus = eventBus;
            _logger = logger;
        }

        public async Task<bool> Handle(UpdateRedundantEventCommand request, CancellationToken cancellationToken)
        {
            var @event = await _context.Events.FirstOrDefaultAsync(x => x.EventServiceEntityGuid == request.EventServiceEntityGuid);
            if (@event == null)
            {
                _logger.LogError("This event not found", request.EventServiceEntityGuid);
                return false;
            }

            @event.EventServiceStatus = request.EventServiceStatus;
            @event.RegistrationEndDateTimeUtc = request.RegistrationEndDateTimeUtc;
            @event.RegistrationStartDateTimeUtc = request.RegistrationStartDateTimeUtc;
            @event.SeatCapacity = request.SeatCapacity;
            @event.Title = request.Title;

            var result = await _context.SaveChangesAsync(cancellationToken) > 0;
            if (!result)
            {
                _logger.LogError($"Redundant event of Id: {request.EventServiceEntityGuid} couldn't be updated.");
            }
            _logger.LogInformation($"Redundant event of Id: {request.EventServiceEntityGuid} created.");
            //var redundantEvent = new RedundantEventCreated(newEvent.Title, newEvent.CreatedBy);
            //await _eventBus.Publish(redundantEvent);
            return true;
        }
    }
}
