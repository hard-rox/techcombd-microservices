﻿using EventRegistration.Domain.Entities;
using MediatR;
using System;

namespace EventRegistration.Application.Queries
{
    public class GetRedundantEventFromEventServiceIdQuery : IRequest<Event>
    {
        public Guid EventServiceId { get; set; }

        public GetRedundantEventFromEventServiceIdQuery(Guid eventServiceId)
        {
            EventServiceId = eventServiceId;
        }
    }
}
