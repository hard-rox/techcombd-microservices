﻿using EventRegistration.Domain.Entities;
using EventRegistration.Domain.Enums;
using MediatR;
using System;
using System.Linq;

namespace EventRegistration.Application.Commands
{
    public class UpdateRegistrationStatusCommand : IRequest<IQueryable<Registration>>
    {
        public Guid Id { get; set; }
        public RegistrationStatus Status { get; set; }
    }
}
