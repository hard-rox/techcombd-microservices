﻿using MediatR;
using System;

namespace EventRegistration.Application.Commands
{
    public class CreateRedundantEventCommand : IRequest<bool>
    {
        public CreateRedundantEventCommand(Guid eventServiceEntityGuid, string title, DateTime? registrationStartDateTimeUtc, DateTime? registrationEndDateTimeUtc, int? seatCapacity, int eventServiceStatus)
        {
            EventServiceEntityGuid = eventServiceEntityGuid;
            Title = title;
            RegistrationStartDateTimeUtc = registrationStartDateTimeUtc;
            RegistrationEndDateTimeUtc = registrationEndDateTimeUtc;
            SeatCapacity = seatCapacity;
            EventServiceStatus = eventServiceStatus;
        }

        public Guid EventServiceEntityGuid { get; }
        public int EventServiceStatus { get; }
        public string Title { get; }
        public int? SeatCapacity { get; }
        public DateTime? RegistrationStartDateTimeUtc { get; }
        public DateTime? RegistrationEndDateTimeUtc { get; }
    }
}
