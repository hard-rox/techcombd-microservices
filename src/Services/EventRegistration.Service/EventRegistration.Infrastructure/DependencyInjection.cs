﻿using EventRegistration.Application.Interfaces;
using EventRegistration.Infrastructure.DbContexts;
using EventRegistration.Infrastructure.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace EventRegistration.Infrastructure
{
    public static class DependencyInjection
    {
        public static void AddInfrastructureDependencies(this IServiceCollection services, IConfiguration configuration, string dbConnectionName)
        {
            services.AddTransient<ICurrentUserService, CurrentUserService>();

            services.AddDbContext<EventRegistrationDataContext>(options =>
                options.UseSqlServer(configuration.GetConnectionString(dbConnectionName),
                    b => b.MigrationsAssembly(typeof(EventRegistrationDataContext).Assembly.FullName)), ServiceLifetime.Transient);
            services.AddTransient<IEventRegistrationDataContext>(provider => provider.GetService<EventRegistrationDataContext>());
        }
    }
}
