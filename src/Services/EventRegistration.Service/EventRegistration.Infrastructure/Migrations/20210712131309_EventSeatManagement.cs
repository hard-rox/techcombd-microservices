﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EventRegistration.Infrastructure.Migrations
{
    public partial class EventSeatManagement : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SeatCapacity",
                table: "Events",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SeatCapacity",
                table: "Events");
        }
    }
}
