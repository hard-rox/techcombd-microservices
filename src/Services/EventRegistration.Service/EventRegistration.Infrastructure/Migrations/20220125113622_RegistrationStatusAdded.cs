﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EventRegistration.Infrastructure.Migrations
{
    public partial class RegistrationStatusAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsConfirmed",
                table: "EventRegistrations");

            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "EventRegistrations",
                type: "int",
                nullable: false,
                defaultValue: 1);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Status",
                table: "EventRegistrations");

            migrationBuilder.AddColumn<bool>(
                name: "IsConfirmed",
                table: "EventRegistrations",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
