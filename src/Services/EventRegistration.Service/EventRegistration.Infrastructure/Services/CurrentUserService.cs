﻿using EventRegistration.Application.Interfaces;
using IdentityModel;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Linq;

namespace EventRegistration.Infrastructure.Services
{
    internal class CurrentUserService : ICurrentUserService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CurrentUserService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;

            if (_httpContextAccessor?.HttpContext?.User?.Identity?.IsAuthenticated ?? false)
            {
                var user = _httpContextAccessor.HttpContext.User;
                var claims = user.Claims.ToList();

                this.UserId = claims.FirstOrDefault(x => x.Type == JwtClaimTypes.Subject)?.Value;
                this.ClientId = claims.FirstOrDefault(x => x.Type == JwtClaimTypes.ClientId)?.Value;
                this.UserEmail = claims.FirstOrDefault(x => x.Type == JwtClaimTypes.Email)?.Value;
                this.PhoneNumber = claims.FirstOrDefault(x => x.Type == JwtClaimTypes.PhoneNumber)?.Value;
                this.UserName = claims.FirstOrDefault(x => x.Type == JwtClaimTypes.PreferredUserName)?.Value;
                this.IsAuthenticated = user.Identity.IsAuthenticated;
                this.Roles = claims.Where(c => c.Type == JwtClaimTypes.Role).Select(c => c?.Value).ToList();
            }
        }

        public string UserId { get; }
        public string ClientId { get; }

        public string UserEmail { get; }

        public string PhoneNumber { get; }

        public string UserName { get; }

        public string RequestOrigin { get; }

        public List<string> Roles { get; }

        public bool IsAuthenticated { get; }
    }
}
