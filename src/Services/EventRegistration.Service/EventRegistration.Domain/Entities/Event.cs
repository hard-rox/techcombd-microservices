﻿using System;
using System.Collections.Generic;
using EventRegistration.Domain.Common;

namespace EventRegistration.Domain.Entities
{
    public class Event : AuditableEntity
    {
        public long Id { get; set; }
        public Guid EventServiceEntityGuid { get; set; }
        public int EventServiceStatus { get; set; }
        public string Title { get; set; }
        public int? SeatCapacity { get; set; }
        public DateTime? RegistrationStartDateTimeUtc { get; set; }
        public DateTime? RegistrationEndDateTimeUtc { get; set; }

        public virtual ICollection<Registration> Registrations { get; set; }
    }
}
