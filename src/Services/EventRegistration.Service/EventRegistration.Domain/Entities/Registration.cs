﻿using EventRegistration.Domain.Common;
using EventRegistration.Domain.Enums;

namespace EventRegistration.Domain.Entities
{
    public class Registration : AuditableEntity
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Profession { get; set; }
        public string Organization { get; set; }
        public RegistrationStatus Status { get; set; }

        public long EventId { get; set; }
        public virtual Event Event { get; set; }
    }
}
