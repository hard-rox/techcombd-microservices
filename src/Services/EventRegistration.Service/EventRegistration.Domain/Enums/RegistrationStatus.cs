﻿namespace EventRegistration.Domain.Enums
{
    public enum RegistrationStatus
    {
        Pending = 1,
        Confirmed = 2,
        Declined = 3,
    }
}
