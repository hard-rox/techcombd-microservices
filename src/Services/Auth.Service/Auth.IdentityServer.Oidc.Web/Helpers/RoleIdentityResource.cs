﻿using IdentityModel;
using IdentityServer4.Models;

namespace Auth.IdentityServer.Oidc.Web.Helpers
{
    public class RoleIdentityResource : IdentityResource
    {
        public RoleIdentityResource()
        {
            Name = "role";
            DisplayName = "User role";
            Description = "Your user role information";
            Emphasize = true;
            UserClaims.Add(JwtClaimTypes.Role);
        }
    }
}
