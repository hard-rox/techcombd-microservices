﻿namespace Auth.IdentityServer.Oidc.Web.Helpers
{
    public class AppSettings
    {
        public string PublicAppHost { get; set; }
        public string AdminAppHost { get; set; }
        public RabbitMQ RabbitMQ { get; set; }
    }

    public class RabbitMQ
    {
        public string Host { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
