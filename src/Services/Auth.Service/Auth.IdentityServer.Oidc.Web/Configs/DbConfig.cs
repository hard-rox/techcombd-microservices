﻿using Auth.IdentityServer.Oidc.Web.Data;
using Auth.IdentityServer.Oidc.Web.Entities;
using Common.Constants;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;

namespace Auth.IdentityServer.Oidc.Web.Configs
{
    public static class DbConfig
    {
        public static void InitializeDataInDatabaseAsync(this WebApplication app)
        {
            var scope = app.Services.CreateScope();
            var context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
            context.Database.Migrate();

            var roleNames = new string[] { UserRoles.Developer, UserRoles.Admin, UserRoles.Guest };
            var roleManager = scope.ServiceProvider.GetRequiredService<RoleManager<ApplicationRole>>();
            foreach (var roleName in roleNames)
            {
                bool adminRoleExists = roleManager.RoleExistsAsync(roleName).Result;

                if (!adminRoleExists)
                {
                    _ = roleManager.CreateAsync(new ApplicationRole() { Name = roleName }).Result;
                }
            }

            if (!context.Users.Any())
            {
                var defaultPass = "123456@Aa";
                var userManager = scope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();
                var devUser = new ApplicationUser()
                {
                    UserName = "developer",
                    Email = "developer@techcombd.org"
                };
                var adminUser = new ApplicationUser()
                {
                    UserName = "admin@techcombd.org",
                    Email = "admin@techcombd.org"
                };
                _ = userManager.CreateAsync(devUser, defaultPass).Result;
                _ = userManager.AddToRoleAsync(devUser, UserRoles.Developer).Result;
                _ = userManager.AddToRoleAsync(devUser, UserRoles.Admin).Result;
                _ = userManager.AddToRoleAsync(devUser, UserRoles.Guest).Result;
                _ = userManager.CreateAsync(adminUser, defaultPass).Result;
                _ = userManager.AddToRoleAsync(adminUser, UserRoles.Admin).Result;
            }
        }
    }
}
