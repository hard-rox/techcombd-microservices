﻿using Auth.IdentityServer.Oidc.Web.Helpers;
using MassTransit;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Auth.IdentityServer.Oidc.Web.Configs
{
    internal static class EventBusConfig
    {
        internal static void ConfigureMassTransitRabbitMQ(this IServiceCollection services, IConfiguration configuration)
        {
            var settings = configuration
                .GetSection("AppSettings").Get<AppSettings>();
            services.AddMassTransit(busConfig =>
            {
                busConfig.UsingRabbitMq((ctx, cfg) =>
                {
                    cfg.Host(settings.RabbitMQ.Host, cfg =>
                    {
                        cfg.Username(settings.RabbitMQ.UserName);
                        cfg.Password(settings.RabbitMQ.Password);
                    });
                });
            });
            services.AddMassTransitHostedService();
        }
    }
}
