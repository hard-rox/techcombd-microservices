﻿using Auth.IdentityServer.Oidc.Web.Helpers;
using IdentityModel;
using IdentityServer4;
using IdentityServer4.Models;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;

namespace Auth.IdentityServer.Oidc.Web.Configs
{
    internal static class InMemoryDataSet
    {
        private static readonly string cronWorkerSecret = "df79eea1-30f8-4398-b863-ab18a26c617a";
        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
            {
                new ApiResource("event.graph", "Event GraphQL", new[] { JwtClaimTypes.Role })
                {
                    Scopes = { "event.graph" },
                },
                new ApiResource("eventregistration.graph", "Event Registration GraphQL", new[] { JwtClaimTypes.Role })
                {
                    Scopes = { "eventregistration.graph" }
                },
                new ApiResource("profile.graph", "Profile GraphQL", new[] { JwtClaimTypes.Role })
                {
                    Scopes = { "profile.graph" }
                },
                new ApiResource("pushnotification.signalr", "Push Notification SignalR", new[] { JwtClaimTypes.Role })
                {
                    Scopes = { "pushnotification.signalr" }
                }
            };
        }
        public static IEnumerable<ApiScope> GetApiScopes()
        {
            return new List<ApiScope>
            {
                new ApiScope("event.graph", "Event GraphQL"),
                new ApiScope("eventregistration.graph", "Event Registration GraphQL"),
                new ApiScope("profile.graph", "Profile GraphQL"),
                new ApiScope("pushnotification.signalr", "Push Notification SignalR"),
            };
        }
        public static IEnumerable<Client> GetClients(IConfiguration configuration)
        {
            var settings = configuration
                .GetSection("AppSettings").Get<AppSettings>();
            var clients = new List<Client>
            {
                new Client
                {
                    ClientId = "admin-app",
                    ClientName = "Angular Admin App",
                    AllowedGrantTypes = GrantTypes.Code,
                    RequireClientSecret = false,
                    RedirectUris = { settings.AdminAppHost },
                    PostLogoutRedirectUris = { settings.AdminAppHost },
                    AllowedCorsOrigins = { settings.AdminAppHost },
                    AllowOfflineAccess = true,
                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.Email,
                        JwtClaimTypes.Role,
                        "pushnotification.signalr",
                        "event.graph",
                        "eventregistration.graph",
                        "profile.graph",
                    }
                },
                new Client
                {
                    ClientId = "public-app",
                    ClientName = "ReactJS Public App",
                    AllowedGrantTypes = GrantTypes.Code,
                    RequireClientSecret = false,
                    RedirectUris = { @$"{settings.PublicAppHost}/authentication/login-callback" },
                    PostLogoutRedirectUris = { @$"{settings.PublicAppHost}" },
                    AllowedCorsOrigins = { @$"{settings.PublicAppHost}" },
                    AllowOfflineAccess=true,
                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.Email,
                        JwtClaimTypes.Role,
                        "pushnotification.signalr",
                        "event.graph",
                    }
                },
                new Client
                {
                    ClientId = "cron-worker",
                    ClientName = "Cron Worker",
                    ClientSecrets = {new Secret(cronWorkerSecret.Sha256())},
                    AllowedGrantTypes = GrantTypes.ClientCredentials,
                    RequireClientSecret = true,
                    AllowOfflineAccess = true,
                    AllowedScopes =
                    {
                        "event.graph",
                        "eventregistration.graph",
                        "profile.graph",
                    }
                },
                new Client
                {
                    ClientId = "js",
                    ClientName = "JavaScript Client",
                    AllowedGrantTypes = GrantTypes.Code,
                    RequireClientSecret = false,
                    RedirectUris = { "http://127.0.0.1:5527/callback.html" },
                    PostLogoutRedirectUris = { "http://127.0.0.1:5527/index.html" },
                    AllowedCorsOrigins = { "http://127.0.0.1:5527" },
                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Email,
                        JwtClaimTypes.Role,
                        "event.graph",
                        "profile.graph",
                        "eventregistration.graph",
                        "pushnotification.signalr"
                    }
                },
            };

            return clients;
        }
        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new IdentityResources.Email(),
                new RoleIdentityResource(),
                //new IdentityResources.Phone(),
            };
        }
    }
}
