﻿using Microsoft.AspNetCore.Identity;
using System;

namespace Auth.IdentityServer.Oidc.Web.Entities
{
    public class ApplicationUser : IdentityUser, IAuditable
    {
        public Guid EntityGuid { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime? LastModifiedAt { get; set; }
    }
}
