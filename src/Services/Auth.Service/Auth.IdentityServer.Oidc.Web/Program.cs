using Auth.IdentityServer.Oidc.Web.Configs;
using HealthChecks.UI.Client;
using MassTransit;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Sinks.SystemConsole.Themes;

// Locals
const string _dbConnectionName = "IdentityOidcDataConnection";

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddCors();
builder.Services.AddControllersWithViews();
builder.Services.ConfigureIdentityServer(builder.Environment, builder.Configuration, _dbConnectionName);
builder.Services.ConfigureHealthChecks(builder.Configuration, _dbConnectionName);
builder.Services.AddMassTransit(x =>
{
    x.UsingRabbitMq();
});

builder.Services.AddMassTransitHostedService();

// Serilog
builder.Host.UseSerilog((context, logConfig) =>
{
    var seqServerUrl = context.Configuration["Serilog:SeqServerUrl"];
    logConfig
    .Enrich.WithProperty("ApplicationContext", "Auth.Service")
    .Enrich.FromLogContext()
    .WriteTo.Console(theme: AnsiConsoleTheme.Code)
    .WriteTo.File("logs/Auth.Service_.log", rollingInterval: RollingInterval.Day)
    .WriteTo.Seq(string.IsNullOrWhiteSpace(seqServerUrl) ? "http://seq" : seqServerUrl)
    .ReadFrom.Configuration(context.Configuration);
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseCors(options =>
{
    options.AllowAnyOrigin();
    options.AllowAnyHeader();
    options.AllowAnyMethod();
});
app.UseRouting();
app.UseStaticFiles();
app.UseIdentityServer();
app.UseCookiePolicy(new CookiePolicyOptions
{
    MinimumSameSitePolicy = SameSiteMode.Lax
});

app.UseAuthorization();
app.UseEndpoints(opt =>
{
    opt.MapDefaultControllerRoute();
    opt.MapHealthChecks("/hc", new HealthCheckOptions()
    {
        Predicate = _ => true,
        ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
    });
});

app.InitializeDataInDatabaseAsync();
app.Run();
