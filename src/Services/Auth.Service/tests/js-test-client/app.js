function log() {
    document.getElementById('results').innerText = '';

    Array.prototype.forEach.call(arguments, function (msg) {
        if (msg instanceof Error) {
            msg = "Error: " + msg.message;
        }
        else if (typeof msg !== 'string') {
            msg = JSON.stringify(msg, null, 2);
        }
        document.getElementById('results').innerHTML += msg + '\r\n';
    });
}

document.getElementById("login").addEventListener("click", login, false);
document.getElementById("api").addEventListener("click", api, false);
document.getElementById("logout").addEventListener("click", logout, false);

var config = {
    // authority: "http://192.168.1.150/techcombd/staging/services/oidc-auth",
    authority: "https://localhost:44392",
    client_id: "js",
    redirect_uri: "http://127.0.0.1:5527/callback.html",
    response_type: "code",
    scope: "openid role email event.graph eventregistration.graph pushnotification.signalr",
    post_logout_redirect_uri: "http://127.0.0.1:5527/index.html",
};
var mgr = new Oidc.UserManager(config);

mgr.getUser().then(function (user) {
    if (user) {
        log("User logged in", user.profile);
        console.log(user.profile);
        document.getElementById('token').innerText = user.access_token;
    }
    else {
        log("User not logged in");
    }
});

function login() {
    mgr.signinRedirect();
}

function api() {
    mgr.getUser().then(function (user) {
        (async function () {
            const data = JSON.stringify({
                query: `{eventRegistrations(eventId:\"CF16433E-F36B-1410-8836-0076103E3D41\"){items{id name}}}`,
            });

            const response = await fetch(
                'https://localhost:44300/graphql',
                {
                    method: 'post',
                    body: data,
                    headers: {
                        'Content-Type': 'application/json',
                        'Content-Length': data.length,
                        Authorization:
                        "Bearer " + user.access_token,
                    },
                }
            );

            // const response = await fetch(
            //     'https://localhost:44349/test',
            //     {
            //         method: 'get',
            //         headers: {
            //             'Content-Type': 'application/json',
            //             'Content-Length': data.length,
            //             Authorization:
            //             "Bearer " + user.access_token,
            //         },
            //     }
            // );

            const res = await response.json();
            console.log(res);
        })();
    });
}

function logout() {
    mgr.signoutRedirect();
}