﻿using Profile.Domain.Common;
using Profile.Domain.Enums;
using System.Collections.Generic;

namespace Profile.Domain.Entities
{
    public class Organization : AuditableEntity
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string ContactPersonName { get; set; }
        public string ContactPersonPhone { get; set; }
        public string PhoneNumber { get; set; }
        public string Website { get; set; }
        public string Facebook { get; set; }
        public string LinkedIn { get; set; }
        public string LogoUrl { get; set; }
        public ProfileStatus Status { get; set; }

        public virtual ICollection<Person> Persons { get; set; }
    }
}
