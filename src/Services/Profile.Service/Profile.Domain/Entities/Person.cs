﻿using Profile.Domain.Common;
using Profile.Domain.Enums;

namespace Profile.Domain.Entities
{
    public class Person : AuditableEntity
    {
        public long Id { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string OrganizationalRole { get; set; }
        public string Twitter { get; set; }
        public string Facebook { get; set; }
        public string Git { get; set; }
        public string LinkedIn { get; set; }
        public string DpUrl { get; set; }
        public ProfileStatus Status { get; set; }

        public long? OrganizationId { get; set; }
        public virtual Organization Organization { get; set; }
    }
}
