﻿namespace Profile.Domain.Enums
{
    public enum ProfileStatus
    {
        Active = 1,
        Inactive = 2,
    }
}
