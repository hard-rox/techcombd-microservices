﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Profile.Infrastructure.Migrations
{
    public partial class PersonRemodel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TwitterProfileUrl",
                table: "Persons",
                newName: "Twitter");

            migrationBuilder.RenameColumn(
                name: "LinkedInProfileUrl",
                table: "Persons",
                newName: "LinkedIn");

            migrationBuilder.RenameColumn(
                name: "GithubProfileUrl",
                table: "Persons",
                newName: "Git");

            migrationBuilder.RenameColumn(
                name: "FacebookProfileUrl",
                table: "Persons",
                newName: "Facebook");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Twitter",
                table: "Persons",
                newName: "TwitterProfileUrl");

            migrationBuilder.RenameColumn(
                name: "LinkedIn",
                table: "Persons",
                newName: "LinkedInProfileUrl");

            migrationBuilder.RenameColumn(
                name: "Git",
                table: "Persons",
                newName: "GithubProfileUrl");

            migrationBuilder.RenameColumn(
                name: "Facebook",
                table: "Persons",
                newName: "FacebookProfileUrl");
        }
    }
}
