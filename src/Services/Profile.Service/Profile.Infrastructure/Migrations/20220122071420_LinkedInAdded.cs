﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Profile.Infrastructure.Migrations
{
    public partial class LinkedInAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "FacebookLinkedIn",
                table: "Organizations",
                newName: "LinkedIn");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "LinkedIn",
                table: "Organizations",
                newName: "FacebookLinkedIn");
        }
    }
}
