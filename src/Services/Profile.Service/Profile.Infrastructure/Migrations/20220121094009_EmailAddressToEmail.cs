﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Profile.Infrastructure.Migrations
{
    public partial class EmailAddressToEmail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "EmailAddress",
                table: "Persons",
                newName: "Email");

            migrationBuilder.RenameIndex(
                name: "IX_Persons_EmailAddress",
                table: "Persons",
                newName: "IX_Persons_Email");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Email",
                table: "Persons",
                newName: "EmailAddress");

            migrationBuilder.RenameIndex(
                name: "IX_Persons_Email",
                table: "Persons",
                newName: "IX_Persons_EmailAddress");
        }
    }
}
