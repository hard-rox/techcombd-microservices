﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Profile.Infrastructure.Migrations
{
    public partial class OrganizationIdNullable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PersonProfiles_OrganizationProfiles_OrganizationId",
                table: "PersonProfiles");

            migrationBuilder.AlterColumn<long>(
                name: "OrganizationId",
                table: "PersonProfiles",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AddForeignKey(
                name: "FK_PersonProfiles_OrganizationProfiles_OrganizationId",
                table: "PersonProfiles",
                column: "OrganizationId",
                principalTable: "OrganizationProfiles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PersonProfiles_OrganizationProfiles_OrganizationId",
                table: "PersonProfiles");

            migrationBuilder.AlterColumn<long>(
                name: "OrganizationId",
                table: "PersonProfiles",
                type: "bigint",
                nullable: false,
                defaultValue: 0L,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_PersonProfiles_OrganizationProfiles_OrganizationId",
                table: "PersonProfiles",
                column: "OrganizationId",
                principalTable: "OrganizationProfiles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
