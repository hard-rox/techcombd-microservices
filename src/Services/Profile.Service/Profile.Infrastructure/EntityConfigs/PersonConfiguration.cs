﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Profile.Domain.Entities;
using Profile.Domain.Enums;

namespace Profile.Infrastructure.EntityConfigs
{
    internal class PersonConfiguration : IEntityTypeConfiguration<Person>
    {
        public void Configure(EntityTypeBuilder<Person> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Email).IsRequired();
            builder.HasIndex(x => x.Email).IsUnique();
            builder.Property(x => x.Status).HasDefaultValue(ProfileStatus.Inactive);
            builder.HasOne(x => x.Organization).WithMany(x => x.Persons).HasForeignKey(x => x.OrganizationId);
        }
    }
}
