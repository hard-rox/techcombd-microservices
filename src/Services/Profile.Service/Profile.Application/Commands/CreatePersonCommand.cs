﻿using System;
using System.Linq;
using MediatR;
using Profile.Domain.Entities;
using Profile.Domain.Enums;

namespace Profile.Application.Commands
{
    public class CreatePersonCommand : IRequest<IQueryable<Person>>
    {
        public string Name { get; set; }
        public Guid? OrganizationId { get; set; }
        public string OrganizationName { get; set; }
        public string OrganizationalRole { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Twitter { get; set; }
        public string Facebook { get; set; }
        public string Git { get; set; }
        public string LinkedIn { get; set; }
        public string DpUrl { get; set; }
        public ProfileStatus Status { get; set; }
    }
}
