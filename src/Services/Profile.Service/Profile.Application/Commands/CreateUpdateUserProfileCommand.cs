﻿using MediatR;

namespace Profile.Application.Commands
{
    public class CreateUpdateUserProfileCommand : IRequest<bool>
    {
        public string UserId { get; }
        public string Email { get; }

        public CreateUpdateUserProfileCommand(string userId, string email)
        {
            UserId = userId;
            Email = email;
        }
    }
}
