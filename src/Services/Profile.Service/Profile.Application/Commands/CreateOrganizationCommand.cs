﻿using MediatR;
using Profile.Domain.Entities;
using Profile.Domain.Enums;
using System.Linq;

namespace Profile.Application.Commands
{
    public class CreateOrganizationCommand : IRequest<IQueryable<Organization>>
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string ContactPersonName { get; set; }
        public string ContactPersonPhone { get; set; }
        public string Website { get; set; }
        public string Facebook { get; set; }
        public string LinkedIn { get; set; }
        public string LogoUrl { get; set; }
        public ProfileStatus Status { get; set; }
    }
}
