﻿using MediatR;
using Profile.Domain.Entities;
using Profile.Domain.Enums;
using System;
using System.Linq;

namespace Profile.Application.Commands
{
    public class UpdateOrganizationStatusCommand : IRequest<IQueryable<Organization>>
    {
        public Guid Id { get; set; }
        public ProfileStatus Status { get; set; }
    }
}
