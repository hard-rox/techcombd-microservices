﻿using MediatR;
using Profile.Domain.Entities;
using System.Linq;

namespace Profile.Application.Queries
{
    public class GetOrganizationsQuery : IRequest<IQueryable<Organization>>
    {
    }
}
