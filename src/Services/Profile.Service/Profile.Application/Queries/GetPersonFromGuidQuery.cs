﻿using System;
using System.Linq;
using MediatR;
using Profile.Domain.Entities;

namespace Profile.Application.Queries
{
    public class GetPersonFromGuidQuery : IRequest<IQueryable<Person>>
    {
        public GetPersonFromGuidQuery(Guid guid)
        {
            Guid = guid;
        }

        public Guid Guid { get; }
    }
}
