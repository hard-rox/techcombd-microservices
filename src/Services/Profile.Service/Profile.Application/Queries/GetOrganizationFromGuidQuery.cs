﻿using System;
using System.Linq;
using MediatR;
using Profile.Domain.Entities;

namespace Profile.Application.Queries
{
    public class GetOrganizationFromGuidQuery : IRequest<IQueryable<Organization>>
    {
        public GetOrganizationFromGuidQuery(Guid guid)
        {
            Guid = guid;
        }

        public Guid Guid { get; }
    }
}
