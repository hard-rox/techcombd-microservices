﻿using System.Linq;
using MediatR;
using Profile.Domain.Entities;

namespace Profile.Application.Queries
{
    public class GetCurrentUserProfileQuery : IRequest<IQueryable<Person>>
    {
    }
}
