﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Profile.Application.Commands;
using Profile.Application.Interfaces;
using Profile.Domain.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace Profile.Application.Handlers.CommandHandlers
{
    internal class UpdateUserProfileCommandHandler : IRequestHandler<CreateUpdateUserProfileCommand, bool>
    {
        private readonly ILogger<UpdateUserProfileCommandHandler> _logger;
        private readonly IProfileDataContext context;

        public UpdateUserProfileCommandHandler(ILogger<UpdateUserProfileCommandHandler> logger, IProfileDataContext context)
        {
            _logger = logger;
            this.context = context;
        }
        public async Task<bool> Handle(CreateUpdateUserProfileCommand request, CancellationToken cancellationToken)
        {
            var person = await context.Persons.FirstOrDefaultAsync(x => x.Email == request.Email);
            if (person == null)
            {
                person = new Person()
                {
                    Email = request.Email,
                    UserId = request.UserId
                };
                context.Persons.Add(person);
            }
            else
            {
                person.UserId = request.UserId;
            }
            var result = await context.SaveChangesAsync() > 0;
            return result;
        }
    }
}
