﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Common.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Profile.Application.Commands;
using Profile.Application.Interfaces;
using Profile.Domain.Entities;

namespace Profile.Application.Handlers.CommandHandlers
{
    internal class UpdatePersonCommandHandler : IRequestHandler<UpdatePersonCommand, IQueryable<Person>>
    {
        private readonly IProfileDataContext _context;

        public UpdatePersonCommandHandler(IProfileDataContext context)
        {
            _context = context;
        }

        public async Task<IQueryable<Person>> Handle(UpdatePersonCommand request, CancellationToken cancellationToken)
        {
            var profile =
                await _context.Persons.FirstOrDefaultAsync(pp => pp.EntityGuid == request.Id, cancellationToken);
            if (profile == null) throw new ResponseException("Profile not found.");

            profile.Name = request.Name;
            profile.Email = request.Email;
            profile.PhoneNumber = request.PhoneNumber;
            profile.OrganizationalRole = request.OrganizationalRole;
            profile.Twitter = request.Twitter;
            profile.Facebook = request.Facebook;
            profile.Git = request.Git;
            profile.LinkedIn = request.LinkedIn;
            profile.DpUrl = request.DpUrl;
            profile.Status = request.Status;
            if (request.OrganizationId != null)
            {
                var org = await _context.Organizations.FirstOrDefaultAsync(pp => pp.EntityGuid != request.Id, cancellationToken);
                profile.OrganizationId = org.Id;
            }
            else
            {
                profile.Organization = new Organization()
                {
                    Name = request.OrganizationName
                };
            }

            var succeed = await _context.SaveChangesAsync(cancellationToken) > 0;
            if (succeed) return _context.Persons.Where(pp => pp.Id == profile.Id);
            throw new ResponseException("Couldn't update");
        }
    }
}