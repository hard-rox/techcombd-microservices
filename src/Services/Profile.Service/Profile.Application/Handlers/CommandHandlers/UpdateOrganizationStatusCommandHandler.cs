﻿using Common.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Profile.Application.Commands;
using Profile.Application.Interfaces;
using Profile.Domain.Entities;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Profile.Application.Handlers.CommandHandlers
{
    internal class UpdateOrganizationStatusCommandHandler : IRequestHandler<UpdateOrganizationStatusCommand, IQueryable<Organization>>
    {
        private readonly ILogger<UpdateOrganizationStatusCommand> _logger;
        private readonly IProfileDataContext profileDataContext;
        public UpdateOrganizationStatusCommandHandler(ILogger<UpdateOrganizationStatusCommand> logger, IProfileDataContext profileDataContext)
        {
            _logger = logger;
            this.profileDataContext = profileDataContext;
        }
        public async Task<IQueryable<Organization>> Handle(UpdateOrganizationStatusCommand request, CancellationToken cancellationToken)
        {
            var org = await profileDataContext.Organizations.FirstOrDefaultAsync(x => x.EntityGuid == request.Id);
            if (org == null) throw new ResponseException("Organization not found");

            org.Status = request.Status;
            var isUpdated = await profileDataContext.SaveChangesAsync() > 0;
            if (!isUpdated) throw new ResponseException("Couldnt updated...");
            return profileDataContext.Organizations.Where(x => x.EntityGuid == request.Id);
        }
    }
}
