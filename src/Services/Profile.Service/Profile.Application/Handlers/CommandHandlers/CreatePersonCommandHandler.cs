﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Common.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Profile.Application.Commands;
using Profile.Application.Interfaces;
using Profile.Domain.Entities;

namespace Profile.Application.Handlers.CommandHandlers
{
    internal class CreatePersonCommandHandler : IRequestHandler<CreatePersonCommand, IQueryable<Person>>
    {
        private readonly IProfileDataContext _context;
        private readonly IMapper _mapper;

        public CreatePersonCommandHandler(IProfileDataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<IQueryable<Person>> Handle(CreatePersonCommand request, CancellationToken cancellationToken)
        {
            var duplicateEmail =
                await _context.Persons.FirstOrDefaultAsync(pp => pp.Email == request.Email,
                    cancellationToken) != null;
            if (duplicateEmail) throw new ResponseException("Email already entered.");

            var org = request.OrganizationId != null ? await _context.Organizations.FirstOrDefaultAsync(x => x.EntityGuid == request.OrganizationId) 
                : new Organization()
                {
                    Name = request.OrganizationName,
                    Status = Domain.Enums.ProfileStatus.Active
                };

            var profile = new Person()
            {
                DpUrl = request.DpUrl != null ? request.DpUrl : Common.Extensions.GlobalExtensions.GetGravatarUrlFromEmail(request.Email),
                Email = request.Email,
                Facebook = request.Facebook,
                Git = request.Git,
                LinkedIn = request.LinkedIn,
                Name = request.Name,
                OrganizationId = request.OrganizationId != null ? org.Id : null,
                Organization = request.OrganizationId == null ? org : null,
                OrganizationalRole = request.OrganizationalRole,
                PhoneNumber = request.PhoneNumber,
                Status = request.Status,
                Twitter = request.Twitter,
            };
            await _context.Persons.AddAsync(profile, cancellationToken);
            var success = await _context.SaveChangesAsync(cancellationToken) > 0;
            if (success) return _context.Persons.Where(pp => pp.Email == request.Email);
            throw new ResponseException("Profile not saved.");
        }
    }
}
