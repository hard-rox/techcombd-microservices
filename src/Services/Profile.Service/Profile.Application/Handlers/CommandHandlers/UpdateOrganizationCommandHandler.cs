﻿using Common.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Profile.Application.Commands;
using Profile.Application.Interfaces;
using Profile.Domain.Entities;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Profile.Application.Handlers.CommandHandlers
{
    internal class UpdateOrganizationCommandHandler : IRequestHandler<UpdateOrganizationCommand, IQueryable<Organization>>
    {
        private readonly ILogger<UpdateOrganizationCommandHandler> _logger;
        private readonly IProfileDataContext profileDataContext;

        public UpdateOrganizationCommandHandler(ILogger<UpdateOrganizationCommandHandler> logger, IProfileDataContext profileDataContext)
        {
            _logger = logger;
            this.profileDataContext = profileDataContext;
        }

        public async Task<IQueryable<Organization>> Handle(UpdateOrganizationCommand request, CancellationToken cancellationToken)
        {
            var org = await profileDataContext.Organizations.FirstOrDefaultAsync(x => x.EntityGuid == request.Id);
            if (org == null) throw new ResponseException("No org found.");

            org.Address = request.Address;
            org.ContactPersonName = request.ContactPersonName;
            org.ContactPersonPhone = request.ContactPersonPhone;
            org.Email = request.Email;
            org.Facebook = request.Facebook;
            org.PhoneNumber = request.PhoneNumber;
            org.LinkedIn = request.LinkedIn;
            org.LogoUrl = request.LogoUrl;
            org.Name = request.Name;
            org.Status = request.Status;
            org.Website = request.Website;

            var saved = await profileDataContext.SaveChangesAsync() > 0;
            if (!saved) throw new ResponseException("Couldnt update");
            return profileDataContext.Organizations.Where(x => x.EntityGuid == request.Id).AsQueryable();
        }
    }
}
