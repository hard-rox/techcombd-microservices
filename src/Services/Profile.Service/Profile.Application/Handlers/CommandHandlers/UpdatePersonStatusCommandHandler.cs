﻿using Common.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Profile.Application.Commands;
using Profile.Application.Interfaces;
using Profile.Domain.Entities;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Profile.Application.Handlers.CommandHandlers
{
    internal class UpdatePersonStatusCommandHandler : IRequestHandler<UpdatePersonStatusCommand, IQueryable<Person>>
    {
        private readonly ILogger<UpdatePersonStatusCommandHandler> _logger;
        private readonly IProfileDataContext profileDataContext;
        public UpdatePersonStatusCommandHandler(ILogger<UpdatePersonStatusCommandHandler> logger, IProfileDataContext profileDataContext)
        {
            _logger = logger;
            this.profileDataContext = profileDataContext;
        }
        public async Task<IQueryable<Person>> Handle(UpdatePersonStatusCommand request, CancellationToken cancellationToken)
        {
            var org = await profileDataContext.Persons.FirstOrDefaultAsync(x => x.EntityGuid == request.Id);
            if (org == null) throw new ResponseException("Person not found");

            org.Status = request.Status;
            var isUpdated = await profileDataContext.SaveChangesAsync() > 0;
            if (!isUpdated) throw new ResponseException("Couldnt updated...");
            return profileDataContext.Persons.Where(x => x.EntityGuid == request.Id);
        }
    }
}
