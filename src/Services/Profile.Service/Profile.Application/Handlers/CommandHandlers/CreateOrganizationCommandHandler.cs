﻿using Common.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Profile.Application.Commands;
using Profile.Application.Interfaces;
using Profile.Domain.Entities;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Profile.Application.Handlers.CommandHandlers
{
    internal class CreateOrganizationCommandHandler : IRequestHandler<CreateOrganizationCommand, IQueryable<Organization>>
    {
        private readonly ILogger<CreateOrganizationCommandHandler> _logger;
        private readonly IProfileDataContext profileDataContext;

        public CreateOrganizationCommandHandler(ILogger<CreateOrganizationCommandHandler> logger, IProfileDataContext profileDataContext)
        {
            _logger = logger;
            this.profileDataContext = profileDataContext;
        }
        public async Task<IQueryable<Organization>> Handle(CreateOrganizationCommand request, CancellationToken cancellationToken)
        {
            var isDuplicate = await profileDataContext.Organizations.FirstOrDefaultAsync(x => x.Email == request.Email || x.Name == request.Name.Trim()) != null;
            if (isDuplicate) throw new ResponseException("Email or Name already exists");
            var org = new Organization()
            {
                Address = request.Address?.Trim(),
                ContactPersonName = request.ContactPersonName?.Trim(),
                ContactPersonPhone = request.ContactPersonPhone?.Trim(),
                Facebook = request.Facebook?.Trim(),
                LinkedIn = request.LinkedIn?.Trim(),
                LogoUrl = request.LogoUrl?.Trim(),
                Name = request.Name?.Trim(),
                PhoneNumber = request.PhoneNumber?.Trim(),
                Status = request.Status,
                Website = request.Website?.Trim(),
            };

            await profileDataContext.Organizations.AddAsync(org);
            var isSaved = await profileDataContext.SaveChangesAsync() > 0;
            if (!isSaved) throw new ResponseException("Couldn't save organization.");
            return profileDataContext.Organizations.Where(x => x.Id == org.Id).AsQueryable();
        }
    }
}
