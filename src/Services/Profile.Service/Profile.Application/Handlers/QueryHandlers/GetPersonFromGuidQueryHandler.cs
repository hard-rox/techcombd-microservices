﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Profile.Application.Interfaces;
using Profile.Application.Queries;
using Profile.Domain.Entities;

namespace Profile.Application.Handlers.QueryHandlers
{
    internal class GetPersonFromGuidQueryHandler : IRequestHandler<GetPersonFromGuidQuery, IQueryable<Person>>
    {
        private readonly IProfileDataContext _context;

        public GetPersonFromGuidQueryHandler(IProfileDataContext context)
        {
            _context = context;
        }

        public async Task<IQueryable<Person>> Handle(GetPersonFromGuidQuery request, CancellationToken cancellationToken)
        {
            return await Task.Run(() => _context.Persons.Where(pp => pp.EntityGuid == request.Guid).AsQueryable(), cancellationToken);
        }
    }
}
