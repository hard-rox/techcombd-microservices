﻿using MediatR;
using Profile.Application.Interfaces;
using Profile.Application.Queries;
using Profile.Domain.Entities;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Profile.Application.Handlers.QueryHandlers
{
    internal class GetOrganizationQueryHandler : IRequestHandler<GetOrganizationsQuery, IQueryable<Organization>>
    {
        private readonly IProfileDataContext _context;

        public GetOrganizationQueryHandler(IProfileDataContext context)
        {
            _context = context;
        }

        public async Task<IQueryable<Organization>> Handle(GetOrganizationsQuery request, CancellationToken cancellationToken)
        {
            return await Task.Run(() => _context.Organizations.AsQueryable());
        }
    }
}
