﻿using MediatR;
using Profile.Application.Interfaces;
using Profile.Application.Queries;
using Profile.Domain.Entities;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Profile.Application.Handlers.QueryHandlers
{
    internal class GetPersonQueryHandler : IRequestHandler<GetPersonsQuery, IQueryable<Person>>
    {
        private readonly IProfileDataContext _context;

        public GetPersonQueryHandler(IProfileDataContext context)
        {
            _context = context;
        }

        public async Task<IQueryable<Person>> Handle(GetPersonsQuery request, CancellationToken cancellationToken)
        {
            return await Task.Run(() => _context.Persons.AsQueryable());
        }
    }
}
