﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Profile.Application.Interfaces;
using Profile.Application.Queries;
using Profile.Domain.Entities;

namespace Profile.Application.Handlers.QueryHandlers
{
    internal class GetCurrentUserProfileQueryHandler : IRequestHandler<GetCurrentUserProfileQuery, IQueryable<Person>>
    {
        private readonly IProfileDataContext _context;
        private readonly ICurrentUserService _currentUserService;

        public GetCurrentUserProfileQueryHandler(IProfileDataContext context, ICurrentUserService currentUserService)
        {
            _context = context;
            _currentUserService = currentUserService;
        }

        public async Task<IQueryable<Person>> Handle(GetCurrentUserProfileQuery request, CancellationToken cancellationToken)
        {
            return await Task.Run(() => _context.Persons.Where(pp => pp.UserId == _currentUserService.UserId).AsQueryable(), cancellationToken);
        }
    }
}
