﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Profile.Application.Interfaces;
using Profile.Application.Queries;
using Profile.Domain.Entities;

namespace Profile.Application.Handlers.QueryHandlers
{
    internal class GetOrganizationFromGuidQueryHandler : IRequestHandler<GetOrganizationFromGuidQuery, IQueryable<Organization>>
    {
        private readonly IProfileDataContext _context;

        public GetOrganizationFromGuidQueryHandler(IProfileDataContext context)
        {
            _context = context;
        }

        public async Task<IQueryable<Organization>> Handle(GetOrganizationFromGuidQuery request, CancellationToken cancellationToken)
        {
            return await Task.Run(() => _context.Organizations.Where(pp => pp.EntityGuid == request.Guid).AsQueryable(), cancellationToken);
        }
    }
}
