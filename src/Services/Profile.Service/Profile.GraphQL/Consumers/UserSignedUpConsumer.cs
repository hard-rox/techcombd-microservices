﻿using EventBus.Events;
using MassTransit;
using MediatR;
using Microsoft.Extensions.Logging;
using Profile.Application.Commands;
using System.Threading.Tasks;

namespace Profile.GraphQL.Consumers
{
    class UserSignedUpConsumer : IConsumer<UserSignedUp>
    {
        private readonly ILogger<UserSignedUpConsumer> logger;
        private readonly IMediator mediator;
        public UserSignedUpConsumer(ILogger<UserSignedUpConsumer> logger, IMediator mediator)
        {
            this.logger = logger;
            this.mediator = mediator;
        }

        public async Task Consume(ConsumeContext<UserSignedUp> context)
        {
            logger.LogInformation("Handling UserSignedUp event");

            await mediator.Send(new CreateUpdateUserProfileCommand(context.Message.UserId, context.Message.Email));

            logger.LogInformation("Handled UserSignedUp event");
        }
    }
}
