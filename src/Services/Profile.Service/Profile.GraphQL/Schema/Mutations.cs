﻿using System.Threading.Tasks;
using Common.Constants;
using HotChocolate;
using HotChocolate.AspNetCore.Authorization;
using MediatR;
using Profile.Application.Commands;
using Profile.GraphQL.Payloads;

namespace Profile.GraphQL.Schema
{
    // ReSharper disable once ClassNeverInstantiated.Global
    [Authorize(Roles = new string[] { UserRoles.Developer, UserRoles.Admin })]
    public class Mutations
    {
        public async Task<CreatePersonPayload> CreatePersonAsync([Service] IMediator mediator, [GraphQLNonNullType] CreatePersonCommand input)
        {
            var profile = await mediator.Send(input);
            return new CreatePersonPayload(profile);
        }

        public async Task<UpdatePersonPayload> UpdatePersonAsync([Service] IMediator mediator, [GraphQLNonNullType] UpdatePersonCommand input)
        {
            var profile = await mediator.Send(input);
            return new UpdatePersonPayload(profile);
        }

        public async Task<UpdatePersonPayload> UpdatePersonStatusAsync([Service] IMediator mediator, [GraphQLNonNullType] UpdatePersonStatusCommand input)
        {
            var profile = await mediator.Send(input);
            return new UpdatePersonPayload(profile);
        }

        public async Task<CreateOrganizationPayload> CreateOrganizationAsync([Service] IMediator mediator, [GraphQLNonNullType] CreateOrganizationCommand input)
        {
            var profile = await mediator.Send(input);
            return new CreateOrganizationPayload(profile);
        }

        public async Task<UpdateOrganizationPayload> UpdateOrganizationAsync([Service] IMediator mediator, [GraphQLNonNullType] UpdateOrganizationCommand input)
        {
            var profile = await mediator.Send(input);
            return new UpdateOrganizationPayload() { UpdatedOrganization = profile };
        }

        public async Task<UpdateOrganizationPayload> UpdateOrganizationStatusAsync([Service] IMediator mediator, [GraphQLNonNullType] UpdateOrganizationStatusCommand input)
        {
            var profile = await mediator.Send(input);
            return new UpdateOrganizationPayload() { UpdatedOrganization = profile };
        }
    }
}
