﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Common.Constants;
using HotChocolate;
using HotChocolate.AspNetCore.Authorization;
using HotChocolate.Data;
using HotChocolate.Types;
using MediatR;
using Profile.Application.Queries;
using Profile.Domain.Entities;
using Profile.GraphQL.Inputs;

namespace Profile.GraphQL.Schema
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class Queries
    {
        [Authorize(Roles = new string[] { UserRoles.Developer, UserRoles.Admin })]
        [UseOffsetPaging]
        [UseProjection]
        [UseFiltering(typeof(PersonFilterInput))]
        [UseSorting]
        public async Task<IQueryable<Person>> GetPersons([Service] IMediator mediator)
        {
            return await mediator.Send(new GetPersonsQuery());
        }

        [Authorize(Roles = new string[] { UserRoles.Developer, UserRoles.Admin })]
        [UseOffsetPaging]
        [UseProjection]
        [UseFiltering(typeof(OrganizationFilterInput))]
        [UseSorting]
        public async Task<IQueryable<Organization>> GetOrganizations([Service] IMediator mediator)
        {
            return await mediator.Send(new GetOrganizationsQuery());
        }

        [UseFirstOrDefault]
        [UseProjection]
        public async Task<IQueryable<Person>> GetPerson([Service] IMediator mediator, Guid id)
        {
            return await mediator.Send(new GetPersonFromGuidQuery(id));
        }

        [UseFirstOrDefault]
        [UseProjection]
        public async Task<IQueryable<Organization>> GetOrganization([Service] IMediator mediator, Guid id)
        {
            return await mediator.Send(new GetOrganizationFromGuidQuery(id));
        }

        [Authorize]
        [UseFirstOrDefault]
        [UseProjection]
        public async Task<IQueryable<Person>> GetCurrentUserProfile([Service] IMediator mediator)
        {
            return await mediator.Send(new GetCurrentUserProfileQuery());
        }
    }
}
