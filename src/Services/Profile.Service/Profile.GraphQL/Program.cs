using HealthChecks.UI.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Profile.Application;
using Profile.GraphQL.Configs;
using Profile.GraphQL.Helpers;
using Profile.Infrastructure;
using Serilog;
using Serilog.Sinks.SystemConsole.Themes;

const string _dbConnectionName = "ProfileDataConnection";

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddHttpContextAccessor();
builder.Services.AddCors();
builder.Services.AddIdentityServerAuth(builder.Configuration);
builder.Services.BuildGraphQLSchema();

builder.Services.AddErrorFilter<ErrorFilter>();
builder.Services.ConfigureHealthChecks(builder.Configuration, _dbConnectionName);
builder.Services.ConfigureMassTransitRabbitMQ(builder.Configuration);
builder.Services.AddApplicationDependencies();
builder.Services.AddInfrastructureDependencies(builder.Configuration);

// Serilog
builder.Host.UseSerilog((context, logConfig) =>
{
    var seqServerUrl = context.Configuration["Serilog:SeqServerUrl"];
    logConfig
    .Enrich.WithProperty("ApplicationContext", "Profile.Service")
    .Enrich.FromLogContext()
    .WriteTo.Console(theme: AnsiConsoleTheme.Code)
    .WriteTo.File("logs/Profile.Service_.log", rollingInterval: RollingInterval.Day)
    .WriteTo.Seq(string.IsNullOrWhiteSpace(seqServerUrl) ? "http://seq" : seqServerUrl)
    .ReadFrom.Configuration(context.Configuration);
});

var app = builder.Build();

if (app.Environment.IsDevelopment()) app.UseDeveloperExceptionPage();

app.UseCors(options =>
{
    options.AllowAnyOrigin();
    options.AllowAnyHeader();
    options.AllowAnyMethod();
});

app.UseRouting();
app.UseAuthentication();
app.UseAuthorization();

app.UseEndpoints(opt =>
{
    opt.MapGraphQL();
    opt.MapHealthChecks("/hc", new HealthCheckOptions()
    {
        Predicate = _ => true,
        ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
    });
});

app.Run();