﻿using System.Linq;
using HotChocolate.Data;
using Profile.Domain.Entities;

namespace Profile.GraphQL.Payloads
{
    public class CreatePersonPayload
    {
        public CreatePersonPayload(IQueryable<Person> person)
        {
            Person = person;
        }

        [UseFirstOrDefault]
        [UseProjection]
        public IQueryable<Person> Person { get; }
    }
}
