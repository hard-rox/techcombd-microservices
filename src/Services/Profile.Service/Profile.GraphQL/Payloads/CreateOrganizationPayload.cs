﻿using System.Linq;
using HotChocolate.Data;
using Profile.Domain.Entities;

namespace Profile.GraphQL.Payloads
{
    public class CreateOrganizationPayload
    {
        public CreateOrganizationPayload(IQueryable<Organization> organization)
        {
            Organization = organization;
        }
        [UseFirstOrDefault]
        [UseProjection]
        public IQueryable<Organization> Organization { get; }
    }
}
