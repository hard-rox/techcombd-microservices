﻿using System.Linq;
using HotChocolate.Data;
using Profile.Domain.Entities;

namespace Profile.GraphQL.Payloads
{
    public class UpdateOrganizationPayload
    {
        [UseFirstOrDefault]
        [UseProjection]
        public IQueryable<Organization> UpdatedOrganization { get; set; }
    }
}
