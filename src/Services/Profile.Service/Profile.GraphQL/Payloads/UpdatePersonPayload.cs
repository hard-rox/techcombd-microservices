﻿using System.Linq;
using HotChocolate.Data;
using Profile.Domain.Entities;

namespace Profile.GraphQL.Payloads
{
    public class UpdatePersonPayload
    {
        public UpdatePersonPayload(IQueryable<Person> person)
        {
            UpdatedPerson = person;
        }

        [UseFirstOrDefault]
        [UseProjection]
        public IQueryable<Person> UpdatedPerson { get; }
    }
}
