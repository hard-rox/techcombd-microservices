﻿using Common.Constants;
using HotChocolate.Types;
using Profile.Domain.Entities;

namespace Profile.GraphQL.Types
{
    public class OrganizationType : ObjectType<Organization>
    {
        protected override void Configure(IObjectTypeDescriptor<Organization> descriptor)
        {
            base.Configure(descriptor);

            descriptor.Field(x => x.EntityGuid)
                .Name("id")
                .Type<NonNullType<UuidType>>();
            descriptor.Field(x => x.Status).Authorize(new string[] { UserRoles.Developer, UserRoles.Admin });

            descriptor.Ignore(x => x.Id);
            descriptor.Ignore(x => x.CreatedAtUtc);
            descriptor.Ignore(x => x.CreatedBy);
            descriptor.Ignore(x => x.LastModifiedAtUtc);
            descriptor.Ignore(x => x.LastModifiedBy);
        }
    }
}
