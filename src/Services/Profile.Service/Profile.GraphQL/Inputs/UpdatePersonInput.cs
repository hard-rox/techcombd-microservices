﻿using HotChocolate.Types;
using Profile.Application.Commands;

namespace Profile.GraphQL.Inputs
{
    public class UpdatePersonInput : InputObjectType<UpdatePersonCommand>
    {
        protected override void Configure(IInputObjectTypeDescriptor<UpdatePersonCommand> descriptor)
        {
            base.Configure(descriptor);
        }
    }
}
