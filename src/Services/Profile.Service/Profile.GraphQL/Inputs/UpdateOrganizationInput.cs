﻿using HotChocolate.Types;
using Profile.Application.Commands;

namespace Profile.GraphQL.Inputs
{
    public class UpdateOrganizationInput : InputObjectType<UpdateOrganizationCommand>
    {
        protected override void Configure(IInputObjectTypeDescriptor<UpdateOrganizationCommand> descriptor)
        {
            base.Configure(descriptor);

            descriptor.Name("updateOrganizationInput");
        }
    }
}
