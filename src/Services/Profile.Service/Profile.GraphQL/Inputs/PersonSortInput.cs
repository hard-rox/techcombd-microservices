﻿using HotChocolate.Data.Sorting;
using Profile.Domain.Entities;

namespace Profile.GraphQL.Inputs
{
    public class PersonSortInput : SortInputType<Person>
    {
        protected override void Configure(ISortInputTypeDescriptor<Person> descriptor)
        {
            base.Configure(descriptor);
            descriptor.BindFieldsExplicitly();

            descriptor.Field(x => x.Name);
            descriptor.Field(x => x.Status);
        }
    }
}
