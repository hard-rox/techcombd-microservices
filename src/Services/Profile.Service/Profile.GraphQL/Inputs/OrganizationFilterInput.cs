﻿using HotChocolate.Data.Filters;
using Profile.Domain.Entities;

namespace Profile.GraphQL.Inputs
{
    public class OrganizationFilterInput : FilterInputType<Organization>
    {
        protected override void Configure(IFilterInputTypeDescriptor<Organization> descriptor)
        {
            base.Configure(descriptor);

            descriptor.BindFieldsExplicitly();
            descriptor.Field(x => x.EntityGuid).Name("id");
            descriptor.Field(x => x.Name);
            descriptor.Field(x => x.Email);
            descriptor.Field(x => x.ContactPersonName);
            descriptor.Field(x => x.Status);
        }
    }
}
