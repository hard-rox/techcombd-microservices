﻿using HotChocolate.Data.Filters;
using Profile.Domain.Entities;

namespace Profile.GraphQL.Inputs
{
    public class PersonFilterInput : FilterInputType<Person>
    {
        protected override void Configure(IFilterInputTypeDescriptor<Person> descriptor)
        {
            base.Configure(descriptor);

            descriptor.BindFieldsExplicitly();
            descriptor.Field(x => x.EntityGuid).Name("id");
            descriptor.Field(x => x.Name);
            descriptor.Field(x => x.Email);
            descriptor.Field(x => x.Status);
            descriptor.Field(x => x.OrganizationalRole);
            descriptor.Field(x => x.Organization);
        }
    }
}
