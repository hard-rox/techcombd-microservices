﻿using HotChocolate.Data.Sorting;
using Profile.Domain.Entities;

namespace Profile.GraphQL.Inputs
{
    public class OrganizationSortInput : SortInputType<Organization>
    {
        protected override void Configure(ISortInputTypeDescriptor<Organization> descriptor)
        {
            base.Configure(descriptor);
            descriptor.BindFieldsExplicitly();

            descriptor.Field(x => x.Name);
            descriptor.Field(x => x.ContactPersonName);
            descriptor.Field(x => x.Status);
        }
    }
}
