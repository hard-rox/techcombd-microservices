﻿using HotChocolate.Types;
using Profile.Application.Commands;
using Profile.Domain.Enums;

namespace Profile.GraphQL.Inputs
{
    public class CreatePersonInput : InputObjectType<CreatePersonCommand>
    {
        protected override void Configure(IInputObjectTypeDescriptor<CreatePersonCommand> descriptor)
        {
            base.Configure(descriptor);

            descriptor.Field(pp => pp.Name)
                .Type<NonNullType<StringType>>();
            descriptor.Field(pp => pp.Email)
                .Type<NonNullType<StringType>>();
            descriptor.Field(x => x.Status)
                .Type<NonNullType<EnumType<ProfileStatus>>>();
        }
    }
}