﻿using HotChocolate;
using HotChocolate.Execution.Configuration;
using HotChocolate.Types;
using HotChocolate.Types.Pagination;
using Microsoft.Extensions.DependencyInjection;
using Profile.GraphQL.Inputs;
using Profile.GraphQL.Schema;
using Profile.GraphQL.Types;
using System;

namespace Profile.GraphQL.Configs
{
    public static class SchemaConfig
    {
        private static IRequestExecutorBuilder AddTypes(this IRequestExecutorBuilder builder)
        {
            return builder
                .AddType<PersonType>()
                .AddType<OrganizationType>();
        }

        private static IRequestExecutorBuilder AddInputs(this IRequestExecutorBuilder builder)
        {
            return builder
                .AddType<CreatePersonInput>()
                .AddType<UpdatePersonInput>()
                .AddType<PersonSortInput>()
                .AddType<OrganizationSortInput>();
        }

        public static IRequestExecutorBuilder BuildGraphQLSchema(this IServiceCollection services)
        {
            return services
                .AddGraphQLServer()
                .AddAuthorization()
                .AddQueryType<Queries>()
                .AddMutationType<Mutations>()
            .AddTypes()
            .AddInputs()
            .AddProjections()
                        .AddFiltering()
                        .AddSorting()
                .ModifyRequestOptions(opt =>
                {
                    opt.IncludeExceptionDetails = true;
                    //opt.TracingPreference = TracingPreference.Always;
                })
                .ModifyOptions(opt =>
                {
                    opt.SortFieldsByName = true;
                })
                .SetPagingOptions(new PagingOptions
                {
                    MaxPageSize = 100,
                    DefaultPageSize = 10,
                    IncludeTotalCount = true
                })
                .BindRuntimeType<DateTime, DateTimeType>()
                .BindRuntimeType<Guid, UuidType>()
                .InitializeOnStartup();
        }
    }
}
