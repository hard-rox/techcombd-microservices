﻿using MassTransit;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Profile.GraphQL.Consumers;
using Profile.GraphQL.Helpers;

namespace Profile.GraphQL.Configs
{
    internal static class EventBusConfig
    {
        internal static void ConfigureMassTransitRabbitMQ(this IServiceCollection services, IConfiguration configuration)
        {
            var settings = configuration
                .GetSection("AppSettings").Get<AppSettings>();
            services.AddMassTransit(busConfig =>
            {
                busConfig.AddConsumer<UserSignedUpConsumer>();
                busConfig.UsingRabbitMq((ctx, cfg) =>
                {
                    cfg.Host(settings.RabbitMQ.Host, cfg =>
                    {
                        cfg.Username(settings.RabbitMQ.UserName);
                        cfg.Password(settings.RabbitMQ.Password);
                    });

                    cfg.ReceiveEndpoint(Common.BusEndpoints.ProfileServiceEndpoint, x =>
                    {
                        x.ConfigureConsumer<UserSignedUpConsumer>(ctx);
                    });
                });
            });
            services.AddMassTransitHostedService();
        }
    }
}
