﻿using StaticContent.Domain.Common;

namespace StaticContent.Domain.Entities
{
    public class StaticFile : AuditableEntity
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public string FileType { get; set; }
        public string FileExtension { get; set; }
        public decimal FileSizeInBytes { get; set; }
        public byte[] FileBytes { get; set; }
    }
}
