﻿using System;

namespace StaticContent.Domain.Common
{
    public class AuditableEntity
    {
        public Guid EntityGuid { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedAtUtc { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime? LastModifiedAtUtc { get; set; }
    }
}
