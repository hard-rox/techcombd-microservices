using HealthChecks.UI.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Serilog;
using Serilog.Sinks.SystemConsole.Themes;
using StaticContent.Api.Configs;
using StaticContent.Application;
using StaticContent.Application.Exceptions;
using StaticContent.Infrastructure;

const string _dbConnectionName = "StaticContentDataConnection";

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllers();
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo { Title = "TechComBD.StaticContent.Api", Version = "v1" });
});
//builder.Services.ConfigureHealthChecks(_configuration, _dbConnectionName);
builder.Services.AddCors(options =>
{
    options.AddPolicy("CorsPolicy",
        builder => builder
        .WithMethods("POST")
        .AllowAnyHeader()
        .SetIsOriginAllowed((host) => true)
        .AllowCredentials()
        .WithExposedHeaders("Location"));
});
//builder.Services.ConfigureMassTransitRabbitMQ(_configuration);
builder.Services.AddApplicationDependencies(builder.Configuration);
builder.Services.AddInfrastructureDependencies(builder.Configuration);
builder.Services.ConfigureHealthChecks(builder.Configuration, _dbConnectionName);

// Serilog
builder.Host.UseSerilog((context, logConfig) =>
{
    var seqServerUrl = context.Configuration["Serilog:SeqServerUrl"];
    logConfig
    .Enrich.WithProperty("ApplicationContext", "StaticContent.Service")
    .Enrich.FromLogContext()
    .WriteTo.Console(theme: AnsiConsoleTheme.Code)
    .WriteTo.File("logs/StaticContent.Service_.log", rollingInterval: RollingInterval.Day)
    .WriteTo.Seq(string.IsNullOrWhiteSpace(seqServerUrl) ? "http://seq" : seqServerUrl)
    .ReadFrom.Configuration(context.Configuration);
});

var app = builder.Build();

if (app.Environment.IsDevelopment()) app.UseDeveloperExceptionPage();

app.UseSwagger();
app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "StaticContent.Api v1"));

app.UseExceptionHandler(appError =>
{
    appError.Run(async context =>
    {
        var contextFeature = context.Features.Get<IExceptionHandlerFeature>();
        if (contextFeature != null)
        {
            if (contextFeature.Error.GetType() == typeof(NotFoundException))
            {
                context.Response.StatusCode = StatusCodes.Status404NotFound;
            }
            else
            {
                context.Response.StatusCode = StatusCodes.Status500InternalServerError;
            }
        }
    });
});

app.UseHttpsRedirection();

app.UseCors("CorsPolicy");
app.UseRouting();

app.UseAuthorization();

app.UseEndpoints(endpoints =>
{
    endpoints.MapControllers();
    endpoints.MapHealthChecks("/hc", new HealthCheckOptions()
    {
        Predicate = _ => true,
        ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
    });
});

app.Run();