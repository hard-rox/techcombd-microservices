﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using StaticContent.Application.Interfaces;
using System;
using System.Threading.Tasks;

namespace StaticContent.Api.Controllers
{
    [ApiController]
    [Route("files")]
    public class FilesController : ControllerBase
    {
        private readonly ILogger<FilesController> _logger;
        private readonly IStaticFileService staticFileService;

        public FilesController(ILogger<FilesController> logger, IStaticFileService staticFileService)
        {
            _logger = logger;
            this.staticFileService = staticFileService;
        }

        [HttpGet("{id}")]
        public async Task<FileResult> Get(Guid id)
        {
            var response = await staticFileService.GetFile(id);
            return response;
        }

        [HttpPost, DisableRequestSizeLimit]
        public async Task<ActionResult> Post()
        {
            var file = Request.Form.Files[0];
            var result = await staticFileService.SaveFile(file);
            var resourseUrl = $"{Request.Scheme}://{Request.Host}/files/{result}";
            return Created(resourseUrl, null);
        }
    }
}
