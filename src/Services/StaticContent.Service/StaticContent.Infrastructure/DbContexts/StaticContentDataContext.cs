﻿using Microsoft.EntityFrameworkCore;
using StaticContent.Application.Interfaces;
using StaticContent.Domain.Common;
using StaticContent.Domain.Entities;
using System;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace StaticContent.Infrastructure.DbContexts
{
    internal class StaticContentDataContext : DbContext, IStaticContentDataContext
    {
        public StaticContentDataContext(DbContextOptions<StaticContentDataContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            var entities = builder.Model.GetEntityTypes();
            //For auto generate Guid Id...
            foreach (var entity in entities)
            {
                foreach (var property in entity.ClrType.GetProperties()
                    .Where(p => p.Name == "EntityGuid" && p.PropertyType == typeof(Guid)))
                {
                    builder
                        .Entity(entity.ClrType)
                        .Property(property.Name)
                        .HasDefaultValueSql("newsequentialid()");
                }
                builder
                        .Entity(entity.ClrType)
                        .HasIndex("EntityGuid")
                        .IsUnique();

            }

            entities
                .SelectMany(t => t.GetProperties())
                .Where(p => p.ClrType == typeof(decimal)
                         || p.ClrType == typeof(decimal?))
                .ToList()
                .ForEach(p =>
                {
                    if (p.GetPrecision() is null)
                        p.SetPrecision(18);
                    if (p.GetScale() is null)
                        p.SetScale(4);
                });

            builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
            base.OnModelCreating(builder);
        }

        public DbSet<StaticFile> StaticFiles { get; set; }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            foreach (var entry in ChangeTracker.Entries<AuditableEntity>())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.CreatedBy = null;
                        entry.Entity.CreatedAtUtc = DateTime.UtcNow;
                        break;
                    case EntityState.Modified:
                        entry.Entity.LastModifiedBy = null;
                        entry.Entity.LastModifiedAtUtc = DateTime.UtcNow;
                        break;
                    case EntityState.Detached:
                        break;
                    case EntityState.Unchanged:
                        break;
                    case EntityState.Deleted:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            return base.SaveChangesAsync(cancellationToken);
        }
    }
}
