﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using StaticContent.Application.Exceptions;
using StaticContent.Application.Interfaces;
using StaticContent.Domain.Entities;
using System;
using System.IO;
using System.Threading.Tasks;

namespace StaticContent.Infrastructure.Services
{
    internal class StaticFileService : IStaticFileService
    {
        private readonly IStaticContentDataContext staticContentDataContext;

        private byte[] GetBytes(IFormFile file)
        {
            using var ms = new MemoryStream();
            file.CopyTo(ms);
            var bytes = ms.ToArray();
            return bytes;
        }


        public StaticFileService(IStaticContentDataContext staticContentDataContext)
        {
            this.staticContentDataContext = staticContentDataContext;
        }

        public async Task<FileContentResult> GetFile(Guid id)
        {
            var file = await staticContentDataContext.StaticFiles
                .FirstOrDefaultAsync(x => x.EntityGuid == id);
            if (file == null) throw new NotFoundException();

            var result = new FileContentResult(file.FileBytes, file.FileType)
            {
                FileDownloadName = id + file.FileExtension
            };
            return result;
        }

        public async Task<Guid> SaveFile(IFormFile file)
        {
            var staticFile = new StaticFile()
            {
                FileName = file.FileName,
                FileType = file.ContentType,
                FileExtension = Path.GetExtension(file.FileName),
                FileSizeInBytes = file.Length,
                FileBytes = GetBytes(file)
            };
            await staticContentDataContext.StaticFiles.AddAsync(staticFile);
            var result = await staticContentDataContext.SaveChangesAsync();
            if (result <= 0) throw new Exception("Couldnt save file");
            return staticFile.EntityGuid;
        }
    }
}
