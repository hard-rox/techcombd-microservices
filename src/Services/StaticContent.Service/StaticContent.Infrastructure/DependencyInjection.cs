﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using StaticContent.Application.Interfaces;
using StaticContent.Infrastructure.DbContexts;
using StaticContent.Infrastructure.Services;

namespace StaticContent.Infrastructure
{
    public static class DependencyInjection
    {
        public static void AddInfrastructureDependencies(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<StaticContentDataContext>(options =>
                options.UseSqlServer(configuration.GetConnectionString("StaticContentDataConnection"),
                    b => b.MigrationsAssembly(typeof(StaticContentDataContext).Assembly.FullName)));

            services.AddScoped<IStaticContentDataContext>(provider => provider.GetService<StaticContentDataContext>());
            services.AddScoped<IStaticFileService, StaticFileService>();
        }
    }
}
