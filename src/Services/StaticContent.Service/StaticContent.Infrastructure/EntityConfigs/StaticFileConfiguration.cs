﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using StaticContent.Domain.Entities;

namespace StaticContent.Infrastructure.EntityConfigs
{
    internal class StaticFileConfiguration : IEntityTypeConfiguration<StaticFile>
    {
        public void Configure(EntityTypeBuilder<StaticFile> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.FileBytes).IsRequired();
            builder.Property(x => x.FileExtension).IsRequired();
            builder.Property(x => x.FileName).IsRequired();
            builder.Property(x => x.FileSizeInBytes).IsRequired();
        }
    }
}
