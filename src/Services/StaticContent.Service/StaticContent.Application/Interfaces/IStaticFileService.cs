﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace StaticContent.Application.Interfaces
{
    public interface IStaticFileService
    {
        Task<Guid> SaveFile(IFormFile file);
        Task<FileContentResult> GetFile(Guid id);
    }
}
