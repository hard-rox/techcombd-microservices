﻿using Newtonsoft.Json;
using System.Security.Cryptography;
using System.Text;

namespace Common.Extensions
{
    public static class GlobalExtensions
    {
        public static string ToIndentedJson(this object param)
        {
            return JsonConvert.SerializeObject(param, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
        }
        public static string ToJson(this object param)
        {
            return JsonConvert.SerializeObject(param, Formatting.None, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
        }

        public static string ToMD5HashString(this string inputText)
        {
            using var md5 = MD5.Create();
            var inputBytes = Encoding.ASCII.GetBytes(inputText);
            var hashBytes = md5.ComputeHash(inputBytes);
            // Convert the byte array to hexadecimal string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hashBytes.Length; i++)
            {
                sb.Append(hashBytes[i].ToString("X2"));
            }
            return sb.ToString();
        }

        public static string GetGravatarUrlFromEmail(this string email)
        {
            var hash = email.Trim().ToLower().ToMD5HashString();
            return $"https://www.gravatar.com/avatar/{hash.ToLower()}";
        }
    }
}
