﻿namespace Common.Constants
{
    public static class UserRoles
    {
        public const string Developer = "Developer";
        public const string Admin = "Admin";
        public const string Guest = "Guest";
    }
}
