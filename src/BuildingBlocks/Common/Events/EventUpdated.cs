﻿using MassTransit.Topology;
using System;

namespace Common.Events
{
    [EntityName("event-updated")]
    public class EventUpdated
    {
        public EventUpdated(Guid eventServiceEntityGuid, string eventTitle, int? seatCapacity, DateTime? registrationStartDateTimeUtc, DateTime? registrationEndDateTimeUtc, int status, string lastModifiedBy)
        {
            EventServiceEntityGuid = eventServiceEntityGuid;
            EventTitle = eventTitle;
            SeatCapacity = seatCapacity;
            RegistrationStartDateTimeUtc = registrationStartDateTimeUtc;
            RegistrationEndDateTimeUtc = registrationEndDateTimeUtc;
            Status = status;
            LastModifiedBy = lastModifiedBy;
        }
        public Guid EventServiceEntityGuid { get; }
        public string EventTitle { get; }
        public int? SeatCapacity { get; }
        public DateTime? RegistrationStartDateTimeUtc { get; }
        public DateTime? RegistrationEndDateTimeUtc { get; }
        public int Status { get; }
        public string LastModifiedBy { get; }
    }
}
