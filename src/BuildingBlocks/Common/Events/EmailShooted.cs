﻿using MassTransit.Topology;

namespace Common.Events
{
    [EntityName("email-shooted")]
    public class EmailShooted
    {
        public EmailShooted(string subject, string body, params string[] receiverEmails)
        {
            ReceiverEmails = receiverEmails;
            Subject = subject;
            Body = body;
        }
        public string[] ReceiverEmails { get; }
        public string Subject { get; }
        public string Body { get; }
    }
}
