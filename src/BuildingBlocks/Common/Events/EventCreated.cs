﻿using MassTransit.Topology;
using System;

namespace Common.Events
{
    [EntityName("event-created")]
    public class EventCreated
    {
        public EventCreated(Guid eventServiceEntityGuid, string eventName, DateTime? registrationStartDateTimeUtc, DateTime? registrationEndDateTimeUtc, int status, string createdBy, int? seatCapacity)
        {
            EventServiceEntityGuid = eventServiceEntityGuid;
            EventTitle = eventName;
            RegistrationStartDateTimeUtc = registrationStartDateTimeUtc;
            RegistrationEndDateTimeUtc = registrationEndDateTimeUtc;
            Status = status;
            CreatedBy = createdBy;
            SeatCapacity = seatCapacity;
        }

        public Guid EventServiceEntityGuid { get; }
        public string EventTitle { get; }
        public int? SeatCapacity { get; }
        public DateTime? RegistrationStartDateTimeUtc { get; }
        public DateTime? RegistrationEndDateTimeUtc { get; }
        public int Status { get; }
        public string CreatedBy { get; }
    }
}
