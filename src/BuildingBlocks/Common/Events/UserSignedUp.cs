﻿using MassTransit.Topology;

namespace EventBus.Events
{
    [EntityName("user-signed-up")]
    public class UserSignedUp
    {
        public string UserId { get; }
        public string Email { get; }

        public UserSignedUp(string userId, string email)
        {
            UserId = userId;
            Email = email;
        }
    }
}
