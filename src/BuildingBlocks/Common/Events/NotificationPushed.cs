﻿using MassTransit.Topology;

namespace Common.Events
{
    [EntityName("notification-pushed")]
    public class NotificationPushed
    {
        public NotificationPushed(string title, string text, string url = null, string imageUrl = null, string audienceUserId = null)
        {
            Title = title;
            Text = text;
            Url = url;
            ImageUrl = imageUrl;
            AudienceUserId = audienceUserId;
        }
        public string Title { get; private set; }
        public string Text { get; private set; }
        public string Url { get; private set; }
        public string ImageUrl { get; private set; }
        public string AudienceUserId { get; private set; }
    }
}
