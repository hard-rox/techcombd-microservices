﻿using MassTransit.Topology;

namespace Common.Events
{
    [EntityName("redundant-event-created")]
    public class RedundantEventCreated
    {
        public string EventName { get; }
        public string CreatedBy { get; }

        public RedundantEventCreated(string eventName, string createdBy)
        {
            EventName = eventName;
            CreatedBy = createdBy;
        }
    }
}
