﻿namespace Common
{
    public static class BusEndpoints
    {
        public const string EventServiceEndpoint = "event-service";
        public const string EventRegistrationServiceEndpoint = "event-registration-service";
        public const string AuthServiceServiceEndpoint = "auth-service";
        public const string ProfileServiceEndpoint = "profile-service";
        public const string PushNotificationServiceEndpoint = "push-notification-service";
        public const string PostmasterServiceEndpoint = "postmaster-service";
    }
}
