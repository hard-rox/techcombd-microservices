﻿namespace WebGraphQLGateway.Helpers
{
    public class AppSettings
    {
        public Service[] Services { get; set; }
    }

    public class Service
    {
        public string Name { get; set; }
        public string BaseUri { get; set; }
    }
}
