using HealthChecks.UI.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Sinks.SystemConsole.Themes;
using System;
using System.Linq;
using System.Net.Http.Headers;
using WebGraphQLGateway.Helpers;


const string EventServiceName = "EventService";
const string EventRegistrationServiceName = "EventRegistrationService";
const string ProfileServiceName = "ProfileService";

var builder = WebApplication.CreateBuilder(args);

// Serilog
builder.Host.UseSerilog((context, logConfig) =>
{
    var seqServerUrl = context.Configuration["Serilog:SeqServerUrl"];
    logConfig
    .Enrich.WithProperty("ApplicationContext", "WebGraphQL")
    .Enrich.FromLogContext()
    .WriteTo.Console(theme: AnsiConsoleTheme.Code)
    .WriteTo.File("logs/WebGraphQL_.log", rollingInterval: RollingInterval.Day)
    .WriteTo.Seq(string.IsNullOrWhiteSpace(seqServerUrl) ? "http://seq" : seqServerUrl)
    .ReadFrom.Configuration(context.Configuration);
});

// Add services to the container.
builder.Services.AddHttpContextAccessor();
builder.Services.AddCors();

var appSettings = builder.Configuration.GetSection("AppSettings").Get<AppSettings>();

var eventService = appSettings.Services.FirstOrDefault(x => x.Name == EventServiceName);
AddDownstreamHttpClient(builder.Services, eventService);

var eventRegService = appSettings.Services.FirstOrDefault(x => x.Name == EventRegistrationServiceName);
AddDownstreamHttpClient(builder.Services, eventRegService);

var profileService = appSettings.Services.FirstOrDefault(x => x.Name == ProfileServiceName);
AddDownstreamHttpClient(builder.Services, profileService);

builder.Services
    .AddGraphQLServer()
    .AddRemoteSchema(EventServiceName)
    .AddRemoteSchema(EventRegistrationServiceName)
    .AddRemoteSchema(ProfileServiceName)
    .AddTypeExtensionsFromFile("./Stitching.graphql");

builder.Services.AddHealthChecks()
    .AddCheck("self", () => HealthCheckResult.Healthy());

var app = builder.Build();

if (app.Environment.IsDevelopment()) app.UseDeveloperExceptionPage();

app.UseCors(options =>
{
    options.AllowAnyOrigin();
    options.AllowAnyHeader();
    options.AllowAnyMethod();
});

app.UseRouting();

app.UseEndpoints(opt =>
{
    opt.MapGraphQL();
    opt.MapHealthChecks("/hc", new HealthCheckOptions()
    {
        Predicate = _ => true,
        ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
    });
});

app.Run();

static void AddDownstreamHttpClient(IServiceCollection services, Service downstreamService)
{
    services.AddHttpClient(downstreamService.Name, (sp, client) =>
    {
        HttpContext context = sp.GetRequiredService<IHttpContextAccessor>().HttpContext;
        if (context.Request.Headers.ContainsKey("Authorization"))
        {
            client.DefaultRequestHeaders.Authorization =
                AuthenticationHeaderValue.Parse(
                    context.Request.Headers["Authorization"]
                        .ToString());
        }
        client.BaseAddress = new Uri(downstreamService.BaseUri);
    });
}