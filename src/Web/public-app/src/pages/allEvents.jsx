import { gql, useQuery } from '@apollo/client';
import { useState } from 'react';
import { Link } from 'react-router-dom';


const All_Events = gql`
  query GetAllEvents($searchText:String!){
    events(take: 100,where: { or: [{ title: { contains: $searchText } }] }, order:{eventEndDateTime:DESC}) {
        items{
          id
         title
         bannerUrl
          about
        }
      }
  }
`;


const AllEvents = () => {
    const [searchText, setSearchText] = useState('');
    const { loading, error, data } = useQuery(All_Events, {
        variables: {
            searchText: searchText
        },
    });
    let searchTextValue = searchText;

    if (loading) return 'Loading...';
    if (error) return `Error! ${error.message}`;
    return (
        <div className='p-6'>
            <div className="flex items-center justify-center">
                <div className="flex border-2 rounded">
                    <input className="px-4 py-2 w-80" placeholder="Search...." onChange={(e) => {
                        searchTextValue = e.target.value;
                    }}/>
                    <button onClick={() => {
                        setSearchText(searchTextValue);
                    }} 
                     className="flex items-center justify-center px-4 border-l">
                        <svg className="w-6 h-6 text-gray-600" fill="currentColor" xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 24 24">
                            <path
                                d="M16.32 14.9l5.39 5.4a1 1 0 0 1-1.42 1.4l-5.38-5.38a8 8 0 1 1 1.41-1.41zM10 16a6 6 0 1 0 0-12 6 6 0 0 0 0 12z" />
                        </svg>
                    </button>
                </div>
            </div>
            <h1 className="font-bold text-4xl mt-2 mb-6">All Events</h1>
            <div className='grid grid-cols-2 gap-5'>
                {
                    data.events.items.map(event => (
                        <div key={event.id}>
                            <img className="w-full" src={event.bannerUrl} alt="#" />
                            <div className='p-6'>
                                <h4 className="font-bold text-lg mb-2">{event.title}</h4>
                                <p className="text-justify mb-4">{event.about.substring(0, 100)}</p>
                                <button className="inline-block bg-gray-300 font-bold px-10 py-2 rounded-md hover:bg-gray-400"><Link to={`/events/${event.id}`}>See More</Link></button>
                            </div>
                        </div>
                    ))
                }
            </div>

        </div>
    )

}
export default AllEvents