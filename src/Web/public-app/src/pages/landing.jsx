import Representation from "../components/landing/representation";
import UpComingEventCard from "../components/landing/upcomingEventCard";
import PreviousEventCard from "../components/landing/previousEventCard"
import BlogPostCard from "../components/landing/blogPostCard";
import { Link } from 'react-router-dom';


const Landing = () => {
  return (
    <div className="px-20">
      {/* Hero section */}
      <div className="py-20">
        <h4 className="font-bold">We believe in community</h4>
        <h1 className="font-bold text-4xl mt-2">Events · Seminars · Blogging</h1>
        <p className="text-justify mt-2">By  creating a community through our organization where you can share your thoughts as well as be a part of the community by joining different events and seminars.</p>
        <div className="space-x-4 mt-4">
          <button className="bg-gray-300 font-bold px-10 py-2 rounded-md hover:bg-gray-400"><Link to={`/events`}>View Event</Link></button>
          <button className="bg-gray-300 font-bold px-10 py-2 rounded-md hover:bg-gray-400">Let’s Join</button>
        </div>
      </div>

      {/* Help section */}
      <div className="py-20">
        <Representation />
      </div>

      {/*Up Coming Event Section */}
      <div className="py-20">
        <UpComingEventCard />
      </div>

      {/*Previous Event Section */}
      <div className="py-20">
        <PreviousEventCard />
      </div>

      {/*Blog Post Section */}
      <div className="py-20">
        <BlogPostCard />
      </div>

    </div>
  );
}

export default Landing;