import { useParams } from 'react-router-dom';
import { gql, useQuery } from '@apollo/client';
import RegistrationBtn from '../components/shared/registrationBtn';

const Event_Details = gql`
  query GetEventDetails($eventId: UUID!) {
    event(id: $eventId) {
  id
  title
      about
      bannerUrl
      registrationEndDateTime
      description
      speakers{
        id
        profile{
          id
          name
          dpUrl
          organizationalRole
          organization{
            id
            name
          }
        }
      }
      sponsors{
        id
        profile{
          id
          name
          logoUrl
        }
      }
      mapEmbedUrl
      seatCapacity
      seatsOccupied
    }
  }
`;

const EventDetails = () => {

  const { id } = useParams();
  const { loading, error, data } = useQuery(Event_Details, {
    variables: { eventId: id }
  });

  if (loading) return 'Loading...';
  if (error) return `Error! ${error.message}`;

  return (
    <div>
      {
        <div >
          {/* Event Topic section */}
          <div className="px-20 py-20">
            <h4 className="font-bold">Event Topic</h4>
            <h1 className="font-bold text-4xl mt-2">{data.event.title}</h1>
            <p className="text-justify mt-2 w-3/4">{data.event.about}
            </p>
            <div className="space-x-4 mt-4">
              <a href='#description' className="flex space-x-2 font-bold hover:text-cyan-500" >
                <span>Show More</span>
                <svg className='hover:fill-cyan-500 my-auto' width="55" height="20" viewBox="0 0 69 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M68.0607 13.0607C68.6464 12.4749 68.6464 11.5251 68.0607 10.9393L58.5147 1.3934C57.9289 0.807611 56.9792 0.807611 56.3934 1.3934C55.8076 1.97919 55.8076 2.92893 56.3934 3.51472L64.8787 12L56.3934 20.4853C55.8076 21.0711 55.8076 22.0208 56.3934 22.6066C56.9792 23.1924 57.9289 23.1924 58.5147 22.6066L68.0607 13.0607ZM0 13.5H67V10.5H0V13.5Z" fill="black" />
                </svg>
              </a>

            </div>
          </div>
          {/* image section */}
          <img className="w-full" src={data.event.bannerUrl} alt="#" />

          {/* Description section */}
          <div className="px-20" id="description">
            <h1 className="font-bold mt-8">Description</h1>
            <div dangerouslySetInnerHTML={{__html: data.event.description}} className="mt-2"></div>

            {/* Speaker section */}
            <h1 className="font-bold mt-16">Speakers</h1>
            <div className="flex justify-center space-x-8 mt-8">
              {
                data.event.speakers.map(speaker => (
                  <div key={speaker.id}>
                    <img className="rounded-full w-24 h-24 mx-auto" src={speaker.profile.dpUrl} alt="#" />
                    <h2 className="font-bold text-center mt-6">{speaker.profile.name}</h2>
                    <h3 className="text-center">{speaker?.profile?.organizationalRole}</h3>
                    <h4 className="text-center">{speaker?.profile?.organization?.name}</h4>
                  </div>
                ))
              }

            </div>

            {/* Sponsor section */}
            <h1 className="font-bold mt-16">Sponsors</h1>
            <div className="flex justify-center space-x-8 mt-8">
              {
                data.event.sponsors.map(sponsor => (
                  <div key={sponsor.id}>
                    <img className="rounded-full w-24 h-24 mx-auto" src={sponsor.profile.logoUrl} alt="#" />
                    <h2 className="font-bold text-center mt-6">{sponsor.profile.name}</h2>
                  </div>
                ))
              }

            </div>

            {/* Statistics section */}
            <h1 className="font-bold mt-16">Statistics</h1>

            {
              data.event.registrationEndDateTime > Date.now() ?
                <div className="flex justify-center space-x-44 mt-8">
                  <div >
                    <h1 className="font-bold text-2xl text-center mt-6">{data.event.seatCapacity}</h1>
                    <h2 className="text-center">Total Participants</h2>
                  </div>
                  <div >
                    <h1 className="font-bold text-2xl text-center mt-6">{data.event.seatCapacity - data.event.seatsOccupied}</h1>
                    <h2 className="text-center">Available Participants</h2>
                  </div>
                  <div >
                    <h1 className="font-bold text-2xl text-center mt-6">{data.event.seatsOccupied}</h1>
                    <h2 className="text-center">Successful Registration</h2>
                  </div>
                </div> :
                <div className="flex justify-center space-x-44 mt-8">
                  <div >
                    <h1 className="font-bold text-2xl text-center mt-6">{data.event.seatCapacity}</h1>
                    <h2 className="text-center">Total Participants</h2>
                  </div>
                  <div >
                    <h1 className="font-bold text-2xl text-center mt-6">{data.event.seatsOccupied}</h1>
                    <h2 className="text-center">Available Participants</h2>
                  </div>
                </div>
            }


            {/* Location section */}
            <h1 className="font-bold mt-16">Location</h1>
            <div>
            {/* <iframe src="{data.event.mapEmbedCode}"/> */}
            <div dangerouslySetInnerHTML={{__html: '<iframe src="' + data.event.mapEmbedUrl + '" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>'}} className="mt-2"></div>
            </div>
            {
              data.event.registrationEndDateTime > Date.now() ?
                <div>
                  <div>
                    <h3>{data.event.registrationEndDateTime}</h3>
                  </div>
                  <div className="flex justify-center space-x-8 mt-8">
                    <RegistrationBtn />
                  </div>
                </div>
                :
                <></>
            }




          </div>


        </div>

      }
    </div>

  )
}
export default EventDetails;