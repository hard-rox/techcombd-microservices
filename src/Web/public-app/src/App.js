import { AuthProvider } from 'oidc-react';

import {
  ApolloClient,
  InMemoryCache,
  useQuery,
  gql
} from "@apollo/client";

import React from 'react';
import {
  BrowserRouter as Router,
  Routes,
  Route
} from 'react-router-dom';

import NavBar from './components/shared/navBar.jsx';
import Footer from './components/shared/footer.jsx';
import Landing from './pages/landing.jsx';
import EventDetails from './pages/eventDetails.jsx';
import AllEvents from './pages/allEvents.jsx';


const oidcConfig = {
  onSignIn: async (user) => {
    alert('You just signed in.');
    console.log(user);
    window.location.hash = '';
    // Redirect?
  },
  authority: 'https://accounts.google.com',
  clientId: '1066073673387-undfdseanu1soilcdprq1p4m8gq8a1iu.apps.googleusercontent.com',
  responseType: 'id_token',
  redirectUri:
    process.env.NODE_ENV === 'development'
      ? 'http://localhost:3000/'
      : 'https://cobraz.github.io/example-oidc-react',
};

function App() {
  return (
    <div>
      <Router>
        <NavBar />
        <Routes>
          <Route exact path='/' element={< Landing />}></Route>
          <Route exact path='/events/:id' element={< EventDetails />}></Route>
          <Route exact path='/events' element={<AllEvents />}></Route>
        </Routes>
      </Router>
      <Footer />
    </div>

    // <AuthProvider {...oidcConfig}>
    // <NavBar />
    // <Landing/>
    // <Footer />
    // </AuthProvider>
  );
}

export default App;
