
import { Link } from 'react-router-dom';
import { gql, useQuery } from '@apollo/client';
import { Carousel } from '@trendyol-js/react-carousel';

const Previous_Events = gql`
  query GetPreviousgEvents($recentDate:DateTime!) {
    events(take: 6, where: { eventEndDateTime: { gt: $recentDate  } }
  order:{eventEndDateTime:ASC}
  ) {
    items {
      id
      title
      about
      bannerUrl
    }
  }
  }
`;

const currentDate = new Date().toISOString();


const PreviousEventCard = () => {
  // useEffect(() => {
  //   document.ready(function() {
  //     ("#news-slider").owlCarousel({
  //         items : 3,
  //         itemsDesktop:[1199,3],
  //         itemsDesktopSmall:[980,2],
  //         itemsMobile : [600,1],
  //         navigation:true,
  //         navigationText:["",""],
  //         pagination:true,
  //         autoPlay:true
  //     });
  //   });
  // }, []);



  const { loading, error, data } = useQuery(Previous_Events, {
    variables: {
      recentDate: currentDate
    },
  });

  if (loading) return 'Loading...';
  if (error) return `Error! ${error.message}`;



  return (
    <div>
      <h1 className="font-bold text-4xl">Previous Events</h1>
      <p className="mt-8 text-justify">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ducimus asperiores officiis iure maiores quae fugit tempora, debitis doloribus enim, ipsa non. Iste mollitia adipisci in debitis autem, exercitationem voluptate sit?</p>

      <div className="flex justify-between space-x-4 mt-8">
        {/* <Carousel show={3.5} slide={2} transition={0.5} swiping={true} className="flex justify-between space-x-4 mt-8"> */}
        {
          data.events.items.map(event => (
            <div className="flex-1 rounded-lg shadow-lg bg-white max-w-sm" key={event.id}>
              <img className="w-full rounded-t-lg" src={event.bannerUrl} alt="#" />
              <div className='p-6'>
                <h4 className="font-bold text-lg mb-2">{event.title}</h4>
                <p className="text-justify mb-4">{event.about.substring(0, 100)}</p>
                <button className="inline-block bg-gray-300 font-bold px-3 py-2 rounded-md hover:bg-gray-400"><Link to={`/events/${event.id}`}>See More</Link></button>
              </div>
            </div>
          ))
        }
          {/* <a class="flex-1 block p-6 max-w-sm bg-white rounded-lg border shadow-lg hover:bg-gray-200 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700">
            <Link to={`/events`}>
              <h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">View All</h5>
            </Link>
          </a> */}
        <Link to={`/events`} className="flex-1 block p-6 max-w-sm bg-white rounded-lg border shadow-lg hover:bg-gray-200 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700 ">
            <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white text-center">View All</h5>
        </Link>

        {/* </Carousel> */}
      </div>

    </div>
  );


}
export default PreviousEventCard;