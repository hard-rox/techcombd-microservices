import { Link } from 'react-router-dom';
import { gql, useQuery } from '@apollo/client';
import RegistrationBtn from '../shared/registrationBtn';
import { useState } from 'react';
import { date } from 'yup';

const Upcoming_Events = gql`
  query GetUpcomingEvents($recentDate:DateTime!) {
    events(take: 3, where: { eventStartDateTime: { gt: $recentDate } } order:{eventStartDateTime:DESC}) {
        items {
          id
          title
          about
          bannerUrl
        }
      }
  }
`;


const currentDate = new Date().toISOString();
const UpComingEventCard = () => {
    const { loading, error, data } = useQuery(Upcoming_Events,{
        variables: {
            recentDate: currentDate
        },
      });

    if (loading) return 'Loading...';
    if (error) return `Error! ${error.message}`;

    // console.debug(data);

    return (
        <div>
            <h1 className="font-bold text-4xl ">Upcoming Events</h1>
            {
                data.events.items.map(event => (
                    <div className="py-14 flex space-x-14" key={event.id}>
                        <div className="my-auto text-justify flex-1">
                            <h4 className="font-bold text-lg">{event.title}</h4>
                            <p className="mt-8 text-justify">{event.about.substring(0,100)}</p>
                            <div className=" mt-8">
                                <button className="bg-gray-300 font-bold px-4 py-2 rounded-md hover:bg-gray-400 mr-4" ><Link to={`/events/${event.id}`}>View Event</Link></button>
                                {/* <button className="bg-gray-300 font-bold px-10 py-2 rounded-md hover:bg-gray-400 ">Register Now</button> */}
                                <RegistrationBtn />
                            </div>
                        </div>
                        <div className="flex-1">
                            <img className="w-full" src={event.bannerUrl} alt="#" />
                        </div>
                    </div>
                ))
            }

            <div className="flex justify-center mt-10">
                <button className="bg-gray-300 font-bold px-10 py-2 rounded-md hover:bg-gray-400 "><Link to={`/events`}>View All</Link></button>
            </div>
        </div>
    );
}
export default UpComingEventCard;