const BlogPostCard = () =>
{
    return(
        <div>
            <h1 className="font-bold text-4xl">Blog Post</h1>
            <p className="mt-8 text-justify">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ducimus asperiores officiis iure maiores quae fugit tempora, debitis doloribus enim, ipsa non. Iste mollitia adipisci in debitis autem, exercitationem voluptate sit?</p>
            <div className="flex justify-between space-x-10 mt-8">
                <div className="flex-1">
                    <img className="w-full" src="https://picsum.photos/500/300" alt="#" />
                    <h4 className="font-bold text-lg mt-4">Blog Post Title</h4>
                    <p className="mt-8 text-justify">Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta est nulla nobis. Vero accusantium quis perferendis debitis modi adipisci quam, numquam explicabo esse architecto, autem harum reprehenderit ea optio eum.</p>
                </div>
                <div className="flex-1 ">
                    <img className="w-full" src="https://picsum.photos/500/300" alt="#" />
                    <h4 className="font-bold text-lg mt-4">Blog Post Title</h4>
                    <p className="mt-8 text-justify">Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta est nulla nobis. Vero accusantium quis perferendis debitis modi adipisci quam, numquam explicabo esse architecto, autem harum reprehenderit ea optio eum.</p>
                </div>
                <div className="flex-1">
                    <img className="w-full" src="https://picsum.photos/500/300" alt="#" />
                    <h4 className="font-bold text-lg mt-4">Blog Post Title</h4>
                    <p className="mt-8 text-justify">Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta est nulla nobis. Vero accusantium quis perferendis debitis modi adipisci quam, numquam explicabo esse architecto, autem harum reprehenderit ea optio eum.</p>
                </div>
            </div>

        </div>

    );

}
export default BlogPostCard;