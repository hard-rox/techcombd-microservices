import React from "react";

import { gql, useMutation } from '@apollo/client';

import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';

import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'

const MySwal = withReactContent(Swal)


//Mutation querry graphQL
const Event_Registration = gql`
  mutation EventRegistration($eventReg: EventRegistrationCommandInput!) {
    registration(input: $eventReg){
        message
        success
      }
  }
`;

// let registrationInput = {
//     eventId: "fae237e4-2379-ec11-b3d6-001f81000250"
// };

const PhoneNoExp = /^[0-9]+$/;
//Validation
const SubmitSchema = Yup.object().shape({
    name: Yup.string()
        .min(2, 'Too Short!')
        .max(50, 'Too Long!')
        .required('Please Enter Your Name.'),
    organization: Yup.string()
        .min(2, 'Too Short!')
        .max(50, 'Too Long!')
        .required('Please Enter Your Organization.'),
    phoneNumber: Yup.string()
        .matches(PhoneNoExp, 'Phone number is not valid')
        .max(11, 'Invalid')
        .required('Please Enter Your Phone Number.'),
    profession: Yup.string()
        .min(2, 'Too Short!')
        .max(50, 'Too Long!')
        .required('Please Enter Your Profession.'),
    email: Yup.string().email('Invalid email').required('Please Enter Your Email.'),
});

const RegistrationBtn = () => {

    const [showModal, setShowModal] = React.useState(false);
    // const [registration, { data, loading, error }] = useMutation(Event_Registration);
    const [registration, { data }] = useMutation(Event_Registration);



    // const handleChange = (e) => {
    //     registrationInput[e.target.name] = e.target.value.trim();

    // };

    // const handleSubmit = (e) => {
    //     e.preventDefault()
    //     //console.log(registrationInput);

    //     registration({
    //         variables: { eventReg: registrationInput }
    //     });
    // };

    // let input;

    // if (loading) return 'Submitting...';
    // if (error) return `Submission error! ${error.message}`;


    // console.log(data)

    return (
        <>
            <button
                className="bg-gray-300 font-bold px-4 py-2 rounded-md hover:bg-gray-400"
                type="button"
                onClick={() => setShowModal(true)}
            >
                Register Now
            </button>
            {showModal ? (
                <>
                    <div
                        className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none"
                    >
                        <div className="relative w-auto my-6 mx-auto max-w-3xl">
                            {/*content*/}
                            <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                                {/*header*/}
                                <div className="flex items-start justify-between p-5 border-b border-solid border-blueGray-200 rounded-t">
                                    <h3 className="text-3xl font-semibold">
                                        Registration
                                    </h3>

                                </div>
                                {/*body*/}
                                <div className="relative p-6 flex-auto">

                                    <Formik
                                        initialValues={{
                                            email: '',
                                            name: '',
                                            organization: '',
                                            phoneNumber: '',
                                            profession: ''
                                        }}
                                        validationSchema={SubmitSchema}
                                        onSubmit={(values, { resetForm }) => {
                                            // same shape as initial values
                                            //console.log(values);
                                            registration({
                                                variables: { eventReg: { ...values, eventId: 'fae237e4-2379-ec11-b3d6-001f81000250' } }
                                            }).then(res => {
                                                console.debug(res);
                                                Swal.fire({
                                                    icon: "success",
                                                    title: res.data.message,
                                                    text: values.name + ' is registered successfully.'
                                                }).then(resetForm({ values:'' }),
                                                    setShowModal(false)
                                                );

                                            }).catch(e => {
                                                console.debug(JSON.stringify(e, null, 1));
                                                Swal.fire({
                                                    icon: "error",
                                                    title: 'Not registered',
                                                    text: e.graphQLErrors[0].message
                                                });
                                            });

                                        }}>
                                        {({ errors, touched }) => (
                                            <Form className="md:w-full" >
                                                <div className="md:flex md:items-center mb-6">
                                                    <div className="md:w-1/3">
                                                        <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" >
                                                            Email
                                                        </label>
                                                    </div>
                                                    <div className="md:w-2/3">
                                                        <Field name="email" className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-blue-500" type="email" placeholder="Email" />
                                                        {errors.email && touched.email ? (
                                                            <div className=" text-red-500">{errors.email}</div>
                                                        ) : null}
                                                    </div>
                                                </div>
                                                <div className="md:flex md:items-center mb-6">
                                                    <div className="md:w-1/3">
                                                        <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" >
                                                            Name
                                                        </label>
                                                    </div>
                                                    <div className="md:w-2/3">
                                                        <Field name="name" className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-blue-500" type="text" placeholder="Name" />
                                                        {errors.name && touched.name ? (
                                                            <div className=" text-red-500">{errors.name}</div>
                                                        ) : null}
                                                    </div>
                                                </div>
                                                <div className="md:flex md:items-center mb-6">
                                                    <div className="md:w-1/3">
                                                        <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" >
                                                            Organization
                                                        </label>
                                                    </div>
                                                    <div className="md:w-2/3">
                                                        <Field name="organization" className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-blue-500" type="text" placeholder="Orgaization" />
                                                        {errors.organization && touched.organization ? (
                                                            <div className=" text-red-500">{errors.organization}</div>
                                                        ) : null}
                                                    </div>
                                                </div>
                                                <div className="md:flex md:items-center mb-6">
                                                    <div className="md:w-1/3">
                                                        <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" >
                                                            Phone no.
                                                        </label>
                                                    </div>
                                                    <div className="md:w-2/3">
                                                        <Field name="phoneNumber" className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-blue-500" type="text" placeholder="Phone no." />
                                                        {errors.phoneNumber && touched.phoneNumber ? (
                                                            <div className=" text-red-500">{errors.phoneNumber}</div>
                                                        ) : null}
                                                    </div>
                                                </div>
                                                <div className="md:flex md:items-center mb-6">
                                                    <div className="md:w-1/3">
                                                        <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" >
                                                            Profession
                                                        </label>
                                                    </div>
                                                    <div className="md:w-2/3">
                                                        <Field name="profession" className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-blue-500" type="text" placeholder="Profession" />
                                                        {errors.profession && touched.profession ? (
                                                            <div className=" text-red-500">{errors.profession}</div>
                                                        ) : null}
                                                    </div>
                                                </div>
                                                <div className="flex items-center justify-end p-6 border-t border-solid border-blueGray-200 rounded-b">
                                                    <button
                                                        className="text-red-500 background-transparent font-bold uppercase px-6 py-2 text-sm outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                                                        type="button"
                                                        onClick={() => setShowModal(false)}
                                                    >
                                                        Close
                                                    </button>

                                                    <button
                                                        className="{errors ? 'bg-neutral-400' : ''}"
                                                        className="bg-blue-500 text-white active:bg-blue-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 "
                                                        type="submit" disabled={(!Formik.isValid && Formik.dirty)}
                                                    >
                                                        Submit
                                                    </button>

                                                </div>
                                            </Form>
                                        )}
                                    </Formik>

                                </div>
                                {/*footer*/}

                            </div>
                        </div>
                    </div>
                    <div className="opacity-25 fixed inset-0 bg-black"></div>
                </>
            ) : null}
        </>

    );


}

export default RegistrationBtn;