module.exports = {
  content: [
    "./src/**/*.{html,ts}"
  ],
  theme: {
    extend: {
      colors: {
        atlas: {
          blue: {
            50: "#DEEBFF",
            75: "#B3D4FF",
            100: "#4C9AFF",
            200: "#0052CC",
            300: "#0065FF",
            400: "#0052CC",
            500: "#0747A6"
          },
          green: {
            100: "#79F2C0",
            400: "#00875A",
            500: "#006644"
          },
          neutrals: {
            10: "#FAFBFC",
            20: "#F4F5F7",
            30: "#EBECF0",
            40: "#DFE1E6",
            70: "#A5ADBA",
            200: "#6B778C",
            500: "#42526E",
            600: "#344563",
            700: "#253858",
            800: "#172B4D",
            900: "#091E42"
          },
          red: {
            75: "#FFBDAD",
            400: "#DE350B",
          },
          yellow: {
            100: "#FFE380",
            200: "#FFC400",
          }
        }
      },
      borderRadius: {
        atlas: "3px"
      },
      boxShadow: {
        'arround': '0px 0px 8px rgba(0, 0, 0, 0.15)',
        'atlas': '0px 8px 12px rgba(9, 30, 66, 0.15)',
      }
    },
  },
  plugins: [],
}
