import { Component } from '@angular/core';
import { OidcSecurityService } from 'angular-auth-oidc-client';
import { HubConnectionBuilder } from "@microsoft/signalr";
import { environment } from 'src/environments/environment';
import { PushNotification } from './shared/types';
import { Router } from '@angular/router';
import { catchError, Observable, of } from 'rxjs';
import { LoggedInUserProfileGQL, LoggedInUserProfileQuery } from 'src/generated/graphql';
import { ApolloQueryResult } from '@apollo/client/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  
  constructor(
    private oidc: OidcSecurityService,
    private router: Router,
    private loggedInUserProfileGql: LoggedInUserProfileGQL
  ) { }

  isAuthenticated: boolean = false;
  isSidebarCollapsed: boolean = false;
  notifications: PushNotification[] = [];
  userProfile$: Observable<ApolloQueryResult<LoggedInUserProfileQuery>> | undefined;
  menues: {
    caption: string,
    icon: string,
    path: string | null,
    children: {
      caption: string,
      path: string
    }[] | null
  }[] = [
    {
      caption: 'Home',
      icon: 'home',
      path: 'home',
      children: null
    },
    {
      caption: 'Events',
      icon: 'festival',
      path: 'events',
      children: null
    },
    {
      caption: 'Profiles',
      icon: 'people',
      path: 'profiles',
      children: [
        {
          caption: 'Persons',
          path: 'persons'
        },
        {
          caption: 'Organizations',
          path: 'organizations'
        },
      ]
    },
  ];

  private configureNotification(token: string) {
    let connection = new HubConnectionBuilder()
      .withUrl(`${environment.notificationGateway}/private`,
        {
          accessTokenFactory: () => token
        })
      .build();
    connection.on('Connected', message => {
      console.debug(message);
    });
    connection.on('Disconnected', message => {
      console.debug(message);
    });
    connection.on('Notification', (notification: PushNotification) => {
      console.debug(notification);
      this.notifications.push(notification);
    });
    connection.start().then(result => {
      console.debug(result);
    });
  }

  private getUserProfile(){
    this.userProfile$ = this.loggedInUserProfileGql.fetch().pipe(
      catchError((error) => {
        console.debug(JSON.stringify(error, null, 2));
        // this.error$.next(error);
        return of();
      })
    );
  }

  ngOnInit() {
    this.oidc.checkAuth().subscribe(result => {
      console.debug(result);
      this.isAuthenticated = result.isAuthenticated;
      if (result.isAuthenticated) {
        this.configureNotification(result.accessToken);
        this.getUserProfile();
      }
      else this.router.navigate(['/error/unauthorized']);
    });
  }

  signout() {
    this.oidc.logoff();
  }
}
