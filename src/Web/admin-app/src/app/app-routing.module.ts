import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './shared/guards';

const routes: Routes = [
  {
    path: "error",
    loadChildren: () => import("./modules/error/error.module").then(x => x.ErrorModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: "home",
    loadChildren: () => import("./modules/home/home.module").then(x => x.HomeModule),
    canActivateChild: [AuthGuard],
  },
  {
    path: "events",
    loadChildren: () => import("./modules/events/events.module").then(x => x.EventsModule),
    canActivateChild: [AuthGuard],
  },
  {
    path: "profiles",
    loadChildren: () => import("./modules/profiles/profiles.module").then(x => x.ProfilesModule),
    canActivateChild: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
