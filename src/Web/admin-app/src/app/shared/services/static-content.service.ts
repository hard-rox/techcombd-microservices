import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StaticContentService {

  private readonly uri = environment.staticContentBaseUrl + 'files';

  constructor(
    private httpClient: HttpClient
  ) { }

  upload(file: File): Observable<any>{
    let formData = new FormData();
    formData.append('', file);
    return this.httpClient.post(this.uri, formData, {observe: 'response'})
    .pipe(map((res) => res.headers.get('Location')));
  }
}
