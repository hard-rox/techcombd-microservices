import { Injectable } from '@angular/core';
import Swal, { SweetAlertOptions } from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor() { }

  showConfirmation(): Promise<boolean> {
    return Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then(result => {
      if (result.isConfirmed)
        return true;
      return false;
    }, err => {
      ////console.debug(err);
      return false;
    });
  }

  showError(title: string, error: string | string[] | undefined = undefined) {
    let options: SweetAlertOptions = {
      title: title,
      icon: 'error'
    };
    if(error?.length ?? 0 > 0) options.html = '<ul>' + (error as string[]).map(x => '<li>' + x + '</li>') + '</ul>';
    else options.text = error as string;
    Swal.fire(options);
  }

  showSuccess(title: string, message: string) {
    Swal.fire({
      title: title,
      icon: 'success',
      text: message,
      confirmButtonColor: '#0052CC',
    });
  }
}
