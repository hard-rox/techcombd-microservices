import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";

export function SelectValidator(values: any): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
        if (Object.values(values).includes(control.value)) return null
        // console.debug(values, control.value);
        return { invalidSelect: true };
    }
}