import { AbstractControl } from "@angular/forms";

export function BdMobileNumberValidator(control: AbstractControl) {
    if ((control?.value?.startsWith('01') && control?.value?.length === 11)
        || (control?.value?.startsWith('+8801') && control?.value?.length === 14)) return null;
    return { invalidBdMobileNumber: true };
}