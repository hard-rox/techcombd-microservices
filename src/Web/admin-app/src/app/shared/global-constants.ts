export class GlobalConstants{
    static readonly PAGE_SIZE: number = 10;
    static readonly EVENT_STATUS_ENUM_NAME: string = 'EventStatus';
    static readonly PROFILE_STATUS_ENUM_NAME: string = 'ProfileStatus';
    static readonly URL_VALIDATION_REGEX: string = '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?';
}