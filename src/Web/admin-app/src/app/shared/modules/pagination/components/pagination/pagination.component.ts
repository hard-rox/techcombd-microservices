import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html'
})
export class PaginationComponent implements OnInit {

  @Input('paginationData') paginationData: { hasPreviousPage: boolean, hasNextPage: boolean } | undefined;
  @Input('currentPage') currentPage: number = 1;
  @Input('totalCount') totalCount: number | undefined;
  @Input('pageSize') pageSize: number | undefined;

  @Output() onPageChange: EventEmitter<number> = new EventEmitter<number>();

  private totalPage: number = 1;

  private setPages() {
    if(this.currentPage > this.totalPage){
      this.pagesToShow = [this.totalPage];
      this.lastPage = this.currentPage;
      return;
    }
    if(this.totalPage > 6){
      this.pagesToShow = Array(5).fill(null).map((el, idx) => idx + 1);
      return;
    }
    else{
      this.pagesToShow = Array(this.totalPage).fill(null).map((el, idx) => idx + 1);
      return;
    }
    //console.debug(this.pagesToShow);
  }

  constructor() { }

  pagesToShow: number[] = [];
  lastPage: number = 1;

  ngOnInit(): void {
    this.totalPage = this.lastPage = Math.max(Math.ceil((this.totalCount ?? 0) / (this.pageSize ?? 1)), 1);
    this.setPages()
    // console.debug(this.paginationData, this.lastPage, this.totalCount, this.pageSize, this.pagesToShow);
  }

  nextClick() {
    if (!this.paginationData?.hasNextPage) return;
    this.changePage(+this.currentPage + 1)
  }

  previousClick() {
    if (!this.paginationData?.hasPreviousPage) return;
    if(this.currentPage > this.totalPage) {
      this.changePage(this.totalPage);
      return;
    }
    this.changePage(+this.currentPage - 1)
  }

  changePage(pageNumber: number) {
    this.onPageChange.emit(pageNumber);
  }

}
