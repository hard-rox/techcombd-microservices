import { animate, style, transition, trigger } from '@angular/animations';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  animations: [
    trigger('modal', [
      // transition(':enter', [
      //   style({ transform: 'translateY(30%)' }),
      //   animate('500ms ease-in', style({ transform: 'translateY(0%)' }))
      // ]),
      // transition(':leave', [
      //   animate('500ms ease-in', style({ transform: 'translateY(30%)' }))
      // ])
    ])
  ]
})
export class ModalComponent implements OnInit {

  @Input() title: string | undefined;
  @Input() isFullsize: boolean = false;
  @Input() hasCloseButton: boolean = true;

  @Output() onSubmitClicked: EventEmitter<void> = new EventEmitter<void>();

  isOpened: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  open() {
    this.isOpened = true;
    document.getElementsByTagName('body')[0].style.overflow = 'hidden';
  }

  close() {
    this.isOpened = false;
    document.getElementsByTagName('body')[0].style.overflow = 'auto';
  }

  submit(){
    this.onSubmitClicked.emit();
  }

  onKeyPressed($event: any){
    console.debug($event)
  }

}
