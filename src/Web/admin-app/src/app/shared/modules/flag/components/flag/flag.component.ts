import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-flag',
  templateUrl: './flag.component.html',
})
export class FlagComponent implements OnInit {

  @Input('type') type: 'success' | 'error' | 'warning' | 'info' = 'info';
  @Input('message') message: string | undefined;

  constructor() { }

  ngOnInit(): void {
  }

}
