export type PushNotification = {
    id: string,
    title: string,
    text: string,
    issuedAt: Date
}