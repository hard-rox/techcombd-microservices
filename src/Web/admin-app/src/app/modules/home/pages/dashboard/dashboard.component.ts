import { Component, OnInit } from '@angular/core';
import { ApolloQueryResult } from '@apollo/client/core';
import { GetDashBoardDataGQL, GetDashBoardDataQuery } from 'src/generated/graphql';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnInit {

  constructor(
    private getDashboardDataGql: GetDashBoardDataGQL
  ) { }

  dashboardDataQueryResult: ApolloQueryResult<GetDashBoardDataQuery> | undefined;

  ngOnInit(): void {
    this.getDashboardDataGql.fetch({
      count: 5,
      currentDateTime: new Date()
    }).subscribe({
      next: (res) => this.dashboardDataQueryResult = res
    });
  }

}
