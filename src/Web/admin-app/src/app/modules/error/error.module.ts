import { NgModule } from '@angular/core';
import { ErrorRoutingModule } from './error-routing.module';
import { UnauthorizedComponent } from './pages/unauthorized/unauthorized.component';

@NgModule({
  declarations: [
    UnauthorizedComponent
  ],
  imports: [
    ErrorRoutingModule
  ]
})
export class ErrorModule { }
