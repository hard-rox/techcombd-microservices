import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OrganizationDetailsComponent } from './pages/organization-details/organization-details.component';
import { OrganizationFormComponent } from './pages/organization-form/organization-form.component';
import { OrganizationsComponent } from './pages/organizations/organizations.component';
import { PersonDetailsComponent } from './pages/person-details/person-details.component';
import { PersonFormComponent } from './pages/person-form/person-form.component';
import { PersonsComponent } from './pages/persons/persons.component';

const routes: Routes = [
  {
    path: 'persons',
    component: PersonsComponent
  },
  {
    path: 'persons/create',
    component: PersonFormComponent
  }, 
  {
    path: 'persons/update/:id',
    component: PersonFormComponent
  },
  {
    path: 'persons/:id',
    component: PersonDetailsComponent
  },
  {
    path: 'organizations',
    component: OrganizationsComponent
  },
  {
    path: 'organizations/create',
    component: OrganizationFormComponent
  },
  {
    path: 'organizations/update/:id',
    component: OrganizationFormComponent
  },
  {
    path: 'organizations/:id',
    component: OrganizationDetailsComponent
  },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfilesRoutingModule { }
