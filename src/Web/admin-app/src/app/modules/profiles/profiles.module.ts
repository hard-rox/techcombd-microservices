import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PersonsComponent } from './pages/persons/persons.component';
import { PersonFormComponent } from './pages/person-form/person-form.component';
import { OrganizationsComponent } from './pages/organizations/organizations.component';
import { OrganizationFormComponent } from './pages/organization-form/organization-form.component';
import { ProfilesRoutingModule } from './profiles-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { FlagModule, PaginationModule } from 'src/app/shared/modules';
import { PersonDetailsComponent } from './pages/person-details/person-details.component';
import { OrganizationDetailsComponent } from './pages/organization-details/organization-details.component';



@NgModule({
  declarations: [
    PersonsComponent,
    PersonFormComponent,
    OrganizationsComponent,
    OrganizationFormComponent,
    PersonDetailsComponent,
    OrganizationDetailsComponent
  ],
  imports: [
    CommonModule,
    ProfilesRoutingModule,
    ReactiveFormsModule,
    PaginationModule,
    FlagModule
  ]
})
export class ProfilesModule { }
