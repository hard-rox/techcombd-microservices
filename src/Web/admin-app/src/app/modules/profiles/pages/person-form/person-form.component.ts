import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ApolloQueryResult } from '@apollo/client/core';
import { GraphQLError } from 'graphql';
import { debounceTime, map, Subject } from 'rxjs';
import { GlobalConstants } from 'src/app/shared/global-constants';
import { AlertService } from 'src/app/shared/services/alert.service';
import { StaticContentService } from 'src/app/shared/services/static-content.service';
import { BdMobileNumberValidator } from 'src/app/shared/validators';
import { CreatePersonGQL, GetPersonForUpdateGQL, GetPersonForUpdateQuery, ProfileStatus, SearchOrganizationsGQL, SearchOrganizationsQuery, UpdatePersonCommandInput, UpdatePersonGQL } from 'src/generated/graphql';

@Component({
  selector: 'app-person-form',
  templateUrl: './person-form.component.html',
})
export class PersonFormComponent implements OnInit {
  private dpFile: any;
  private orgSearchSubject: Subject<any> = new Subject();
  private personIdForUpdate: string | undefined | null;
  private personQueryResult: ApolloQueryResult<GetPersonForUpdateQuery> | undefined;

  constructor(
    private fb: FormBuilder,
    private staticContent: StaticContentService,
    private searchOrgsGql: SearchOrganizationsGQL,
    private createPersonGql: CreatePersonGQL,
    private updatePersonGql: UpdatePersonGQL,
    private activatedRoute: ActivatedRoute,
    private getPersonForUpdateGql: GetPersonForUpdateGQL,
    private alert: AlertService
  ) { }

  isUpdate: boolean = false;
  dpUrl: string | undefined | null;
  profileStatuses = Object.values(ProfileStatus);
  organizationSearchQueryResult: ApolloQueryResult<SearchOrganizationsQuery> | undefined;

  personForm = this.fb.group({
    name: [null, [Validators.required, Validators.pattern('^[a-zA-Z ]*$'), Validators.minLength(3), Validators.maxLength(50)]],
    email: [null, [Validators.required, Validators.email]],
    phoneNumber: [null, [Validators.required, BdMobileNumberValidator]],
    facebook: [null, [Validators.pattern(GlobalConstants.URL_VALIDATION_REGEX)]],
    git: [null, [Validators.pattern(GlobalConstants.URL_VALIDATION_REGEX)]],
    linkedIn: [null, [Validators.pattern(GlobalConstants.URL_VALIDATION_REGEX)]],
    twitter: [null, [Validators.pattern(GlobalConstants.URL_VALIDATION_REGEX)]],
    organizationId: [null],
    organizationName: [null],
    organizationalRole: [null],
    dpUrl: [null],
    status: [null, [Validators.required]]
  });

  private createPerson() {
    // console.debug(this.orgForm.value);
    this.createPersonGql.mutate({
      input: this.personForm.value
    }).subscribe({
      next: (res) => {
        if (res.errors) this.alert.showError('Error', res.errors?.map(x => x.message));
        else this.alert.showSuccess('Created', res.data?.createPerson?.person?.name + ' is created');
      }
    })
  }

  private updatePerson() {
    const updateOrgInput: UpdatePersonCommandInput = { id: this.personIdForUpdate, ...this.personForm.value }
    // console.debug(updateOrgInput);
    this.updatePersonGql.mutate({
      input: updateOrgInput
    }).subscribe({
      next: (res) => {
        res.errors ? this.alert.showError('Not Updated', res.errors.map(x => x.message))
          : this.alert.showSuccess('Updated', res.data?.updatePerson?.updatedPerson?.name + ' is updated');
      }
    })
  }

  private loadPersonForUpdate(personId: string) {
    this.getPersonForUpdateGql.watch({
      id: personId
    }).valueChanges.subscribe({
      next: (res) => {
        // console.debug(res);
        this.personQueryResult = res;
        this.personForm.setValue({
          name: res.data.person?.name,
          phoneNumber: res.data.person?.phoneNumber,
          email: res.data.person?.email,
          status: res.data.person?.status,
          facebook: res.data.person?.facebook,
          git: res.data.person?.git,
          linkedIn: res.data.person?.linkedIn,
          twitter: res.data.person?.twitter,
          organizationId: res.data.person?.organization?.id ?? null,
          organizationName: res.data.person?.organization?.name ?? null,
          organizationalRole: res.data.person?.organizationalRole,
          dpUrl: res.data.person?.dpUrl
        });
        this.dpUrl = res.data.person?.dpUrl
      },
      error: (err: GraphQLError[]) => {
        console.debug(err);
      }
    })
  }

  ngOnInit(): void {
    let personIdParamValue = this.activatedRoute.snapshot.params['id'];
    if (personIdParamValue) {
      this.isUpdate = true;
      this.personIdForUpdate = personIdParamValue;
      this.loadPersonForUpdate(personIdParamValue);
    }

    this.orgSearchSubject.pipe(
      debounceTime(700),
      map(searchText => {
        this.searchOrgsGql.watch({
          searchText: searchText,
          count: 5,
          exceptIds: []
        }).valueChanges.subscribe({
          next: (res) => {
            this.organizationSearchQueryResult = res;
          }
        })
      })
    ).subscribe();
  }

  onDpChange($event: any) {
    let file = $event.target.files[0];
    if (!file) {
      this.dpUrl = this.dpFile = undefined;
      return;
    }
    this.dpUrl = undefined;
    this.dpFile = file;
    let fileReader = new FileReader();
    fileReader.onload = () => {
      this.dpUrl = fileReader.result as string;
    }
    fileReader.readAsDataURL(file);
  }

  onFormSubmit() {
    if (!this.personForm.valid) return;

    // console.debug(this.personForm.value);
    if (this.isUpdate) {
      if (this.dpUrl && !this.dpFile) {
        this.updatePerson();
      }
      else if (this.dpFile) {
        this.staticContent.upload(this.dpFile)
          .subscribe({
            next: (dpUrl: string) => {
              this.personForm.patchValue({ "dpUrl": dpUrl });
              this.updatePerson();
            },
            error: (e) => {
              console.debug(e)
            }
          });
      }
      else {
        this.alert.showError('Can\'t update', 'No display picture found.');
        return;
      }
    } else {
      if (!this.dpFile) {
        this.alert.showError('Can\'t create', 'No display picture found.');
        return;
      }
      this.staticContent.upload(this.dpFile)
        .subscribe({
          next: (dpUrl: string) => {
            this.personForm.patchValue({ "dpUrl": dpUrl });
            this.createPerson();
          },
          error: (e) => {
            console.debug(e)
          }
        });
    }
  }

  onOrgInput(orgName: string) {
    this.orgSearchSubject.next(orgName);
    if(orgName != '' && this.personQueryResult?.data.person?.organization?.name != orgName) this.personForm.patchValue({organizationId: null});
  }

  onOrganizationSelect(org: any) {
    // console.debug(org);
    this.personForm.patchValue({ "organizationId": org?.id });
    this.personForm.patchValue({ "organizationName": org?.name });
  }

}
