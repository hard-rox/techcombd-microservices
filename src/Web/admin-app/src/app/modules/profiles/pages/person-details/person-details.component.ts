import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApolloQueryResult } from '@apollo/client/core';
import { catchError, Observable, of } from 'rxjs';
import { GetPersonDetailsGQL, GetPersonDetailsQuery } from 'src/generated/graphql';

@Component({
  selector: 'app-person-details',
  templateUrl: './person-details.component.html',
})
export class PersonDetailsComponent implements OnInit {

  constructor(
    private activatedRoute: ActivatedRoute,
    private getPersonDetailsGql: GetPersonDetailsGQL
  ) { }

  personDetailsQueryResult: ApolloQueryResult<GetPersonDetailsQuery> | undefined;

  ngOnInit(): void {
    const personId = this.activatedRoute.snapshot.params['id'] as string;
    this.getPersonDetailsGql.watch({
      id: personId
    }).valueChanges.subscribe({
      next: (res) => {
        this.personDetailsQueryResult = res;
      }
    });
  }

}
