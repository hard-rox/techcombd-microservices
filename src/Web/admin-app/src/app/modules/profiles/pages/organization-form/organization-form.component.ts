import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ApolloQueryResult, FetchResult } from '@apollo/client/core';
import { GraphQLError } from 'graphql';
import { catchError, Observable, of } from 'rxjs';
import { GlobalConstants } from 'src/app/shared/global-constants';
import { AlertService } from 'src/app/shared/services/alert.service';
import { StaticContentService } from 'src/app/shared/services/static-content.service';
import { BdMobileNumberValidator } from 'src/app/shared/validators';
import { CreateOrganizationGQL, CreateOrganizationMutation, GetOrganizationForUpdateGQL, ProfileStatus, UpdateOrganizationCommandInput, UpdateOrganizationGQL, UpdateOrganizationMutation } from 'src/generated/graphql';

@Component({
  selector: 'app-organization-form',
  templateUrl: './organization-form.component.html',
})
export class OrganizationFormComponent implements OnInit {

  private logoFile: any;
  private orgIdForUpdate: string | undefined | null;

  constructor(
    private fb: FormBuilder,
    private staticContent: StaticContentService,
    private createOrganizationGql: CreateOrganizationGQL,
    private activatedRoute: ActivatedRoute,
    private getOrgForUpdateGql: GetOrganizationForUpdateGQL,
    private updateOrgGql: UpdateOrganizationGQL,
    private alert: AlertService
  ) { }

  isUpdate = false;
  logoUrl: string | undefined | null;
  profileStatusKeys = Object.values(ProfileStatus);
  orgForm = this.fb.group({
    name: [null, [Validators.required, Validators.minLength(2), Validators.maxLength(50)]],
    email: [null, [Validators.required, Validators.email]],
    address: [null],
    phoneNumber: [null],
    contactPersonName: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
    contactPersonPhone: [null, [Validators.required, BdMobileNumberValidator]],
    facebook: [null, [Validators.pattern(GlobalConstants.URL_VALIDATION_REGEX)]],
    linkedIn: [null, [Validators.pattern(GlobalConstants.URL_VALIDATION_REGEX)]],
    logoUrl: [null],
    website: [null, [Validators.pattern(GlobalConstants.URL_VALIDATION_REGEX)]],
    status: [null, [Validators.required]]
  });

  private loadOrganizationForUpdate(orgId: string) {
    this.getOrgForUpdateGql.fetch({
      id: orgId
    }).subscribe({
      next: (res) => {
        // console.debug(res);
        this.orgForm.setValue({
          name: res.data.organization?.name,
          address: res.data.organization?.address,
          phoneNumber: res.data.organization?.phoneNumber,
          email: res.data.organization?.email,
          contactPersonName: res.data.organization?.contactPersonName,
          contactPersonPhone: res.data.organization?.contactPersonPhone,
          status: res.data.organization?.status,
          website: res.data.organization?.website,
          facebook: res.data.organization?.facebook,
          linkedIn: res.data.organization?.linkedIn,
          logoUrl: res.data.organization?.logoUrl
        });
        this.logoUrl = res.data.organization?.logoUrl
      },
      error: (err: GraphQLError[]) => {
        console.debug(err);
      }
    })
  }

  private createOrganization() {
    // console.debug(this.orgForm.value);
    this.createOrganizationGql.mutate({
      organizationInput: this.orgForm.value
    }).subscribe({
      next: (res) => {
        if (res.errors) this.alert.showError('Couldn\'t Create', res.errors.map(x => x.message));
        else {
          this.alert.showSuccess('Created', res.data?.createOrganization?.organization?.name + ' is created');
        }
      }
    });
  }

  private updateOrganization() {
    const updateOrgInput: UpdateOrganizationCommandInput = { id: this.orgIdForUpdate, ...this.orgForm.value }
    // console.debug(updateOrgInput);
    this.updateOrgGql.mutate({
      input: updateOrgInput
    }).subscribe({
      next: (res) => {
        if (res.errors) this.alert.showError('Couldn\'t update', res.errors.map(x => x.message));
        else {
          this.alert.showSuccess('Update', res.data?.updateOrganization?.updatedOrganization?.name + ' is updated');
        }
      }
    });
  }

  ngOnInit(): void {
    let orgIdParamValue = this.activatedRoute.snapshot.params['id'];
    if (orgIdParamValue) {
      this.isUpdate = true;
      this.orgIdForUpdate = orgIdParamValue;
      this.loadOrganizationForUpdate(orgIdParamValue);
    }
  }

  onLogoChange($event: any) {
    let file = $event.target.files[0];
    this.logoFile = file;
    let fileReader = new FileReader();
    fileReader.onload = () => {
      this.logoUrl = fileReader.result as string;
    }
    fileReader.readAsDataURL(file);
  }

  onFormSubmit() {
    if (!this.orgForm.valid) return;

    // console.debug(this.personForm.value);
    if (this.isUpdate) {
      if (this.logoUrl && !this.logoFile) {
        this.updateOrganization();
      }
      else if (this.logoFile) {
        this.staticContent.upload(this.logoFile)
          .subscribe({
            next: (dpUrl: string) => {
              this.orgForm.patchValue({ "logoUrl": dpUrl });
              this.updateOrganization();
            },
            error: (e) => {
              console.debug(e)
            }
          });
      }
      else {
        this.alert.showError('Can\'t update', 'No logo found.');
        return;
      }
    } else {
      if (!this.logoFile) {
        this.alert.showError('Can\'t create', 'No logo found.');
        return;
      }
      this.staticContent.upload(this.logoFile)
        .subscribe({
          next: (dpUrl: string) => {
            this.orgForm.patchValue({ "logoUrl": dpUrl });
            this.createOrganization();
          },
          error: (e) => {
            console.debug(e)
          }
        });
    }
  }

}
