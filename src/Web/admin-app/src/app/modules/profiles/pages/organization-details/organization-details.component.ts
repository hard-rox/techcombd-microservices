import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApolloQueryResult } from '@apollo/client/core';
import { catchError, Observable, of } from 'rxjs';
import { GetOrganizationDetailsGQL, GetOrganizationDetailsQuery } from 'src/generated/graphql';

@Component({
  selector: 'app-organization-details',
  templateUrl: './organization-details.component.html',
})
export class OrganizationDetailsComponent implements OnInit {

  constructor(
    private activatedRoute: ActivatedRoute,
    private getOrgDetailsGql: GetOrganizationDetailsGQL
  ) { }

  orgDetailsQueryResult: ApolloQueryResult<GetOrganizationDetailsQuery> | undefined;
  error: any;

  ngOnInit(): void {
    const orgId = this.activatedRoute.snapshot.params['id'] as string;
    this.getOrgDetailsGql.watch({
      id: orgId
    }).valueChanges.subscribe({
      next: (res) => {
        this.orgDetailsQueryResult = res;
      }
    });
  }

}
