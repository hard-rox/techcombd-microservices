import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApolloQueryResult, FetchResult } from '@apollo/client/core';
import { catchError, debounceTime, map, Observable, of, Subject } from 'rxjs';
import { GlobalConstants } from 'src/app/shared/global-constants';
import { AlertService } from 'src/app/shared/services/alert.service';
import { GetPersonsGQL, GetPersonsQuery, PersonFilterInput, ProfileStatus, UpdatePersonStatusGQL, UpdatePersonStatusMutation } from 'src/generated/graphql';

@Component({
  selector: 'app-persons',
  templateUrl: './persons.component.html',
})
export class PersonsComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private getPersonsGql: GetPersonsGQL,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private updatePersonStatusGql: UpdatePersonStatusGQL,
    private alert: AlertService
  ) { }

  private filterSubject: Subject<any> = new Subject();
  filterInput: PersonFilterInput | null = null;
  filterForm: FormGroup = this.fb.group({
    name: [null],
    email: [null],
    organization: [null],
    organizationalRole: [null],
    status: ['0'],
  });

  pageSize: number = GlobalConstants.PAGE_SIZE;
  currentPage: number = 1;
  profileStatuses: string[] = Object.values(ProfileStatus);
  profileStatus = ProfileStatus;

  personsQueryResult: ApolloQueryResult<GetPersonsQuery> | undefined;

  private changeRouteQueryParams() {
    this.router.navigate([], {
      relativeTo: this.activatedRoute,
      queryParams: { ...this.filterForm.value, page: this.currentPage }
    });
  }

  private setFilterInput() {
    this.filterInput = {
      and: []
    };

    // console.debug(this.filterForm.value);
    if (this.filterForm.value?.name) {
      this.filterInput.and?.push({ name: { contains: this.filterForm.value?.name } })
    }
    if (this.filterForm.value?.status != 'null') {
      this.filterInput.and?.push({ status: { eq: this.filterForm.value?.status } })
    }
    if (this.filterForm.value?.email) {
      this.filterInput.and?.push({ email: { contains: this.filterForm.value?.email } })
    }
    if (this.filterForm.value?.organization) {
      this.filterInput.and?.push({ organization: { name: { contains: this.filterForm.value?.organization } } })
    }
    if (this.filterForm.value?.organizationalRole) {
      this.filterInput.and?.push({ organizationalRole: { contains: this.filterForm.value?.organizationalRole } })
    }
    // console.debug(this.filterInput);
    if (this.filterInput.and?.length == 0) this.filterInput = null;
  }

  ngOnInit(): void {
    // console.debug(this.activatedRoute.snapshot.queryParams);
    this.currentPage = this.activatedRoute.snapshot.queryParams['page'] ?? 1;
    this.filterForm.get('name')?.setValue(this.activatedRoute.snapshot.queryParams['name'] ?? null);
    this.filterForm.get('status')?.setValue(this.activatedRoute.snapshot.queryParams['status'] ?? null);
    this.filterForm.get('email')?.setValue(this.activatedRoute.snapshot.queryParams['email'] ?? null);
    this.filterForm.get('organization')?.setValue(this.activatedRoute.snapshot.queryParams['organization'] ?? null);
    this.filterForm.get('organizationalRole')?.setValue(this.activatedRoute.snapshot.queryParams['organizationalRole'] ?? null);

    this.loadPage(this.currentPage);

    this.filterForm.valueChanges.subscribe({
      next: (changes) => {
        // console.debug(changes);
        this.changeRouteQueryParams();
        this.filterSubject.next(null);
      }
    });

    this.filterSubject.pipe(
      debounceTime(700),
      map(() => {
        this.setFilterInput();
        this.loadPage(1);
      })
    ).subscribe();
  }

  loadPage(pageNumber: number) {
    if (pageNumber <= 0) return;
    // console.debug(pageNumber);

    this.getPersonsGql.watch({
      skip: (this.pageSize * (pageNumber - 1)),
      take: this.pageSize,
      personFilter: this.filterInput
    }).valueChanges.subscribe({
      next: (res) => this.personsQueryResult = res,
      error: () => this.alert.showError("Something went wrong")
    })
    this.currentPage = pageNumber;
    this.changeRouteQueryParams();
  }

  updateStatus(personId: string, newStatus: ProfileStatus) {
    // console.debug(orgId, newStatus);
    this.updatePersonStatusGql.mutate({
      input: {
        id: personId,
        status: newStatus
      }
    }).subscribe({
      next: (res) => {
        if (res.errors) {

        } else {
          this.alert.showSuccess("Updated", res.data?.updatePersonStatus?.updatedPerson?.name + ' is now ' + res.data?.updatePersonStatus?.updatedPerson?.status);
        }
      }
    })
  }

}
