import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlagModule, ModalModule, PaginationModule } from 'src/app/shared/modules';
import { EventsRoutingModule } from './events-routing.module';
import { EventsComponent } from './pages/events/events.component';
import { EventFormComponent } from './pages/event-form/event-form.component';
import { EventDetailsComponent } from './pages/event-details/event-details.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxEditorModule } from 'ngx-editor';
import { RegistrationsComponent } from './pages/registrations/registrations.component';
import { SafePipe } from 'src/app/shared/pipes';
@NgModule({
  declarations: [
    EventsComponent,
    EventFormComponent,
    EventDetailsComponent,
    RegistrationsComponent,
    SafePipe
  ],
  imports: [
    CommonModule,
    EventsRoutingModule,
    PaginationModule,
    ModalModule,
    ReactiveFormsModule,
    NgxEditorModule,
    FlagModule
  ]
})
export class EventsModule { }
