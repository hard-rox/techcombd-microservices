import { DatePipe } from '@angular/common';
import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApolloQueryResult, FetchResult } from '@apollo/client/core';
import * as moment from 'moment';
import { Editor, toDoc, toHTML } from 'ngx-editor';
import { catchError, debounceTime, map, Observable, of, Subject } from 'rxjs';
import { GlobalConstants } from 'src/app/shared/global-constants';
import { ModalComponent } from 'src/app/shared/modules';
import { AlertService } from 'src/app/shared/services/alert.service';
import { StaticContentService } from 'src/app/shared/services/static-content.service';
import { SelectValidator } from 'src/app/shared/validators';
import { CreateEventCommandInput, CreateEventGQL, CreateEventMutation, EventStatus, EventType, GetEventForUpdateGQL, SearchOrganizationsGQL, SearchOrganizationsQuery, SearchPersonsGQL, SearchPersonsQuery, UpdateEventCommandInput, UpdateEventGQL, UpdateEventMutation } from 'src/generated/graphql';

@Component({
  selector: 'app-event-form',
  templateUrl: './event-form.component.html',
  providers: [DatePipe]
})
export class EventFormComponent implements OnInit {

  @ViewChild('addSpeakersModal') private addSpeakersModal?: ModalComponent;
  @ViewChild('addSponsorsModal') private addSponsorsModal?: ModalComponent;

  private speakerSearchSubject: Subject<any> = new Subject();
  private sponsorSearchSubject: Subject<any> = new Subject();
  eventIdForUpdate: any;

  constructor(
    private searchPersonsGql: SearchPersonsGQL,
    private searchOrgsGql: SearchOrganizationsGQL,
    private createEventGql: CreateEventGQL,
    private updateEventGql: UpdateEventGQL,
    private fb: FormBuilder,
    private staticContentService: StaticContentService,
    private getEventForUpdate: GetEventForUpdateGQL,
    private activatedRoute: ActivatedRoute,
    private alert: AlertService,
    private datePipe: DatePipe,
    private router: Router
  ) { }

  currentDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
  isUpdate: boolean = false;
  eventTypes: string[] = Object.values(EventType);
  eventStatuses: string[] = Object.values(EventStatus);
  selectedSpeakers: any[] = [];
  selectedSponsors: any[] = [];
  bannerFile: File | null | undefined;
  editor: Editor = new Editor();

  personSearchQueryResult: ApolloQueryResult<SearchPersonsQuery> | undefined;
  organizationSearchQueryResult: ApolloQueryResult<SearchOrganizationsQuery> | undefined;
  bannerUrl: string | undefined;

  eventForm: FormGroup = this.fb.group({
    title: [null, [Validators.required, Validators.minLength(5), Validators.maxLength(100)]],
    about: [null, [Validators.required, Validators.minLength(10), Validators.maxLength(500)]],
    descriptionJson: [],
    eventType: [null, [Validators.required, SelectValidator(EventType)]],
    eventStatus: [null, [Validators.required, SelectValidator(EventStatus)]],
    isRegistrationRequired: [false],
    seatCapacity: [null, [Validators.min(1)]],
    eventStartDate: [null, [Validators.required]],
    eventStartTime: [null, [Validators.required]],
    eventEndDate: [null, [Validators.required]],
    eventEndTime: [null, [Validators.required]],
    registrationStartDate: [],
    registrationStartTime: [],
    registrationEndDate: [],
    registrationEndTime: [],
    mapEmbedCode: [],
    facebookEventUrl: [null, [Validators.required, Validators.pattern(GlobalConstants.URL_VALIDATION_REGEX)]]
  })

  private getMapEmbedUrlFromCode(): string | null {
    let el: HTMLElement = document.createElement('div');
    if(!this.eventForm.get('mapEmbedCode')?.value) return null;
    try {
      el.innerHTML = this.eventForm.get('mapEmbedCode')?.value as string;
      const iframe = el.firstChild as HTMLElement;
      const srcValue = iframe.getAttribute('src');
      if (iframe.nodeName != 'IFRAME' || !srcValue || srcValue == null || srcValue == '') {
        this.eventForm.get('mapEmbedCode')?.setErrors(['invalidEmbedCode']);
        this.eventForm.updateValueAndValidity();
        return null;
      }
      else {
        console.debug(srcValue);
        return srcValue;
      }
    } catch (error) {
      this.eventForm.get('mapEmbedCode')?.setErrors(['invalidEmbedCode']);
      this.eventForm.updateValueAndValidity();
      return null;
    }
  }

  private createInput(submitterId: string, bannerUrl: string | null): CreateEventCommandInput | UpdateEventCommandInput {
    let description = this.eventForm.value?.descriptionJson != null ? toHTML(this.eventForm.value?.descriptionJson) : null;
    let input: CreateEventCommandInput | UpdateEventCommandInput = {
      id: this.eventIdForUpdate,
      "title": this.eventForm.get('title')?.value,
      "about": this.eventForm.get('about')?.value,
      "bannerUrl": bannerUrl,
      "description": description,
      "eventStartDateTime": new Date(this.eventForm.get('eventStartDate')?.value + ' ' + this.eventForm.get('eventStartTime')?.value),
      "eventEndDateTime": new Date(this.eventForm.get('eventEndDate')?.value + ' ' + this.eventForm.get('eventEndTime')?.value),
      "registrationStartDateTime": new Date(this.eventForm.get('registrationStartDate')?.value + ' ' + this.eventForm.get('registrationStartTime')?.value),
      "registrationEndDateTime": new Date(this.eventForm.get('registrationEndDate')?.value + ' ' + this.eventForm.get('registrationEndTime')?.value),
      "facebookEventUrl": this.eventForm.get('facebookEventUrl')?.value,
      "isRegistrationRequired": this.eventForm.get('isRegistrationRequired')?.value,
      "location": this.eventForm.get('location')?.value,
      "mapEmbedUrl": this.getMapEmbedUrlFromCode(),
      "seatCapacity": this.eventForm.get('seatCapacity')?.value,
      "type": this.eventForm.get('eventType')?.value,
      "status": !this.isUpdate ? submitterId === 'saveAsDraft' ? EventStatus.Draft : EventStatus.Inactive : this.eventForm.get('eventStatus')?.value,
      "speakerProfileIds": this.selectedSpeakers?.map(x => x.id),
      "sponsorProfileIds": this.selectedSponsors?.map(x => x.id)
    }
    // console.debug(input);    
    return input;
  }

  private loadEventForUpdate(eventId: string) {
    this.getEventForUpdate.fetch({
      id: eventId
    }).subscribe({
      next: (res) => {
        // console.debug(res);
        this.eventForm.setValue({
          title: res.data.event?.title,
          about: res.data.event?.about,
          descriptionJson: toDoc(res.data.event?.description ?? ''),
          eventType: res.data.event?.type ?? '0',
          eventStatus: res.data.event?.status ?? '0',
          isRegistrationRequired: res.data.event?.isRegistrationRequired,
          seatCapacity: res.data.event?.seatCapacity,
          eventStartDate: moment(res.data.event?.eventStartDateTime).format('YYYY-MM-DD'),
          eventStartTime: moment(res.data.event?.eventStartDateTime).format('hh:mm'),
          eventEndDate: moment(res.data.event?.eventEndDateTime).format('YYYY-MM-DD'),
          eventEndTime: moment(res.data.event?.eventEndDateTime).format('hh:mm'),
          registrationStartDate: moment(res.data.event?.registrationStartDateTime).format('YYYY-MM-DD'),
          registrationStartTime: moment(res.data.event?.registrationStartDateTime).format('hh:mm'),
          registrationEndDate: moment(res.data.event?.registrationEndDateTime).format('YYYY-MM-DD'),
          registrationEndTime: moment(res.data.event?.registrationEndDateTime).format('hh:mm'),
          mapEmbedCode: res.data.event?.mapEmbedUrl,
          facebookEventUrl: res.data.event?.facebookEventUrl
        });
        this.selectedSpeakers = res.data.event?.speakers?.map(x => x?.profile) ?? [];
        this.selectedSponsors = res.data.event?.sponsors?.map(x => x?.profile) ?? [];

        // console.debug(this.selectedSpeakers, this.selectedSponsors);
      }
    });
  }

  ngOnInit(): void {

    let eventIdParamValue = this.activatedRoute.snapshot.params['id'];
    if (eventIdParamValue) {
      this.isUpdate = true;
      this.eventIdForUpdate = eventIdParamValue;
      this.loadEventForUpdate(eventIdParamValue);
    }

    this.speakerSearchSubject.pipe(
      debounceTime(500),
      map(searchText => {
        this.searchPersonsGql.fetch({
          searchText: searchText,
          count: 5,
          exceptIds: this.selectedSpeakers.map(x => x.id)
        }).subscribe({
          next: (res) => {
            this.personSearchQueryResult = res;
          }
        });
      })
    ).subscribe();

    this.sponsorSearchSubject.pipe(
      debounceTime(500),
      map(searchText => {
        this.searchOrgsGql.fetch({
          searchText: searchText,
          count: 5,
          exceptIds: this.selectedSponsors.map(x => x.id)
        }).subscribe({
          next: (res) => {
            this.organizationSearchQueryResult = res;
          }
        });
      })
    ).subscribe();

    this.eventForm.valueChanges.subscribe(changes => {
      // console.debug(changes);
    });
  }

  openAddSpeakersModal() {
    this.addSpeakersModal?.open();
  }

  openAddSponsorsModal() {
    this.addSponsorsModal?.open();
  }

  onSpeakerSearchInput(searchText: string) {
    // console.debug(searchText);
    this.speakerSearchSubject.next(searchText);
  }

  onSponsorSearchInput(searchText: string) {
    // console.debug(searchText);
    this.sponsorSearchSubject.next(searchText);
  }

  selectSpeaker(speaker: any) {
    // console.debug(speaker, typeof speaker);
    if (this.selectedSpeakers?.filter(x => x.id === speaker.id).length ?? 0 > 0) return;
    this.selectedSpeakers?.push(speaker);
  }

  selectSponsor(sponsor: any) {
    // console.debug(sponsor, typeof sponsor);
    if (this.selectedSponsors?.filter(x => x.id === sponsor.id).length ?? 0 > 0) return;
    this.selectedSponsors?.push(sponsor);
  }

  removeSpeakerFromSelection(speakerId: string) {
    // console.debug(speakerId);
    this.selectedSpeakers = this.selectedSpeakers?.filter(x => x.id != speakerId);
  }

  removeSponsorFromSelection(sponsorId: string) {
    // console.debug(sponsorId);
    this.selectedSponsors = this.selectedSponsors?.filter(x => x.id != sponsorId);
  }

  onBannerChnange($event: any) {
    let file = $event.target.files[0];
    if (!file) {
      this.bannerUrl = this.bannerFile = undefined;
      return;
    }
    this.bannerUrl = undefined;
    this.bannerFile = file;
    let fileReader = new FileReader();
    fileReader.onload = () => {
      this.bannerUrl = fileReader.result as string;
    }
    fileReader.readAsDataURL(file);
  }

  setRegistrationRequiredValidation(isRegRequired: boolean) {
    // console.debug(isRegRequired);
    if (isRegRequired) {
      this.eventForm.controls['registrationStartDate']?.setValidators([Validators.required]);
      this.eventForm.controls['registrationStartTime'].setValidators([Validators.required]);
      this.eventForm.controls['registrationEndDate'].setValidators([Validators.required]);
      this.eventForm.controls['registrationEndTime'].setValidators([Validators.required]);
      this.eventForm.controls['seatCapacity'].setValidators([Validators.required]);
    } else {
      this.eventForm.controls['registrationStartDate']?.clearValidators();
      this.eventForm.controls['registrationStartTime'].clearValidators();
      this.eventForm.controls['registrationEndDate'].clearValidators();
      this.eventForm.controls['registrationEndTime'].clearValidators();
      this.eventForm.controls['seatCapacity'].clearValidators();
      this.eventForm.patchValue({
        registrationStartDate: null,
        registrationStartTime: null,
        registrationEndDate: null,
        registrationEndTime: null,
        seatCapacity: null,
      })
    }
    this.eventForm.updateValueAndValidity({emitEvent: false});
  }

  onFormSubmit(submitterId: string | undefined) {

    if(submitterId != 'saveAsDraft' && !this.eventForm.valid) return;
    console.debug(this.eventForm.value);

    if (this.isUpdate) {
      if (this.bannerUrl && !this.bannerFile) {
        this.updateEventGql.mutate({
          input: this.createInput(submitterId as string, null) as UpdateEventCommandInput
        }).subscribe({
          next: (res) => {
            if(res.errors) this.alert.showError('Couldn\'t Update', res.errors.map(x => x.message));
            else this.alert.showSuccess('Updated', res.data?.updateEvent?.updatedEvent?.title + ' is updated');
          }
        });
      }
      else if (this.bannerFile) {
        this.staticContentService.upload(this.bannerFile)
          .subscribe({
            next: (bannerUrl: string) => {
              this.updateEventGql.mutate({
                input: this.createInput(submitterId as string, bannerUrl) as UpdateEventCommandInput
              }).subscribe({
                next: (res) => {
                  if (res.errors) this.alert.showError('Couldn\'t Update', res.errors.map(x => x.message));
                  else this.alert.showSuccess('Updated', res.data?.updateEvent?.updatedEvent?.title + ' is updated');
                }
              });
            },
            error: (e) => {
              console.debug(e)
            }
          });
      }
      else {
        this.alert.showError('Can\'t update', 'No banner found.');
        return;
      }
    } else {
      if (!this.bannerFile) {
        this.alert.showError('Can\'t create', 'No banner found.');
        return;
      }
      this.staticContentService.upload(this.bannerFile)
        .subscribe({
          next: (bannerUrl: string) => {
            this.createEventGql.mutate({
              createEventInput: this.createInput(submitterId as string, bannerUrl) as CreateEventCommandInput
            }).subscribe({
              next: (res) => {
                if (res.errors) this.alert.showError('Couldn\'t Create', res.errors.map(x => x.message));
                else {
                  this.alert.showSuccess('Created', res.data?.createEvent?.event?.title + ' is created');
                  this.router.navigate(['events', res.data?.createEvent?.event?.id]);
                }
              }
            });
          },
          error: (e) => {
            console.debug(e)
          }
        });
    }
    
  }

  getFormValidationErrors() {
    Object.keys(this.eventForm.controls).forEach(key => {
      const controlErrors = this.eventForm.get(key)?.errors;
      if (controlErrors != null) {
        Object.keys(controlErrors).forEach(keyError => {
          console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
        });
      }
    });
  }
}
