import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApolloQueryResult } from '@apollo/client/core';
import { Observable } from 'rxjs';
import { GlobalConstants } from 'src/app/shared/global-constants';
import { AlertService } from 'src/app/shared/services/alert.service';
import { GetEventRegistrationsGQL, GetEventRegistrationsQuery, RegistrationFilterInput, RegistrationStatus, UpdateRegistrationStatusGQL } from 'src/generated/graphql';

@Component({
  selector: 'app-registrations',
  templateUrl: './registrations.component.html',
})
export class RegistrationsComponent implements OnInit {

  private eventId: string | null = null;
  private filter: RegistrationFilterInput | null = null;

  constructor(
    private activatedRoute: ActivatedRoute,
    private getEventRegistrationGql: GetEventRegistrationsGQL,
    private updateStatusGql: UpdateRegistrationStatusGQL,
    private alert: AlertService
  ) { }

  pageSize = GlobalConstants.PAGE_SIZE;
  currentPage = 1;
  registrationStatus = RegistrationStatus;
  registrationsQueryResult: ApolloQueryResult<GetEventRegistrationsQuery> | null = null;;

  ngOnInit(): void {
    this.eventId = this.activatedRoute.snapshot.params['eventId'];
    this.loadPage(this.currentPage);
  }

  loadPage(pageNumber: number){
    this.getEventRegistrationGql.watch({
      eventId: this.eventId,
      skip: (+pageNumber - 1) * GlobalConstants.PAGE_SIZE,
      take: this.pageSize,
      filter: this.filter,
    }).valueChanges.subscribe({
      next: (result) => this.registrationsQueryResult = result
    });
  }

  updateStatus(regId: string, status: RegistrationStatus){
    this.updateStatusGql.mutate({
      input: {
        id: regId,
        status: status
      }
    }).subscribe({
      next: (res) => {
        if(res.errors) this.alert.showError('Error', res.errors[0]?.message);
        else this.alert.showSuccess('Updated', res.data?.updateRegistrationStatus?.updatedRegistration?.name + ' is now ' + res.data?.updateRegistrationStatus?.updatedRegistration?.status);
      }
    })
  }

}
