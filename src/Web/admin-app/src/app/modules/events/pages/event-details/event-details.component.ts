import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApolloQueryResult } from '@apollo/client/core';
import { Observable } from 'rxjs';
import { GetEventDetailsGQL, GetEventDetailsQuery } from 'src/generated/graphql';

@Component({
  selector: 'app-event-details',
  templateUrl: './event-details.component.html',
})
export class EventDetailsComponent implements OnInit {


  constructor(
    private activatedRoute: ActivatedRoute,
    private eventDetailsGql: GetEventDetailsGQL
  ) { }

  eventDetailsQueryResult: ApolloQueryResult<GetEventDetailsQuery> | undefined;

  ngOnInit(): void {
    let eventId = this.activatedRoute.snapshot.params['id'];
    this.eventDetailsGql.watch({
      eventId: eventId
    }).valueChanges.subscribe({
      next: (res) => {
        this.eventDetailsQueryResult = res;
      }
    });
  }

}
