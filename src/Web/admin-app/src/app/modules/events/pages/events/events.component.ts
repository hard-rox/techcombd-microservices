import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApolloQueryResult, FetchResult } from '@apollo/client/core';
import { debounceTime, map, Subject } from 'rxjs';
import { GlobalConstants } from 'src/app/shared/global-constants';
import { AlertService } from 'src/app/shared/services/alert.service';
import { EventFilterInput, EventStatus, GetEventsGQL, GetEventsQuery, UpdateEventStatusGQL, UpdateEventStatusMutation } from 'src/generated/graphql';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
})
export class EventsComponent implements OnInit {

  constructor(
    private getEventsGql: GetEventsGQL,
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private updateEventStatusGql: UpdateEventStatusGQL,
    private alertService: AlertService
  ) { }

  private filterSubject: Subject<any> = new Subject();

  pageSize: number = GlobalConstants.PAGE_SIZE;
  currentPage: number = 1;
  eventsQueryResult: ApolloQueryResult<GetEventsQuery> | null = null;
  statusUpdateMutationResult: FetchResult<UpdateEventStatusMutation> | null = null;
  eventStatuses: string[] = Object.values(EventStatus);;
  eventStatus = EventStatus;

  filterInput: EventFilterInput | null = null;
  filterForm: FormGroup = this.fb.group({
    title: [null],
    status: ['0'],
    startDate: [null],
    endDate: [null],
  });

  private setFilterInput(){
    this.filterInput = {
      and: []
    };

    // console.debug(this.filterForm.value);
    if(this.filterForm.value?.title){
      this.filterInput.and?.push({title: {contains: this.filterForm.value?.title}})
    }
    if(this.filterForm.value?.status && this.filterForm.value?.status != 'null'){
      this.filterInput.and?.push({status: {eq: this.filterForm.value?.status}})
    }
    if(this.filterForm.value?.startDate){
      this.filterInput.and?.push({eventStartDateTime: {gte: this.filterForm.value?.startDate}})
    }
    if(this.filterForm.value?.startDate){
      this.filterInput.and?.push({registrationStartDateTime: {gte: this.filterForm.value?.startDate}})
    }
    if(this.filterForm.value?.endDate){
      this.filterInput.and?.push({eventEndDateTime: {lte: this.filterForm.value?.endDate}})
    }
    if(this.filterForm.value?.endDate){
      this.filterInput.and?.push({registrationEndDateTime: {lte: this.filterForm.value?.endDate}})
    }

    // console.debug(this.filterInput);
    if(this.filterInput.and?.length == 0) this.filterInput = null;
  }

  private changeRouteQueryParams(){
    this.router.navigate([], {
      relativeTo: this.activatedRoute,
      queryParams: {...this.filterForm.value, page: this.currentPage}
    });
  }

  ngOnInit(): void {
    // console.debug(this.activatedRoute.snapshot.queryParams);
    this.currentPage = this.activatedRoute.snapshot.queryParams['page'] ?? 1;
    this.filterForm.get('title')?.setValue(this.activatedRoute.snapshot.queryParams['title'] ?? null);
    this.filterForm.get('status')?.setValue(this.activatedRoute.snapshot.queryParams['status'] ?? null);
    this.filterForm.get('startDate')?.setValue(this.activatedRoute.snapshot.queryParams['startDate'] ?? null);
    this.filterForm.get('endDate')?.setValue(this.activatedRoute.snapshot.queryParams['endDate'] ?? null);
    this.setFilterInput();

    this.loadPage(this.currentPage);

    this.filterForm.valueChanges.subscribe({
      next: (changes) => {
        // console.debug(changes);
        this.changeRouteQueryParams();
        this.filterSubject.next(null);
      }
    });

    this.filterSubject.pipe(
      debounceTime(700),
      map(() => {
        this.setFilterInput();
        // console.debug(this.filterInput)
        this.loadPage(1);
      })
    ).subscribe();
  }

  loadPage(pageNumber: number) {
    if (pageNumber <= 0) return;
    // console.debug(pageNumber);

    this.getEventsGql.watch({
      skip: (this.pageSize * (pageNumber - 1)),
      take: this.pageSize,
      filter: this.filterInput
    }).valueChanges.subscribe({
      next: (res) => {
        this.eventsQueryResult = res;
        if(res.errors) this.alertService.showError("Error", res.errors.map(x => x.message).join('\n'));
      }
    });
    this.currentPage = pageNumber;
    this.changeRouteQueryParams();
  }

  updateStatus(eventId: string | undefined, newStatus: EventStatus){
    // console.debug(orgId, newStatus);
    this.updateEventStatusGql.mutate({
      input: {
        id: eventId,
        status: newStatus
      }
    }).subscribe({
      next: (res) => {
        if(res.data)this.alertService.showSuccess("Updated", res.data.updateEventStatus?.updatedEvent?.title + ' is now ' + res.data.updateEventStatus?.updatedEvent?.status)
      } 
    });
  }
}
