import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EventDetailsComponent } from './pages/event-details/event-details.component';
import { EventFormComponent } from './pages/event-form/event-form.component';
import { EventsComponent } from './pages/events/events.component';
import { RegistrationsComponent } from './pages/registrations/registrations.component';

const routes: Routes = [
  {
    path: '',
    component: EventsComponent
  },
  {
    path: 'create',
    component: EventFormComponent
  },
  {
    path: 'update/:id',
    component: EventFormComponent
  },
  {
    path: ':id',
    component: EventDetailsComponent,
  },
  {
    path: ':eventId/registrations',
    component: RegistrationsComponent,
  },
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EventsRoutingModule { }
