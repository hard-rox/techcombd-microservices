// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  notificationGateway: "https://localhost:44385",
  // notificationGateway: "http://192.168.1.150/techcombd/staging/services/pushnotification",

  oidcAuthority: "https://localhost:44392",
  // oidcAuthority: "http://192.168.1.150/techcombd/staging/services/oidc-auth",
  
  webGraphqlGateway: "https://localhost:44300/graphql",
  // webGraphqlGateway: "http://192.168.1.150/techcombd/staging/gateways/web-graphql/graphql",

  staticContentBaseUrl: "https://localhost:44320/"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
