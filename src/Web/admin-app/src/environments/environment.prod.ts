export const environment = {
  production: true,
  notificationGateway: "",
  oidcAuthority: "http://192.168.1.150/techcombd/staging/services/oidc-auth",
  webGraphqlGateway: "http://192.168.1.150/techcombd/staging/gateways/web-graphql/graphql",
  staticContentBaseUrl: "https://localhost:44320/"
};
